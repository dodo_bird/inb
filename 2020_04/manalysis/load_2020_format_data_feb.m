function data_struct = load_2020_format_data_feb(fpath1)

task_labels = {
    'Control (Reaction Time)',...
    'Control Fix intervals (RT)',...
    'Multiple-Tap Tuning',...
    'Tuning',...
    'Multiple-Tap Blocks'};

empty_cell_threshold_sps = 2;

load(fpath1)


% Meta info.
data_struct.monkey = behav.Correct(1).Monkey;
if strcmp(behav.Correct(1).Monkey,'M1')
    str=num2str(int64(behav.Correct(1).session));
    data_struct.session_id = str2double(str(int64(str)~=32));
else
    data_struct.session_id = behav.Correct(1).session(9:14);
end
data_struct.sampling_rate = 24414.0625;
data_struct.period_vec = [.45 .55 .65 .75 .85 .95];

data_struct.trial_crop(1) = -.75;
data_struct.trial_crop(2) = .5;


% data_struct.trial.Periodic = [];
% data_struct.trial.Random = [];
% data_struct.trial.Incorrect = [];
% data_struct.trial.Correct = [];


% Raw sps and first selection criterion.
ST = behav.Correct(1).Times;
nonempty_signals = zeros(1,size(ST,1));
for l=1:size(ST,1)
    % Average firing rate
    ave_fr = numel(ST{l})/diff(ST{l}([1 end]));
    fprintf('Neuron # %3.0d is at %8.3f Hz.\n',l,ave_fr)
    if ave_fr>empty_cell_threshold_sps
        nonempty_signals(l)=1;
    end
end
nonempty_signals_index = find(nonempty_signals);clear nonempty_signals
data_struct.nonempty_signals_index = nonempty_signals_index;


% Here restructure the data, find extra info such as stim interval, create
% condition indices in fields, and cut the neural data per trial.
tr_cumul = 0;
for s = 1:numel(behav.Correct)
    for correct=[0 1]
        % I guess the other conditions/tasks will come in here as extra loops.
        switch correct
            case 0
                if numel(behav.Incorrect)<s
                    continue
                end
                beh = behav.Incorrect(s);
            case 1
                if numel(behav.Correct)<s
                    continue
                end
                beh = behav.Correct(s);
        end
        
        if iscell(beh.ID)
            taskID = beh.ID{1};
            fprintf('%s\n',beh.ID{1})
        else
            taskID = beh.ID;
            fprintf('%s\n',beh.ID)
        end
        task=nan;
        for l=1:numel(task_labels)
            if strcmp(task_labels{l},taskID)
                task=l-1;
            end
        end
        if isnan(task);continue;end
        task(task>2)=task(task>2)-1;
        
        
        remove_really_short_trials = find(sum(~isnan(beh.Stims),2)<3);
        if ~isempty(remove_really_short_trials)
            beh.Stims(remove_really_short_trials,:)=[];
            beh.Movs(remove_really_short_trials,:)=[];
            beh.numtrials = beh.numtrials - numel(remove_really_short_trials);
        end
        
        % Don't even bother unless monkey completed more than x trials.
        if beh.numtrials<5
            continue
        end
        
        for tr=1:beh.numtrials
            tr_cumul = tr_cumul + 1;
            data_struct.task(tr_cumul,1)=task;
            data_struct.taskID{tr_cumul,1}=taskID;
            data_struct.task_label{tr_cumul,1}=taskID;
            
            data_struct.correct(tr_cumul,1)=correct;
            data_struct.stims{tr_cumul,1}=beh.Stims(tr,~isnan(beh.Stims(tr,:)));
            if numel(data_struct.stims{tr_cumul,1})>1
                if diff(data_struct.stims{tr_cumul,1}(end-1:end))>2
                    data_struct.stims{tr_cumul,1}(end) = [];
                end
            end
            data_struct.tap{tr_cumul,1}=beh.Movs(tr,~isnan(beh.Movs(tr,:)));
            
            % !!! Crop some stims and taps?
            % data_struct.stims{tr_cumul} = data_struct.stims{tr_cumul}(3:end-1);
            % data_struct.tap{tr_cumul} = data_struct.tap{tr_cumul}(2:end-1);
            
            %             switch correct
            %                 case 0
            %                     data_struct.trial.Incorrect=[data_struct.trial.Incorrect;tr_cumul];
            %                 case 1
            %                     data_struct.trial.Correct=[data_struct.trial.Incorrect;tr_cumul];
            %             end

            % Find the reference point where the monkey starts tapping.
            if ~isempty(data_struct.stims{tr_cumul,1})
                if ~isempty(data_struct.tap{tr_cumul,1})
                    index = find(data_struct.stims{tr_cumul,1}<data_struct.tap{tr_cumul,1}(1),1,'last');
                    data_struct.center(tr_cumul,1) = data_struct.stims{tr_cumul,1}(index);
                    data_struct.first_tap(tr_cumul,1) = index;
                else
                    data_struct.center(tr_cumul,1) = data_struct.stims{tr_cumul,1}(end);
                    data_struct.first_tap(tr_cumul,1) = nan;
                end
            else
                data_struct.center(tr_cumul,1) = nan;
                data_struct.first_tap(tr_cumul,1) = nan;
            end
            
            temp = round(diff(beh.Stims(tr,:))*1e2)./1e2;
            temp(isnan(temp))=[];
            % You have this info in data_struct.task
            if any(task==[0 1])
                data_struct.interval(tr_cumul,1)=mean(temp);
                % data_struct.trial.Random=[data_struct.trial.Random;tr_cumul];
                data_struct.periodic_index(tr_cumul,1)=0;
            else
                data_struct.interval(tr_cumul,1)=mean(temp);
                [~,ind] = min((data_struct.interval(tr_cumul)-data_struct.period_vec).^2);
                data_struct.interval(tr_cumul,1)=data_struct.period_vec(ind);
                % data_struct.trial.Periodic=[data_struct.trial.Periodic;tr_cumul];
                data_struct.periodic_index(tr_cumul,1)=1;
            end
            % Guess if it was a random or periodic stim and find the period.
            %             if (std(temp)>.02) && numel(temp)>2
            %                 data_struct.interval(tr_cumul,1)=nan;
            %                 data_struct.trial.Random=[data_struct.trial.Random;tr_cumul];
            %                 data_struct.periodic_index(tr_cumul,1)=0;
            %             else
            %                 data_struct.interval(tr_cumul,1)=mean(temp);
            %                 [~,ind] = min((data_struct.interval(tr_cumul)-data_struct.period_vec).^2);
            %                 data_struct.interval(tr_cumul,1)=data_struct.period_vec(ind);
            %                 data_struct.trial.Periodic=[data_struct.trial.Periodic;tr_cumul];
            %                 data_struct.periodic_index(tr_cumul,1)=1;
            %             end
            
            data_struct.id_trials(tr_cumul,1)=nan;
            data_struct.error(tr_cumul,1)=nan;
            
            % Now cut the neural data.
            data_struct.lims(tr_cumul,:) = [(data_struct.trial_crop(1)+min(data_struct.stims{tr_cumul,1})) (data_struct.trial_crop(2)+max(data_struct.stims{tr_cumul,1}))];  %  max(data_struct.tap{tr_cumul});
            %for l=1:numel(nonempty_signals_index)
            %    index = logical(...
            %        (ST{nonempty_signals_index(l)}>lims(1))...
            %        .*(ST{nonempty_signals_index(l)}<lims(2)));
            %    if sum(index)>0
            %        data_struct.Neurons{tr_cumul,nonempty_signals_index(l)} = ST{nonempty_signals_index(l)}(index)';
            %    end
            %end
            for l=1:numel(ST)
               index = logical((ST{l}>data_struct.lims(tr_cumul,1)).*(ST{l}<data_struct.lims(tr_cumul,2)));
               if sum(index)>0
                   data_struct.Neurons{tr_cumul,l} = ST{l}(index)';
               else
                   data_struct.Neurons{tr_cumul,l} = [];
               end
            end
        end
    end
end

for tr = 1:numel(data_struct.tap)
    data_struct.stims_to_tap(tr,1) = numel(data_struct.stims{tr}) - numel(data_struct.tap{tr});
end
