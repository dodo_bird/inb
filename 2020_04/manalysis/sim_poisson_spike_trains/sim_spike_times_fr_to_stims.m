function data = sim_spike_times_fr_to_stims(varargin)
%sim_spike_times_fr_to_stims
% Simulate n cells with a Poisson spike train. Each of m trials has a
% stimulus with a different period. Some of the cells' spiking rate is
% tuned to the stimulus, others not. Set the plotting flag for a visual check
% and several other parameters.
% Dr. DGD


% If enabled, this will make more cells to be responsive to the stimulus
% with higher stimulus intervals. This is meant to resemble the 2019 PLoSBiol
% paper where more cells were found to be recruited in longer stimulus intervals.
switch 0
    case 0
        prop_active_cells_bias = 0;
    case 1
        prop_active_cells_bias = 1;
end

% If enabled, this will increase the cells' firing rate linearly with
% stimulus interval.
switch 0
    case 0
        firing_rate_period_bias = 0;
    case 1
        firing_rate_period_bias = 1;
end

% Diagnostic plotting or not?
if isempty(varargin)
    plotting = 1;
else
    plotting = varargin{1};
end
if plotting == 1
    close all
end

% The important parameters.
spikesPerS = 40;	 % max spikes per second
spikesPerS_base = 5; % baseline sps
timeStepS = .001;	 % 1 msec
prop_cells_with_signal = 10; % Every that many cells respond to the stimulus.

durationS = .050;	 % 50 msec simulation bins
sr = 1/durationS;    % trial sampling rate
conv_win_len_ms = 400; % msec
gaussFilter = gausswin(round(conv_win_len_ms/1e3*sr*1));
gaussFilter = gaussFilter./sum(gaussFilter);

event_int = .450:.100:.950; % s

num_events = 5;
num_cells = 50;
data.sr = 1/timeStepS;
data.period_vec = event_int;
data.Neurons = cell(1);
counter = 0;
for reps = 1:2
    for p = 1:numel(event_int)
        counter = counter + 1;
        events = cumsum(repmat(event_int(p),num_events,1));
        data.stims{1,counter} = events;
        data.interval(counter,1) = event_int(p);
        trial_time_vec = (0:durationS:(events(end)+.5))';
        
        SPIKES = [];
        for c = 1:num_cells
            
            firing_rate = trial_time_vec*0;
            % Allow for a period-varying number of signal cells.
            prop = round(prop_cells_with_signal - prop_active_cells_bias*p);
            if mod(c,prop)==0
                for ev = 1:numel(events)
                    [~,ind] = min(abs(trial_time_vec-events(ev)));
                    firing_rate(ind) = 1;
                end
                temp = conv(firing_rate,gaussFilter,'same');temp = temp./max(temp);
                firing_rate = (firing_rate_period_bias*20*event_int(p) + rand(1) + spikesPerS)*temp; % Just a little bit var to the fr.
            end
            firing_rate = firing_rate + rand(size(firing_rate))+spikesPerS_base/2;
            
            spikes = [];
            for t = 1:numel(trial_time_vec)
                spikes_temp = makeSpikes(timeStepS, firing_rate(t), durationS);
                spikes = horzcat(spikes,spikes_temp(2:end));
            end
            data.Neurons{counter,c} = find(spikes).*timeStepS;
            SPIKES = [SPIKES;spikes];
            
            if plotting == 1
                figure(1)
                subplot(2,3,p)
                plot(trial_time_vec,firing_rate);
                hold on
            end
        end
        
        if plotting == 1
            figure(1)
            subplot(2,3,p)
            plot(events,0,'^r')
            hold off
            ylim([0 spikesPerS*1.5])
            xlabel('Trial time, s');ylabel('fr, sps')
            
            figure(2)
            subplot(2,3,p)
            rasterPlot(SPIKES);
            hold on
            plot(events*1e3,0,'^r')
            hold off
        end
    end
end

data.trial_crop = [-0.2000 0.5000];
data.session_id = int64(now);
for p = 1:size(data.Neurons,1)
    data.tap{1,p} = data.stims{1,p}(end)+.2;
    data.correct(p,1) = 1;
    data.task(p,1) = 2;
    data.center(1,p) = data.stims{p}(end);
    data.periodic_index(p,1) = 1;
    data.id_trials(p,1) = p;
    data.lims(p,:) = [data.stims{p}(1)+data.trial_crop(1) data.stims{p}(end)+data.trial_crop(2)];
    for c = 1:size(data.Neurons,2)
        index = logical((data.Neurons{p,c}>data.lims(p,1)).*(data.Neurons{p,c}<data.lims(p,2)));
        data.Neurons{p,c} = data.Neurons{p,c}(index);
    end
end

end

function spikes = makeSpikes(timeStepS, spikesPerS, durationS, numTrains)

if (nargin < 4)
    numTrains = 1;
end
times = 0:timeStepS:durationS;
spikes = zeros(numTrains, length(times));
for train = 1:numTrains
    vt = rand(size(times));
    spikes(train, :) = (spikesPerS*timeStepS) > vt;
end
end

function rasterPlot(spikes)
%axes('position', [0.1, 0.1, 0.8, 0.8]);
%axis([0, length(spikes) - 1, 0, 1]);
trains = size(spikes, 1);
ticMargin = 0.01; % gap between spike trains (full scale is 1)
ticHeight = (1.0 - (trains + 1) * ticMargin) / trains;

for train = 1:trains
    spikeTimes = find(spikes(train, :) == 1);
    yOffset = ticMargin + (train - 1) * (ticMargin + ticHeight);
    for i = 1:length(spikeTimes)
        line([spikeTimes(i), spikeTimes(i)], [yOffset, yOffset + ticHeight],'color','k');
    end
end
set(gca,'YTick',[])
xlabel('Time (ms)')
%title('Raster plot of spikes');
end