clear
switch computer
    case 'PCWIN64'
        home_folder = 'C:\Users\Dobri\Desktop\inb2\';
        data_folder = 'C:\Users\Dobri\Desktop\inb_data';
    case 'GLNXA64'
        home_folder = '~/inb/inb2/';
        data_folder = '/media/dobri/disk2/inb_data';
end
addpath(fullfile(home_folder,'manalysis','feb2020')) %'laplace_and_gauss_additive'))


%%
m = 2;
for sess=1:2
    % Simulate. Simple: Poisson neurons + time-varying firing rate.
    data = sim_spike_times_fr_to_stims(0);
    
    % Get the sdf per neuron per trial.
    data = sdf_raw_over_trials(data);
    
    % The PCs.
    data.PC = get_pcs_looping_over_trials_from_raw_sts(data,[],0,0);
    pcs_plot_aves_per_period_etc(data,0,[]);pause
    
    plotting_flag = 0;save_plots_flag = 0;
    [out_SDF_PC_per_trial_cyclofit{m}{sess},...
        out_SDF_PC_per_trial_on_peaks{m}{sess},...
        out_SDF_PC_per_trial_which_ramping{m}{sess}] = ...
        get_pc_cycl_fit_params(data.PC,data,[],plotting_flag,save_plots_flag,[]);
    
    plotting_flag = 0;save_plots_flag = 0;
    [PC{m}{sess},out_PC_beta_exponents{m}{sess}] = ...
        pcs_plot_power_law_eigenvalues(data,plotting_flag,0,[]);
end


%%
all_pc1_peak_raw_params.table = [];
all_pc1_peak_ave_params.table = [];
all_pc1_peak_raw_params.labels = [];
all_pc1_peak_ave_params.labels = [];
for m=1:2
    for n=1:size(out_SDF_PC_per_trial_on_peaks{m},2) %which_session
        if ~isempty(out_SDF_PC_per_trial_on_peaks{m}{n})
            all_pc1_peak_raw_params.table = vertcat(all_pc1_peak_raw_params.table,...
                out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks);
            all_pc1_peak_ave_params.table = vertcat(all_pc1_peak_ave_params.table,...
                out_SDF_PC_per_trial_on_peaks{m}{n}.params);
            % Get the column labels only once.
            if isempty(all_pc1_peak_ave_params.labels)
                if ~isempty(out_SDF_PC_per_trial_on_peaks{m}{n})
                    all_pc1_peak_raw_params.labels = out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks_labels;
                    all_pc1_peak_ave_params.labels = out_SDF_PC_per_trial_on_peaks{m}{n}.param_labels;
                end
            end
        end
    end
end
fprintf('\n\n')

R=[];P=[];
for dv=9:15;subplot(3,3,dv-8);[R(1,dv-8),P(1,dv-8)]=corr(all_pc1_peak_raw_params.table(:,4),all_pc1_peak_raw_params.table(:,dv),'Rows','pairwise');scatter(all_pc1_peak_raw_params.table(:,4),all_pc1_peak_raw_params.table(:,dv));title(all_pc1_peak_raw_params.labels{dv});end
disp([R;P])