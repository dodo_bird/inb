%%
clear
monkey = 1;
scale_ind_cell_activity = false;
target_sr_per_cycle = 50;

%% Prep paths
lib_folder = '~/Desktop/inb/2020_04/manalysis';
data_folder = '~/Desktop/inb_data';
addpath(lib_folder)


%% Choose monkey
base_folder = fullfile(data_folder,['M' num2str(monkey,'%1.f') '-TBC']);
cd(fullfile(base_folder,'mid_processing'))


%% These mat files are sessions w/ sorted spike times and behav events.
file_name_sessions = dir('mid_processing_*');


%% Trial time limits
SDFS = aggredate_sdfs_over_trials_sessions_2021(file_name_sessions, monkey);


%% load
% load('SDFS_aggregate_stage.mat','SDFS')

%% get the cell ids and max #
ts_len = size(SDFS.sdf{1},1);

found_cells = [];
if monkey == 1
    % Monkey 1 has more trials w/ .85
    comb_conditions = [1 2 3 4 6 7 8 9 10 12];
else
    comb_conditions = 1:numel(SDFS.cell_id);
end
for c = comb_conditions
    found_cells = horzcat(found_cells,SDFS.cell_id{c});
end
found_cells = sort(unique(found_cells));


% total_unit_count = [];
% for c = 1:size(SDFS.cell_id,1)
%     total_unit_count(c,1) = numel(SDFS.cell_id{c});
%     if ts_len ~= size(SDFS.sdf{c},1)
%         keyboard
%     end
% end
% [~,ind] = max(total_unit_count);
% count_id_across_all_sessions = SDFS.cell_id{ind};
% if any(found_cells~=sort(count_id_across_all_sessions));keyboard;end


%% Resample
SDF_huge = [];
var_cells = found_cells*nan;
mean_cells = found_cells*nan;

if any(monkey==[1 2])
    conditions_vec = 7:12;
else
    conditions_vec = 1:8;
end

for counter = conditions_vec
    % Resample per interval and concat. This will give 50 samples per stimulus interval.
    sdfds = resample([nan(1,size(SDFS.sdf{counter},2));SDFS.sdf{counter};nan(1,size(SDFS.sdf{counter},2))], ...
        round(target_sr_per_cycle/SDFS.conds_interval(counter)), round(1/mean(diff(SDFS.time{counter}))));
    % sdfds = resample([nan(1,size(SDFS.sdf{counter},2));SDFS.sdf{counter};nan(1,size(SDFS.sdf{counter},2))],5,round(SDFS.conds_interval(counter)*1e2));
    sdfds_temp = nan(size(sdfds,1),numel(found_cells));
    for c = 1:size(sdfds,2)
        ind = find(SDFS.cell_id{counter}(c)==found_cells);
        if ~isempty(ind)
            var_cells(counter,ind) = nanvar(sdfds(:,c));
            mean_cells(counter,ind) = nanmean(sdfds(:,c));
            sdfds_temp(:,ind) = sdfds(:,c);
        end
    end
    SDF_huge = [SDF_huge;sdfds_temp];
end


%% Place in one long array w/out resampling. Quicker.
if 0
    SDF_HUGE = nan(ts_len,numel(found_cells),size(SDFS.sdf,1));
    for c = 1:size(SDFS.sdf,1)
        for n = 1:numel(found_cells)
            ind = find(found_cells(n)==SDFS.cell_id{c});
            if ~isempty(ind)
                SDF_HUGE(:,n,c) = SDFS.sdf{c}(:,ind);
            end
        end
    end
    SDF_HUGE = reshape(permute(SDF_HUGE,[1 3 2]),ts_len*size(SDFS.sdf,1),numel(found_cells));
end


%% Remove totally empty cells and periods
cell_ids_retained_c = found_cells(~all(isnan(SDF_huge),1));
SDF_huge_c = SDF_huge(~all(isnan(SDF_huge),2),~all(isnan(SDF_huge),1));

percent_cells_threshold = .1;
% Why? At this level, pretty much all data from bad trials will be excluded.
SDF_huge_c(sum(isnan(SDF_huge_c),2)./size(SDF_huge_c,2)>percent_cells_threshold,:) = [];

SDF_huge_c(isnan(SDF_huge_c)) = 0;
mean_cells_c = nanmean(SDF_huge_c);
if scale_ind_cell_activity
    scale_by_var_cells_c = nanvar(SDF_huge_c);
else
    scale_by_var_cells_c = ones(1,size(SDF_huge_c,2));
end


%% Normalize: softmax, divide by the max variance from all conditions.
SDF_huge_c = SDF_huge_c - mean_cells_c;
SDF_huge_c = SDF_huge_c./scale_by_var_cells_c;


%% Holy grail
[P,sc,~,~,explained_c,mu_cells_c] = pca(SDF_huge_c);
% [P,sc,~,~,explained_c,mu_cells_c] = pca(SDF_huge_c(1:10:end,:),'algorithm','als');
% [P,sc,~,~,explained_c,mu_cells_c] = pca(SDF_huge_c,'Rows','pairwise'); % by now it's the same deal cause we've cleaned the sdf mat a lot.


%% Save the PC coefficients
save(fullfile(base_folder,'mid_processing','P_stage.mat'),...
    'P','cell_ids_retained_c','scale_by_var_cells_c','mu_cells_c','explained_c')


%% Load again
% load(fullfile(base_folder,'P_stage.mat'))
% load('SDFS_aggregate_stage.mat')


%% Project neurons over population activity. Use averaged neurons across trials in the same condition.
counter_plus_session = 0;
LV.lv = cell(numel(SDFS.sdf),1);
LV.cell_id = cell(numel(SDFS.sdf),1);
LV.time = cell(numel(SDFS.sdf),1);
for counter = 1:numel(SDFS.sdf)
    session_ids = unique(round(SDFS.cell_id{counter}./1e4));
    for s = 1:numel(session_ids)
        cells_alive = ~all(isnan(SDFS.sdf{counter}),1); % remove any period with at least one unit in a nan state.
        matching_mat = (((cell_ids_retained_c==SDFS.cell_id{counter}').*(cells_alive')).*(round(cell_ids_retained_c./1e4) == session_ids(s))).*((round(SDFS.cell_id{counter}./1e4) == session_ids(s))');
        index_matching_cells_in_session_for_P = find(sum(matching_mat,1)==1);
        index_matching_cells_in_session_for_SDF = find(sum(matching_mat,2)==1)';
        
        cells_in_session = SDFS.cell_id{counter}(index_matching_cells_in_session_for_SDF);
        x = SDFS.sdf{counter}(:,index_matching_cells_in_session_for_SDF);
        %index_nan_in_time = (sum(isnan(x),2)./size(x,2))>.01; % if more cols than this prop are nan, mark all rows as nan. why?
        index_nan_in_time = any(isnan(x),2); % remove rows with at least one nan.
        if ~all(index_nan_in_time) && ~isempty(x)
            x = x(~index_nan_in_time,:);
            x = resample(x,5,round(SDFS.conds_interval(counter)*1e2));
            x = x - mean_cells_c(index_matching_cells_in_session_for_P);
            x = x./scale_by_var_cells_c(index_matching_cells_in_session_for_P);
                
            
            t = linspace(min(SDFS.time{counter}(~index_nan_in_time,:)),...
                max(SDFS.time{counter}(~index_nan_in_time,:)),size(x,1))';
            
            lv = (P(index_matching_cells_in_session_for_P,:)'*x')';
        
            fprintf('%6.0f,',SDFS.conds_succ(counter),SDFS.conds_periodic(counter),...
                SDFS.conds_interval(counter)*1e3,...
                session_ids(s),size(lv,1),size(lv,2));
            fprintf('\n')

            counter_plus_session = counter_plus_session + 1;
            LV.conditions(counter_plus_session,:) = ...
                [SDFS.conds_succ(counter),SDFS.conds_periodic(counter),...
                SDFS.conds_interval(counter)];
            LV.lv{counter_plus_session,1} = lv;
            LV.cell_id{counter_plus_session,1} = cells_in_session;
            LV.time{counter_plus_session,1} = t;
        end
    end
end

if 1
    for counter_plus_session = 1:numel(LV.lv)
        fprintf('%6.0f,',LV.conditions(counter_plus_session,:).*[1 1 1e2],...
            size(LV.lv{counter_plus_session,1},1),size(LV.lv{counter_plus_session,1},2));
        fprintf('\n')
        index = (LV.time{counter_plus_session,1}./LV.conditions(counter_plus_session,3))>-3;
        for d=1:6
            %plot(LV.time{counter_plus_session,1}./LV.conditions(counter_plus_session,3),smooth(LV.lv{counter_plus_session,1}(:,d),1e1),'-','linewidth',2)
            plot(LV.time{counter_plus_session,1}(index)./LV.conditions(counter_plus_session,3),LV.lv{counter_plus_session,1}(index,d),'-','linewidth',2)
            hold on
        end
        hold off
        set(gca,'color','k')
        pause
    end
end


%%
isi_intervals_vec = unique(SDFS.conds_interval);

LVave.conditions = zeros(numel(isi_intervals_vec)*2,3);
LVave.lv = cell(numel(isi_intervals_vec)*2,1);
LVave.cell_id = cell(numel(isi_intervals_vec)*2,1);
LVave.time = cell(numel(isi_intervals_vec)*2,1);
LVave.count_n = cell(numel(isi_intervals_vec)*2,1);
counter = 0;
for succ = 0:1
    for p = 1
        for i = 1:numel(isi_intervals_vec)
            counter = counter + 1;
            min_t = nan;
            max_t = nan;
            sr = [];
            for l = 1:numel(LV.time)
                if all(LV.conditions(l,:) == [succ p isi_intervals_vec(i)])
                    min_t = min(min_t,min(LV.time{l}));
                    max_t = max(max_t,max(LV.time{l}));
                    sr = vertcat(sr,1/mean(diff(LV.time{l})));
                end
            end
            min_t = min_t - .1;
            max_t = max_t + .1;
            sr = mean(sr);
            
            LVave.conditions(counter,:) = [succ p isi_intervals_vec(i)];
            LVave.time{counter} = linspace(min_t,max_t,round((max_t-min_t)*sr))';
            LVave.lv{counter} = nan(size(LVave.time{counter},1),size(LV.lv{1},2)); % as many pc dimensions
            LVave.count_n{counter} = LVave.time{counter}*0;
            [~,ind1] = min(abs(LVave.time{counter}-0));
            for l = 1:numel(LV.lv)
                if all(LV.conditions(l,:) == [succ p isi_intervals_vec(i)])
                    [~,ind2] = min(abs(LV.time{l}-0));
                    index = ((ind1-ind2+1):(size(LV.time{l},1)-ind2+ind1))';
                    if any(index<1);keyboard;index(index<1)=[];end
                    LVave.count_n{counter}(index) = LVave.count_n{counter}(index) + 1;
                    for n=1:size(LV.lv{l},2)
                        LVave.lv{counter}(index,n) = nansum([LVave.lv{counter}(index,n),LV.lv{l}(:,n)],2);
                    end
                end
            end
            index = any(isnan(LVave.lv{counter}),2);
            LVave.lv{counter}(index,:) = [];
            LVave.time{counter}(index) = [];
            LVave.count_n{counter}(index) = [];
            LVave.lv{counter} = LVave.lv{counter}./LVave.count_n{counter};
        end
    end
end

for counter = 1:numel(LVave.time)
    t = LVave.time{counter}./LVave.conditions(counter,3);
    x = LVave.lv{counter}(:,1:3);
    plot(t(t>-6),x(t>-6,:))
    pause
end


%%
for l=1:numel(LVave.time)
    t = LVave.time{l};
    index = (t./LVave.conditions(l,3))>-3; % that many stims before first tap.
    t = t(index);
    x = LVave.lv{l}(index,1:3);
    stims = find(diff(mod(t./LVave.conditions(l,3),1))<-.5);
    subplot(2,6,l)
    plot_3d_cool_color(t,x,1,10,t(stims)',0);
end


%%