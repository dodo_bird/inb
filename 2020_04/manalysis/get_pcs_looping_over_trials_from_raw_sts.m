function PC = get_pcs_looping_over_trials_from_raw_sts(data_struct,pics_path,plotting_flag,save_plots_flag)

dim_red_method = 1;
switch dim_red_method 
    case 1
        drm = 'pca';
        center_pc_flag = true;
    case 2
        drm = 'nnmf';
end
PC=cell(numel(data_struct.interval),1);
for trial=1:numel(data_struct.interval)
    if isfield(data_struct.SDF_RAW{trial},'sdf') && ~isempty(data_struct.SDF_RAW{trial}.sdf)
        if strcmp(drm,'pca')
            index = sum(isnan(data_struct.SDF_RAW{trial}.sdf),2) < size(data_struct.SDF_RAW{trial}.sdf,2);
            x = data_struct.SDF_RAW{trial}.sdf(index,:);
            x(isnan(x)) = 0;
            [~,PC{trial}.data,PC{trial}.latent,~,PC{trial}.r2,mu_vec] = ...
                pca(x,'Centered',center_pc_flag);
            PC{trial}.data = PC{trial}.data + mu_vec;
            % At most 100 columns.
            PC{trial}.data(:,min(100,size(PC{trial}.data,2)):end) = [];
            PC{trial}.data = PC{trial}.data - min(PC{trial}.data); % I feel a little weird about this. What's the interpretation of the variable then?
        else
            n_factors = 10;
            [PC{trial}.data,PC{trial}.latent] = ...
                nnmf(data_struct.SDF_RAW{trial}.sdf,n_factors);
            %    nnmf(data_struct.SDF_RAW{trial}.sdf,min(50,size(data_struct.SDF_RAW{trial}.sdf,2)));
            % nnmf(data_struct.SDF_RAW{trial}.sdf,5);
            r2vec=[];for c=1:n_factors;r2vec(c,1)=mean(1-var(data_struct.SDF_RAW{trial}.sdf - PC{trial}.data(:,c)*PC{trial}.latent(c,:))./var(data_struct.SDF_RAW{trial}.sdf));end
            PC{trial}.r2 = r2vec;
        end
        PC{trial}.time = data_struct.SDF_RAW{trial}.time(index);
        PC{trial}.time_units = data_struct.SDF_RAW{trial}.time_units(index);
    else
        PC{trial}.data = [];
        PC{trial}.latent = [];
        PC{trial}.r2 = [];
        PC{trial}.time = [];
    end
    PC{trial}.session_id = data_struct.session_id;
    PC{trial}.interv = data_struct.interval(trial);
    PC{trial}.period = data_struct.interval(trial);
    PC{trial}.correct = data_struct.correct(trial);
    PC{trial}.trial = trial;
    PC{trial}.fr_ave = nanmean(PC{trial}.data);
    PC{trial}.fr_sd = nanstd(PC{trial}.data);
    PC{trial}.fr_median = nanmedian(PC{trial}.data);
    PC{trial}.fr_range = range(PC{trial}.data);
end

% We don't really expect to look very high in PC axes.
fr_ave_vec = nan(size(PC,1),5);
fr_sd_vec = nan(size(PC,1),5);
fr_median_vec = nan(size(PC,1),5);
fr_range_vec = nan(size(PC,1),5);
for tr=1:size(PC,1)
    fr_ave_vec(tr,:) = PC{tr}.fr_ave(1:5);
    fr_sd_vec(tr,:) = PC{tr}.fr_sd(1:5);
    fr_median_vec(tr,:) = PC{tr}.fr_median(1:5);
    fr_range_vec(tr,:) = PC{tr}.fr_range(1:5);
end

PC{1}.fr_ave = nanmean(fr_ave_vec);
PC{1}.fr_sd = nanmean(fr_sd_vec);
PC{1}.fr_median = nanmean(fr_median_vec);
PC{1}.fr_range = nanmean(fr_range_vec);

%% Plot the pcs and print for later reference.
if plotting_flag == 1
    close all
    figure('Position',[1 1 1000 800])
    for tr=1:numel(PC)
        if ~isempty(PC{tr}.data) && size(PC{tr}.data,1)>1e3
            plot(PC{tr}.time(1:10:end),PC{tr}.data(1:10:end,1:3),'linewidth',2)
            hold on
            plot(data_struct.stims{tr},data_struct.stims{tr}*0,'rv','linewidth',3)
            hold off
            grid on
            %ylim([-4 6 -60 140])
            per = data_struct.interval(tr)*1e2;
            title(['PCs, tr ' num2str(tr,'%03.0f') ', correct' num2str(data_struct.correct(tr),'%.0f') ', bpm' num2str(per,'%03.0f')])
            %if (PC{tr}.period*1e2) ~= per;keyboard;end
            if save_plots_flag == 1
                f = fullfile(pics_path,['pcs_of_all_sdf_tr_' num2str(tr,'%03.0f') '_correct' num2str(data_struct.correct(tr),'%.0f') '_bpm' num2str(per,'%03.0f') '_' datestr(now,'yyyy-mm-dd-HHMMSS') '.png']);
                print(f, '-dpng', '-r50');
            else
                pause
            end
        end
    end
end