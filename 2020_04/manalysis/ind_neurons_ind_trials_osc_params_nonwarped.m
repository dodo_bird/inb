function IND_Cell_IND_Trial_osc_params = ind_neurons_ind_trials_osc_params_nonwarped(...
    data_struct,session_id,plotting_flag,varargin)

%%
IND_Cell_IND_Trial_osc_params.param_labels = {'sessionID','cell-num','task',...
    'periodic','correct-trials','trial','trial-phase',...
    'selected','amp-max','amp-mean','amp-sig','amp-noise','amp-to-noise',...
    '\lambda','rate-c','rate-a','gof-r2',...
    'b_{\Delta}','\Delta','\theta','peakA','amp','delta-rise','theta-rise',...
    'delta-bottom','amp-bottom','sd-delta'};
IND_Cell_IND_Trial_osc_params.params = [];

IND_Cell_IND_Trial_osc_params.raw_peaks_labels = {'sessionID','cell-num','task',...
    'periodic','correct-trials','trial','trial-phase',...
    'event_num','delta','theta','amp','delta-rise','theta-rise',...
    'delta-bottom','amp-bottom','delta2'};
IND_Cell_IND_Trial_osc_params.raw_peaks_data = [];


%%
cycles_trial_phases = [-inf .7;0 inf];
row = 0;
for tr = 1:numel(data_struct.SDF_RAW)
    for l = 1:numel(data_struct.SDF_RAW{tr}.cell_ids)
        sdf = data_struct.SDF_RAW{tr}.sdf(:,l);
        for trial_phase = [1 2]
            sdf_time = data_struct.SDF_RAW{tr}.time;
            % This is time warped but not resampled.
            % sdf_time = data_struct.SDF_RAW{tr}.time_units;
            index = logical((sdf_time<(cycles_trial_phases(trial_phase,2)+data_struct.center(tr))).*...
                (sdf_time>(cycles_trial_phases(trial_phase,1)+data_struct.center(tr))));
            tt = sdf_time(index);
            xx = sdf(index);
            %% Careful with signal quality!
            if mean(xx)>1
                row=row+1;
                IND_Cell_IND_Trial_osc_params.params(row,1:27) = nan;
                
                task = data_struct.task(tr);
                period = data_struct.interval(tr);
                correct = data_struct.correct(tr);
                cellid = data_struct.SDF_RAW{tr}.cell_ids(l);
                conds = [str2double(session_id) cellid task period correct tr trial_phase];
                IND_Cell_IND_Trial_osc_params.params(row,1:7) = conds;
                fprintf('%6.0f,',conds([2:3 5:end]))
                fprintf('%10.3f,',period,mean(xx))
                fprintf('\n')
                
                
                %% 1. Signal analysis (raw mean, amp, range, etc..)
                clear temp
                temp.sdf=xx;
                temp.var=xx*0;
                temp.time=tt;
                IND_Cell_IND_Trial_osc_params.params(row, 8:13) = ...
                    ts_amp_and_noise_of_oscillations_to_select_neurons(...
                    temp,3,cycles_trial_phases(trial_phase,1),cycles_trial_phases(trial_phase,2));


                %% 2. Cycle-by-cycle analysis
                stand_devs_threshold = 1;
                ind = find(data_struct.SDF_RAW{1}.cell_ids==data_struct.SDF_RAW{tr}.cell_ids(l));
                threshold_crossing = data_struct.SDF_RAW{1}.sps_ave(ind)+...
                    stand_devs_threshold*data_struct.SDF_RAW{1}.sps_sd(ind);
                peaks_stats = ts_of_sdf_peak_params(xx,tt-data_struct.center(tr),plotting_flag,...
                    data_struct.SDF_RAW{tr}.sr,data_struct.stims{tr}-data_struct.center(tr),[],...
                    threshold_crossing);
                IND_Cell_IND_Trial_osc_params.params(row, 18) = peaks_stats.params(5);
                IND_Cell_IND_Trial_osc_params.params(row, 19:27) = peaks_stats.params([1:4 6:10]);
                
                % Concatenate ind cycle params in a long table.
                if ~isempty(peaks_stats.raw_data)
                    switch trial_phase
                        case 1
                            event_num = (((size(peaks_stats.raw_data,1)-1)*-1):0)';
                        case 2
                            event_num = (1:size(peaks_stats.raw_data,1))';
                    end
                    IND_Cell_IND_Trial_osc_params.raw_peaks_data = ...
                        vertcat(IND_Cell_IND_Trial_osc_params.raw_peaks_data,...
                        [repmat(conds,size(peaks_stats.raw_data,1),1) event_num ...
                        peaks_stats.raw_data peaks_stats.raw_data(:,end-3)./2/pi*conds(:,3)]);
                end
                
                
                %% 3. Attack-rate analysis if (IND_Cell_osc_params.data(row-1,8)*2)>10 && (IND_Cell_osc_params.data(row,8)*2)>10
                if 0
                    fpath1 = varargin{1};
                    save_plots_flag = varargin{2};
                    temp = ts_rate_params(xx,tt,plotting_flag);
                    IND_Cell_IND_Trial_osc_params.params(row, 14:17) = temp.data;
                    % Visualize
                    if plotting_flag == 1
                        if save_plots_flag == 1
                            set(gcf,'Position',[1,1,1920,1280])
                            f = fullfile(fpath1,'pics',['rate_fit_sdf_correct' num2str(succ,'%.0f') '_cell' num2str(data_struct.nonempty_signals_index(l),'%.0f') '_trial' num2str(trial,'%.0f') '_phase' num2str(trial_phase,'%1f') '_' datestr(now,'yyyy-mm-dd-HHMMSS') '.png']);
                            print(f, '-dpng', '-r100');
                        else
                            pause
                        end
                    end
                end
            end
        end
    end
end


%% Regressions
regress_these_raw_peak_vars = [9:11 15];
nsi = unique(IND_Cell_IND_Trial_osc_params.raw_peaks_data(:,2));
sess = unique(IND_Cell_IND_Trial_osc_params.raw_peaks_data(:,1));
IND_Cell_IND_Trial_osc_params.raw_peaks_fits.table = cell(1,numel(regress_these_raw_peak_vars));
IND_Cell_IND_Trial_osc_params.raw_peaks_fits.dv = cell(1,numel(regress_these_raw_peak_vars));
IND_Cell_IND_Trial_osc_params.raw_peaks_fits.labels = ...
    {'sessionID','cell-num','trial-phase',...
    'reg-int-coef','reg-per-coef','reg-correct-coef','reg-order-coef','reg-rand-coef',...
    'reg-int-p','reg-per-p','reg-correct-p','reg-order-p','reg-rand-p'};
for s = 1:numel(sess)
    for l = 1:numel(nsi)
        for trial_phase = [1 2]
            index = logical(...
                (IND_Cell_IND_Trial_osc_params.raw_peaks_data(:,1)==sess(s)).*...
                (IND_Cell_IND_Trial_osc_params.raw_peaks_data(:,2)==nsi(l)).*...
                (IND_Cell_IND_Trial_osc_params.raw_peaks_data(:,7)==trial_phase));
            if sum(index)>20
                % In some sessions there were not any incorrect trials.
                no_correct_range = numel(unique(IND_Cell_IND_Trial_osc_params.raw_peaks_data(index,5)))==1;
                no_rand = sum(IND_Cell_IND_Trial_osc_params.raw_peaks_data(index,3)<2)==0;
                x=[IND_Cell_IND_Trial_osc_params.raw_peaks_data(index,[4 5 8]) ...
                    IND_Cell_IND_Trial_osc_params.raw_peaks_data(index,3)>1];
                if no_correct_range
                    if no_rand
                        pred_cols = [1 3];
                    else
                        pred_cols = [1 3 4];
                    end
                else
                    if no_rand
                        pred_cols = [1 2 3];
                    else
                        pred_cols = [1 2 3 4];
                    end
                end
                for dv=1:numel(regress_these_raw_peak_vars)
                    y=IND_Cell_IND_Trial_osc_params.raw_peaks_data(index,regress_these_raw_peak_vars(dv));
                    lm=fitlm(x(:,pred_cols),y,'linear');
                    betas_temp = lm.Coefficients.Estimate'.*(lm.Coefficients.pValue'<.01)*(lm.coefTest<.05);
                    ps_temp = lm.Coefficients.pValue'.^(lm.coefTest<.05);
                    betas = nan(1,5);
                    ps = nan(1,5);
                    betas([1 1+pred_cols]) = betas_temp;
                    ps([1 1+pred_cols]) = ps_temp;
                    coefs = [sess(s) nsi(l) trial_phase betas ps];
                    IND_Cell_IND_Trial_osc_params.raw_peaks_fits.dv{dv} = IND_Cell_IND_Trial_osc_params.raw_peaks_labels{regress_these_raw_peak_vars(dv)};
                    IND_Cell_IND_Trial_osc_params.raw_peaks_fits.table{dv} = vertcat(IND_Cell_IND_Trial_osc_params.raw_peaks_fits.table{dv},coefs);
                end
            end
        end
    end
end

% keyboard

%% Some diagnostic plots for sanity check
if plotting_flag==2
    dv=18;
    for l=1:numel(data_struct.nonempty_signals_index)
        index = IND_Cell_IND_Trial_osc_params.params(:,2)==data_struct.nonempty_signals_index(l);
        boxplot(IND_Cell_IND_Trial_osc_params.params(index,dv),IND_Cell_IND_Trial_osc_params.params(index,[5 7]))
        title(IND_Cell_IND_Trial_osc_params.param_labels{dv})
        fprintf('Cell %6.0f, Firing rate %8.3f.',data_struct.nonempty_signals_index(l),mean(IND_Cell_IND_Trial_osc_params.params(index,9)))
        fprintf('\n')
        pause
    end
end

if plotting_flag==3
    for trial_phase = 1
        for l = 1:numel(data_struct.nonempty_signals_index)
            for succ = 0:1
                for per = 1:numel(data_struct.period_vec)
                    subplot(2,3,per)
                    index = logical((IND_Cell_IND_Trial_osc_params.params(:,2)==data_struct.nonempty_signals_index(l))...
                        .*(IND_Cell_IND_Trial_osc_params.params(:,4)==data_struct.period_vec(per))...
                        .*(IND_Cell_IND_Trial_osc_params.params(:,5)==succ)...
                        .*(IND_Cell_IND_Trial_osc_params.params(:,7)==trial_phase));
                    hist(IND_Cell_IND_Trial_osc_params.params(index,19))
                    fprintf('%6.0f',data_struct.nonempty_signals_index(l),trial_phase,succ,per)
                    fprintf('\n')
                end
                title(IND_Cell_IND_Trial_osc_params.param_labels{19})
                pause
            end
        end
    end
end
