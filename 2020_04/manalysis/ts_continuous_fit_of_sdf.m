function sdf_fit_by_cont_osc = ts_continuous_fit_of_sdf(xx,tt,plotting,varargin)
%%Fit a cycloid-like function with increasing amplitude and compare params
% by way of their 95% CI. [How to do proper model comparison?]
% Or, go in R and use mle
% https://www.r-bloggers.com/fitting-a-model-by-maximum-likelihood/
% https://stats.stackexchange.com/questions/87570/maximum-likelihood-estimation-for-custom-equation-in-r

%resfun=@(b0,b1,b2,theta,x)(b0*b2*(x+6+1/b2).*abs(cos(2*pi*x./2+theta))+b1);
%resfun=@(b0,b1,theta,x)(b0*b1*(x+6+1/b1).*abs(cos(2*pi*x./2+theta)));
%resfun=@(b0,b1,theta,x)((b1*x+b0).*abs(cos(2*pi*x./2+theta)));

resfun=@(b0,b1,theta,fr,x)((b1*x+b0).*(-abs(cos(2*pi*fr*x./2+pi*fr/2-theta*fr/2))+1));
%resfun=@(b0,b1,theta,x)((b1*x+b0).*(-abs(cos(2*pi*x./2+pi/2-theta/2))+1));
%resfun_sawtooth=@(b0,b1,theta,delta,x)((b1*x+b0).*(sawtooth(2*pi*x+theta,delta)+1)./2);
%plot(t,resfun(20,5,pi,2*pi,t),t,resfun_sawtooth(20,5,pi,.5,t))

if numel(varargin)>0
    period = varargin{1};
else
    period = 1;
end

sdf_fit_by_cont_osc.cont_nl_regression_labels={...
    'b_{0,-95%}','b_{0}','b_{0,+95%}',...
    'b_{1,-95%}','b_{1}','b_{1,+95%}',...
    '\Delta','\theta','nan',...
    'R^2','RMSE'};

fo = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',[-100,-100,-2*pi,(1/period)],...
    'Upper',[200, 100,2*pi,(1/period)],...
    'StartPoint',[10 10 pi/4,(1/period)]);

%ft = fittype('resfun(mus)' )
ft = fittype(resfun,'options',fo);
[curve,gof] = fit(repmat(tt,size(xx,2),1),xx(:),ft);

cofs=[coeffvalues(curve);confint(curve)]';
cofs=cofs(1:3,[2 1 3]);
cofs(3,:)=nan;

yy=resfun(curve.b0,curve.b1,curve.theta,curve.fr,tt);
[~,events]=findpeaks(yy./max(yy),'MinPeakDistance',.2,'MinPeakProminence',.05);
[~,ind]=min(abs(tt(events)));
delta=tt(events(ind));
if isempty(delta);delta=nan;end
cofs(3,1)=delta;
cofs(3,2)=mod(delta,period)/period*2*pi;

sdf_fit_by_cont_osc.cont_nl_regression = [reshape(cofs',1,[]) gof.rsquare gof.rmse];

if plotting == 1
    plot(tt,xx,'-','LineWidth',1)
    hold on
    plot(tt,yy,'-r','linewidth',3)
    %disp([curve.theta curve.theta/2/pi*period curve.theta/2/pi/period curve.theta/2])
    plot(curve.theta/2/pi,0,'^r','linewidth',2)
    plot(delta,0,'vm','linewidth',2)
    hold off
    pause
end

%figure(2)
%plot(tt,sum(xx==0,2))
%figure(1)
%keyboard
%print('-djpeg','-r100',['raw_trajectories_and_fits_CPD-6_cell' num2str(neuron_l,2) '_succ' num2str(success,0) '_per' num2str(periodic,0) '_' datestr(now,'yyyy-mm-dd-HHMMSS') '.jpg'])

%{
if which('curve')
    %                     fo = fitoptions('Method','NonlinearLeastSquares',...
    %                         'Lower',[0,0,-10,0],...
    %                         'Upper',[100,20,100,2*pi],...
    %                         'StartPoint',[curve.b0 curve.b1 curve.b2 curve.theta]);
    fo = fitoptions('Method','NonlinearLeastSquares',...
        'Lower',[-100,-100,0],...
        'Upper',[200, 100,2*pi],...
        'StartPoint',[10 5 pi/4]);
    %'StartPoint',[curve.b0 curve.b1 curve.theta]);
else
    %                     fo = fitoptions('Method','NonlinearLeastSquares',...
    %                         'Lower',[0,0,-10,0],...
    %                         'Upper',[100,20,100,2*pi],...
    %                         'StartPoint',[1 1 5 pi/8]);
end
%}


%sdf_fit_by_cont_osc.cycle_trends_params=cell(2,2,numel(SDF{2,2}.avg));
%sdf_fit_by_cont_osc.cycle_trends_labels=[{'periodic'},'success','nt',...
%    'peaks-b_1','peaks-b_0','valls-b_1','valls-b_0','osc-amps-b_1','osc-amps-b_0','last-amp',...
%    'mean-peak-phase','mean-onset-phase','psd-at-1cycle',...
%    'trend-r^2','var-rel-to-ave'];

%sdf_fit_by_cont_osc.cycle_amp_data_labels =[{'periodic'},'success','nt',...
%    'peak_loc','peak-level','osc-amp',...
%    'peak-phase','onset-phase','r^2'];
%sdf_fit_by_cont_osc.cycle_amp_data=cell(2,2,numel(SDF{2,2}.avg));

%                 for nt=1:numel(current_trials)
%                     curr_stims = st.stimtimes_warped{nt};
%                     curr_butts = st.butttimes_warped{nt};
%
%                     % temp0_conv is what you need to apply the
%                     % time-series analysis to: filter, hilbert
%                     % phase, sync from peak times, amps, b0 and b1.
%                     % Also curr_stims and curr_butts.
%                     temp = ts_analysis(temp0_conv(:,nt),trial_time_start_stop_vec,curr_stims,curr_butts,plotting,sampling_rate*.1);
%                     if ~isempty(temp) && ~isempty(temp.cycle_amp_data)
%                         varintime = SDF{periodic+1,success+1}.var_in_time_per_trial{neuron_l}(nt);
%                         SDF_TS.cycle_trends_params{periodic+1,success+1,neuron_l} = vertcat(SDF_TS.cycle_trends_params{periodic+1,success+1,neuron_l},...
%                             [periodic success nt temp.bpeak temp.bvall temp.bamp temp.last_amp temp.ave_peak_phase temp.ave_onset_phase temp.psd_peak temp.trend_r2 varintime]);
%                         SDF_TS.cycle_amp_data{periodic+1,success+1,neuron_l} = vertcat(SDF_TS.cycle_amp_data{periodic+1,success+1,neuron_l},...
%                             [ones(size(temp.cycle_amp_data,1),1)*periodic ones(size(temp.cycle_amp_data,1),1)*success ones(size(temp.cycle_amp_data,1),1)*nt temp.cycle_amp_data]);
%                     end
%                 end
%
%                 % The same, but on the trial-averaged SDF.
%                 temp = ts_analysis(SDF{periodic+1,success+1}.avg{neuron_l},SDF{periodic+1,success+1}.time{neuron_l},(-4:0)',.1,plotting,sampling_rate*.1);
%                 SDF_TS.cycle_trends_params_ave_trial{periodic+1,success+1,neuron_l} = ...
%                             [periodic success inf temp.bpeak temp.bvall temp.bamp temp.last_amp temp.ave_peak_phase temp.ave_onset_phase temp.psd_peak temp.trend_r2 0];
%                 SDF_TS.cycle_amp_data_ave{periodic+1,success+1,neuron_l} = ...
%                             [ones(size(temp.cycle_amp_data,1),1)*periodic ones(size(temp.cycle_amp_data,1),1)*success ones(size(temp.cycle_amp_data,1),1)*inf temp.cycle_amp_data];
