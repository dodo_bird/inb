function pcs_plot_aves_per_period_etc(data_struct,save_plots_flag,fpath1)

%close all

%save_plots_flag = 0;
for which_pc = 1
    if save_plots_flag==1
        f = figure('visible','off');
    else
        f = figure('visible','on');
    end
    set(f,'Position',[1 1 2400 800])
    
    for tr=1:numel(data_struct.PC)
        if data_struct.task(tr)==0
            p = 8;
        end
        if data_struct.task(tr)==1
            p = 9;
        end
        if data_struct.task(tr)==2
            p = find(data_struct.PC{tr}.period == data_struct.period_vec);
        end
        if data_struct.task(tr)==3
            p = 7;
        end
        correct = data_struct.correct(tr);
        subplot(2,9,p+(correct)*9)
        xx = data_struct.PC{tr}.data(:,which_pc); %xx=-1*xx;xx=xx-min(xx);
        plot(data_struct.PC{tr}.time,xx)
        hold on
    end
    
    for s=1:18
        subplot(2,9,s)
        ylim([-0 120])
        xlim([-7 6])
        set(gca,'XTick',-5:6)
        grid on
        hold off
        if s>9
            xlabel('Time [stim units to first tap]')
        end
        if s<7
            text(0,1.1,['PC' num2str(which_pc,'%1.0f') ', single trials, Interval=' num2str(data_struct.period_vec(mod(s-1,6)+1)*1e3,'%4.0f') ' ms, Correct=' num2str(s>9,'%1.0f')],'units','normalized')
        end
        if s==7
            text(0,1.1,['PC' num2str(which_pc,'%1.0f') ', single trials, Interval=Blocked, Correct=' num2str(s>9,'%1.0f')],'units','normalized')
        end
        if s==8
            text(0,1.1,['PC' num2str(which_pc,'%1.0f') ', single trials, Interval=Rand, Correct=' num2str(s>9,'%1.0f')],'units','normalized')
        end
        if s==9
            text(0,1.1,['PC' num2str(which_pc,'%1.0f') ', single trials, Interval=Rand-Categorical, Correct=' num2str(s>9,'%1.0f')],'units','normalized')
        end
    end
    if save_plots_flag == 1
        name_string = ['pc' num2str(which_pc,'%1.0f') '_per_trials_periods_correct_' data_struct.monkey '_' num2str(data_struct.session_id) '_' datestr(now,'yyyy-mm-dd-HHMMSS') '.jpeg'];
        f=fullfile(fpath1,name_string);
        print('-djpeg','-r100',f)
    end
end