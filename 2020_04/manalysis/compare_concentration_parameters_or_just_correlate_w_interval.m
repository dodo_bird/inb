%load('~/Desktop/inb_data/M1-TBC/sdf_and_pc_params_temp.mat');m = 1;
%load('~/Desktop/inb_data/M2-TBC/sdf_and_pc_params_temp.mat');m = 2;
%which_session = dlmread('~/Desktop/inb_data/M2-TBC/selected_sessions.csv',',',0,0);which_session = find(which_session>1);out_SDF_PC_per_trial_on_peaks{2}=out_SDF_PC_per_trial_on_peaks{2}(which_session);
if 0 % this is not useful, other than for double-checking delta to phi.
    %addpath(fullfile(home_folder,'manalysis','CircStat2012a'))
    for n=1:numel(out_SDF_PC_per_trial_on_peaks{m})
        if ~isempty(out_SDF_PC_per_trial_on_peaks{m}{n})
            c = 0;
            for p=.450:.100:.950
                c = c+1;
                deltas = out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks((out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks(:,4)==p),9);
                phis = out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks((out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks(:,4)==p),10);
                
                deltas = mod(deltas./p,1)*2*pi;
                phis = mod(phis,1)*2*pi;
                
                deltas(isnan(deltas))=[];
                phis(isnan(phis))=[];
                subplot(2,6,c)
                hist(deltas,1e2)
                subplot(2,6,c+6)
                hist(phis,1e2)
                pval = circ_ktest(deltas,phis);
                disp([p circ_mean(deltas) circ_r(deltas) circ_mean(phis) circ_r(phis) pval])
            end
            pause
        end
    end
end

if 1
    x=[];
    y=[];
    ints=[];
    for n=1:numel(out_SDF_PC_per_trial_on_peaks{m})
        if ~isempty(out_SDF_PC_per_trial_on_peaks{m}{n})
            index = logical((out_SDF_PC_per_trial_on_peaks{m}{n}.params(:,3)==2)...
                .*(out_SDF_PC_per_trial_on_peaks{m}{n}.params(:,7)==1)...
                .*~isnan(sum(out_SDF_PC_per_trial_on_peaks{m}{n}.params(:,[9 10 13 14]),2))...
                .*(out_SDF_PC_per_trial_on_peaks{m}{n}.params(:,5)==1));
            
            if sum(index)>0
                ps = out_SDF_PC_per_trial_on_peaks{m}{n}.params(index,4);
                deltas = out_SDF_PC_per_trial_on_peaks{m}{n}.params(index,9);
                phis2 = deltas./ps*2*pi;
                phis = out_SDF_PC_per_trial_on_peaks{m}{n}.params(index,10);
                bott_delt = out_SDF_PC_per_trial_on_peaks{m}{n}.params(index,13);
                bott_phis = out_SDF_PC_per_trial_on_peaks{m}{n}.params(index,14);
                ints=[ints;ps];
                x=[x;[deltas phis phis2 bott_delt bott_phis]];
                
                [r,p]=corr(ps,[deltas phis phis2 bott_delt bott_phis]);
                disp(n)
                disp([r;p])
                
                subplot(1,2,1)
                plot(ps,deltas,'bo','MarkerSize',5,'linewidth',2);
                subplot(1,2,2)
                plot(ps,phis,'ko','MarkerSize',5,'linewidth',2);
                hold on
                plot(ps,phis2,'rs','MarkerSize',5,'linewidth',2);
                hold off
                pause
            end
        end
    end
    [r,p]=corr(ints,x);
    disp([r;p])
end
%{
m=1 That's not that meaningful though. Phi is supposed to go down with
period if delta is constant. :/
    deltas    phis
r = 0.1326    0.2110
p = 0.1082    0.0101

m=2 (only sessions w/ strong signal: 1     2     4     5     6     9    10    17    18    23)
    deltas    phis      phis2     bott delt bott phi
    0.0128   -0.1058   -0.0513   -0.1654    0.1211
    0.7933    0.0301    0.2940    0.0007    0.0130
%}


if 0
    x=[];
    y=[];
    ints=[];
    for n=1:numel(out_SDF_PC_per_trial_on_peaks{m})
        if ~isempty(out_SDF_PC_per_trial_on_peaks{m}{n})
            index = logical((out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks(:,3)==2)...
                .*(out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks(:,5)==1)...
                .*~isnan(sum(out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks(:,[9 10 12 13]),2))...
                .*(out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks(:,7)==1));
            
            if sum(index)>0
                ps = out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks(index,4);
                deltas = out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks(index,9);
                phis2 = deltas./ps*2*pi;
                phis = (mod(out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks(index,10)-.5,1)-.5)*2*pi;
                [r,p]=corr(ps,[deltas phis phis2]);
                disp(n)
                disp([r;p])
                ints=[ints;ps];
                x=[x;[deltas phis phis2]];
                
                subplot(1,2,1)
                plot(ps,deltas,'o','MarkerSize',5,'linewidth',2);
                subplot(1,2,2)
                plot(ps,phis,'o','MarkerSize',5,'linewidth',2);
                hold on
                plot(ps,phis2,'rs','MarkerSize',5,'linewidth',2);
                hold off
                pause
            else
                fprintf('%s\n','No trial of this kind.')
            end
        end
    end
    [r,p]=corr(ints,x);
    disp([r;p])
end
%{
m=1
    deltas    phis      phis2
    0.0645    0.0160    0.0160
    0.0602    0.6406    0.6405
m=2
    deltas    phis      phis2
    0.0045   -0.0124   -0.0124
    0.8476    0.5949    0.5948
%}