function y = fun_additive_laplace4(mu1,mu2,mu3,mu4,b0,b1,a0,c0,c1,x)
%mus = mus - min(x);
%    y=y+1/2./(b0+b1*(x-min(x))).*exp(-abs(x-mus(n))./(b0+b1*(x-min(x))));
mus = [mu1,mu2,mu3,mu4];
y = zeros(size(x));
for n=1:numel(mus)
    y=y+1./(2*(b0+b1*x)).*exp(-abs(x-mus(n))./(b0+b1*x));
end
y = a0*y+(c0+c1*x); %y = (a0+a1*x).*y;
end