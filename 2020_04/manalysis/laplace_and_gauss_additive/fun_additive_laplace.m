function y = fun_additive_laplace(mus,b,a,x)
if isempty(mus)
    y=x*nan;
    return
end

fun = @(b1,b2,mu,x) ...
    1./(2*(b1+b2*x)).*exp(-abs(x-mu)./(b1+b2*x));

y = x*0 + fun(b(1),b(2),mus(1),x);
if numel(mus)>1
    for n=2:numel(mus)
        y=y + fun(b(1),b(2),mus(n),x);
    end
end
y = a(1)*y+(a(2)+a(3)*x);
end