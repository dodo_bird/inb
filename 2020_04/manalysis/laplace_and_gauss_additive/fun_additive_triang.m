function y = fun_additive_triang(mus,b,a,x)
if isempty(mus)
    y=x*nan;
    return
end

fun = @(b0,b1,mu,x) ...
    1./(b0+b1*x).*(abs(x-mu)<=((b0+b1*x)./2)).*(-(1./(b0+b1*x)).*abs(mu-x)+(1/2));

y=fun(b(1),b(2),mus(1),x);
if numel(mus)>1
    for n=2:numel(mus)
        y = y + fun(b(1),b(2),mus(n),x);
    end
end
y = a(1)*y+(a(2)+a(3)*x);
end