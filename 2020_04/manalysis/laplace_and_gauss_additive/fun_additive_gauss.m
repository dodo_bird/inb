function y = fun_additive_gauss(mus,sigma,a,x)
if isempty(mus)
    y=x*nan;
    return
end

fun = @(sigma1,sigma2,mu,x) ...
    1./((sigma1+sigma2*x)*(2*pi)^.5).*exp(-.5*((x-mu)./(sigma1+sigma2*x)).^2);

y=fun(sigma(1),sigma(2),mus(1),x);
if numel(mus)>1
    for n=2:numel(mus)
        y = y + fun(sigma(1),sigma(2),mus(n),x);
    end
end
y = a(1)*y+(a(2)+a(3)*x);
end