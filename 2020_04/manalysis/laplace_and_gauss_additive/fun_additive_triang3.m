function y = fun_additive_triang3(mu1,mu2,mu3,b0,b1,a0,c0,c1,x)
mus = [mu1,mu2,mu3];
y = zeros(size(x));
for n = 1:numel(mus)
    y = y + 1./(b0+b1*x).*(abs(x-mus(n))<=((b0+b1*x)./2)).*(-(1./(b0+b1*x)).*abs(mus(n)-x)+(1/2));
end
y = a0*y+(c0+c1*x); 
end