function y = fun_additive_laplace5(mu1,mu2,mu3,mu4,mu5,b0,b1,a0,c0,c1,x)
mus = [mu1,mu2,mu3,mu4,mu5];
y = zeros(size(x));
for n=1:numel(mus)
    y = y + 1./(2*(b0+b1*x)).*exp(-abs(x-mus(n))./(b0+b1*x));
end
y = a0*y+(c0+c1*x);
end