clear
sr = 1e3;
monkey = 2;
periods_vec = [.45 .55 .65 .75 .85 .95];
stim_init_tap_init = 2;
switch stim_init_tap_init
    case 1
        pre_tap_stims = 0;
        post_tap_stims = 2;
        pre_stim_time = .2;
        post_stim_time = .0;
    case 2
        pre_tap_stims = 2;
        post_tap_stims = 4;
        pre_stim_time = .5;
        post_stim_time = .0;
end


%% Prep paths
%{
addpath('C:\Users\Dobri\Desktop\inb2\manalysis')
data_folder = 'C:\Users\Dobri\Desktop\inb_data';
addpath('~/inb/inb2/manalysis')
data_folder = '/media/dobri/disk2/inb_data';
%}
addpath('~/Desktop/inb/2020_04/manalysis')
data_folder = '~/Desktop/inb_data';
base_folder = fullfile(data_folder,['M' num2str(monkey,'%1.f') '-TBC']);
cd(base_folder)

load(fullfile(base_folder,['P_stage',num2str(stim_init_tap_init,0),'.mat']))


%% These mat files are sessions w/ sorted spike times and behav events.
file_name_sessions = dir('Behav*');
file_name_sessions = file_name_sessions([file_name_sessions.isdir]);

which_session = dlmread(fullfile(base_folder,'selected_sessions.csv'),',',0,0);
which_session = find(which_session>=1);

sdftime = (-(pre_tap_stims+pre_stim_time):(1/sr):(post_tap_stims+post_stim_time))';
sdftime_zero_ind = find(sdftime==0);


if isempty(dir('LV_temp.mat'))
    LV.lv = cell(1);
    LV.conditions = [];
    LV.time=cell(1);
    LV.time_unit=cell(1);
    LV.cell_id=cell(1);
    
    sess_latest=1;
    trial = 0;
else
    load('LV_temp.mat')
    sess_latest=sess_latest+1;
    trial = trial;
end

for s = sess_latest:numel(which_session)
    fprintf('%s\n',file_name_sessions(which_session(s)).name)
    load(fullfile(file_name_sessions(which_session(s)).folder,file_name_sessions(which_session(s)).name,'mid_processing.mat'),'data_struct')
    cell_ids = data_struct.nonempty_signals_index; % + s*1e4
    for tr = 1:numel(data_struct.SDF_RAW)
        %         if (data_struct.correct(tr)~=1) || (data_struct.periodic_index(tr)~=1)
        %             continue
        %         end
        trial = trial + 1;
        LV.conditions(trial,:) = [...
            data_struct.correct(tr),...
            data_struct.periodic_index(tr),...
            data_struct.interval(tr)]; % ...
        
        cells_in_trial = [];
        SDFS_temp = [];
        SDFS_temp_time = [];
        %SDFS_temp_time_unit = [];
        for c = 1:numel(cell_ids)
            cell_found = find(data_struct.SDF_RAW{tr}.cell_ids==cell_ids(c));
            if ~isempty(cell_found) 
                cells_in_trial = horzcat(cells_in_trial,cell_ids(c)+s*1e4);
                % crop between chosen stims, relative to first stim or first tap-stim.
                switch stim_init_tap_init
                    case 1
                        if diff(data_struct.stims{tr}(1:2))>1
                            center = data_struct.stims{tr}(2);
                        else
                            center = data_struct.stims{tr}(1);
                        end
                    case 2
                        center = data_struct.center(tr);
                end
                ind1=find(data_struct.stims{tr}<=center,pre_tap_stims+1,'last' );
                ind2=find(data_struct.stims{tr}> center,post_tap_stims ,'first');
                stims = [data_struct.stims{tr}(ind1);data_struct.stims{tr}(ind2)];
                if diff(stims(1:2))>1;stims=stims(2:end);end
                if diff(stims(end-1:end))>1;stims=stims(1:end-1);end
                
                % first crop
                index = logical(...
                    (data_struct.SDF_RAW{tr}.time<(stims(end)+post_stim_time)).*...
                    (data_struct.SDF_RAW{tr}.time>(stims(1)-pre_stim_time)));
                time_cropped = data_struct.SDF_RAW{tr}.time(index);
                sdf_cropped = data_struct.SDF_RAW{tr}.sdf(index,cell_found);
                %shift_time_units = round((data_struct.center(tr) - center)/periods_vec(bpm));
                %time_unit_cropped = data_struct.SDF_RAW{tr}.time_units(index) + shift_time_units;
                
                % second crop
                [~,ind_zero] = min(abs(time_cropped-center));
                index = int16((sdftime_zero_ind-ind_zero+1):(sdftime_zero_ind-ind_zero+numel(sdf_cropped)))';
                SDFS_temp_temp = nan(size(sdftime));
                SDFS_temp_temp(index) = sdf_cropped;
                sdf_time = nan(size(sdftime));
                sdf_time(index) = (time_cropped-center);%.*data_struct.interval(tr);
                %sdf_time_unit = nan(size(sdftime));
                %sdf_time_unit(index) = time_unit_cropped;
                
                % resample again, does it matter?
                SDFS_temp_temp = resample(SDFS_temp_temp,10,round(data_struct.interval(tr)*1e2));
                sdf_time = resample(sdf_time,10,round(data_struct.interval(tr)*1e2));
        
                % horzcat raw sdfs
                SDFS_temp = horzcat(SDFS_temp,SDFS_temp_temp);
                SDFS_temp_time = horzcat(SDFS_temp_time,sdf_time);
                %SDFS_temp_time_unit = horzcat(SDFS_temp_time_unit,sdf_time_unit);
            end
        end
        
        %plot(SDFS_temp_time,SDFS_temp)
        %hold on
        %plot(data_struct.stims{tr},data_struct.stims{tr}*0,'^r','linewidth',2)
        %plot(data_struct.center(tr),10,'^b','linewidth',2)
        %hold off
        %pause
        
        index_matching_cells_across_sessions = find(sum(cell_ids_retained_c==(cells_in_trial'))==1);
        %x = (SDFS_temp./max_var_cells_c(index_matching_cells_across_sessions));
        x = SDFS_temp;
        %x = x - mu_cells_c(index_matching_cells_across_sessions);
        x = x - nanmean(x);
        p = P(:,index_matching_cells_across_sessions);
        lv = p*x';
        LV.lv{trial} = lv;
        LV.cell_id{trial} = cells_in_trial;
        LV.time{trial} = SDFS_temp_time;
        
        if 0
            fprintf('%6.3f,',LV.conditions(trial,:));fprintf('\n')
            for d=1:7
                plot(SDFS_temp_time(:,1)./data_struct.interval(tr),(lv(:,d)))
                hold on
            end
            hold off
            pause
        end
    end
    sess_latest = s;
    %save('LV_temp.mat','LV','sess_latest','trial','-v7.3')
end
