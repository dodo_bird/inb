function data_struct = sdf_raw_over_trials(data_struct)

% Raw single neuron/trial sdfs
sdfsr = 2e4;
ds = 2e1;
for tr=1:numel(data_struct.id_trials)
    fprintf('SDFS, Trial #%3.0d.\n',tr)
    lims = data_struct.lims(tr,:); %[(data_struct.trial_crop(1) + min(data_struct.stims{tr})) (data_struct.trial_crop(2) + max([max(data_struct.stims{tr})]))]; % max(data_struct.tap{tr}) No
    if numel(lims)==2
        sdf = [];
        ids_temp = [];
        %for l=1:numel(data_struct.nonempty_signals_index)
        %    [~,sdftemp] = sdf_simple(data_struct.Neurons{tr,data_struct.nonempty_signals_index(l)},lims,sdfsr,ds);
        %    if ~isempty(sdftemp)
        %        sdf = horzcat(sdf,sdftemp);
        %        ids_temp = horzcat(ids_temp,data_struct.nonempty_signals_index(l));
        %    end
        %end
        for l=1:size(data_struct.Neurons,2)
           try [~,sdftemp] = sdf_simple(data_struct.Neurons{tr,l},lims,sdfsr,ds);catch me;keyboard;end
           if ~isempty(sdftemp)
               sdf = horzcat(sdf,sdftemp);
               ids_temp = horzcat(ids_temp,l);
           end
        end
        % Make the time vector
        temp = zeros(round(diff(lims)*sdfsr),1);
        temp = temp(1:ds:end);
        sdftime = lims(1)+(1:size(temp,1))'./(sdfsr/ds);
        % Norm it, and the behav events
        [sdftime_units,stims_norm,lims_norm,time_warp_factor] = norm_time(sdftime,lims,data_struct.stims{tr},data_struct.center(tr));
        [taps_norm,~,~] = norm_time(data_struct.tap{tr},lims,data_struct.stims{tr},data_struct.center(tr));
        
        % Store everything
        data_struct.SDF_RAW{tr,1}.sdf = sdf;
        data_struct.SDF_RAW{tr,1}.cell_ids = ids_temp;
        data_struct.SDF_RAW{tr,1}.time = sdftime;
        data_struct.SDF_RAW{tr,1}.sr = 1/mean(diff(sdftime));
        data_struct.SDF_RAW{tr,1}.time_units = sdftime_units;
        data_struct.SDF_RAW{tr,1}.stims_norm = stims_norm;
        data_struct.SDF_RAW{tr,1}.time_warp_factor = time_warp_factor;
        data_struct.SDF_RAW{tr,1}.taps_norm = taps_norm;
        data_struct.SDF_RAW{tr,1}.lims_norm = lims_norm;
        data_struct.SDF_RAW{tr,1}.sps_ave = nanmean(sdf);
        data_struct.SDF_RAW{tr,1}.sps_sd = nanstd(sdf);
        
        % That's for debugging.
        if 0
            subplot(2,1,1)
            plot(data_struct.SDF_RAW{tr}.time,data_struct.SDF_RAW{tr,1}.sdf);
            hold on
            plot(data_struct.stims{tr},data_struct.stims{tr}*0,'^','LineWidth',2);
            plot(data_struct.tap{tr},data_struct.tap{tr}*0,'v','LineWidth',2);
            hold off
            
            subplot(2,1,2)
            plot(data_struct.SDF_RAW{tr}.time_units,data_struct.SDF_RAW{tr}.sdf);
            hold on
            plot(data_struct.SDF_RAW{tr}.stims_norm,data_struct.SDF_RAW{tr}.stims_norm*0,'^','LineWidth',2);
            plot(data_struct.SDF_RAW{tr}.taps_norm,data_struct.SDF_RAW{tr}.taps_norm*0,'v','LineWidth',2);
            hold off
            pause
        end
    end
end

%fr_ave_vec = nan(size(data_struct.Neurons,1),size(data_struct.Neurons,2));
fr_ave_vec = nan(size(data_struct.Neurons));
fr_sd_vec = nan(size(fr_ave_vec));
for tr=1:size(data_struct.Neurons,1)
    for l=1:size(data_struct.Neurons,2)
        ind = find(data_struct.SDF_RAW{tr}.cell_ids == l);
        if ~isempty(ind)
            try fr_ave_vec(tr,l) = data_struct.SDF_RAW{tr,1}.sps_ave(ind);catch;keyboard;end
            fr_sd_vec(tr,l) = data_struct.SDF_RAW{tr,1}.sps_sd(ind);
        end
    end
end

data_struct.fr_ave = nanmean(fr_ave_vec);
data_struct.fr_sd = nanmean(fr_sd_vec);

% if numel(data_struct.nonempty_signals_index) ~= sum(~isnan(data_struct.fr_ave))
%     fprintf('%3.0f',data_struct.nonempty_signals_index')
%     fprintf('\n')
%     fprintf('%3.0f',find(~isnan(data_struct.fr_ave)))
%     fprintf('\n')
%     data_struct.nonempty_signals_index = find(~isnan(data_struct.fr_ave))';
% end

%fprintf('%s\n','A small check for cell_id alignment after some of the pre-processing steps.')
%fprintf('%3.0f',data_struct.nonempty_signals_index'-find(~isnan(data_struct.fr_ave)))
%fprintf('\n')
