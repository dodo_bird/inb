function SDFS = aggredate_sdfs_over_trials_sessions(...
    periods_vec,file_name_sessions,which_session,pre_tap_stims,post_tap_stims,stim_init_tap_init,pre_stim_time,post_stim_time)

sr = 1e3;

sdftime = (-(pre_tap_stims+pre_stim_time):(1/sr):(post_tap_stims+post_stim_time))';
sdftime_zero_ind = find(sdftime==0);

% loops for recording sessions, condition, and bpm.
% average neuron across trials.
if isempty(dir('SDFS_temp.mat'))
    SDFS.sdf=cell(2,2,6);
    SDFS.time=cell(2,2,6);
    SDFS.time_unit=cell(2,2,6);
    SDFS.cell_id=cell(2,2,6);
    
    sess_latest=1;
else
    load('SDFS_temp.mat')
    sess_latest=sess_latest+1;
end
for s = sess_latest:numel(which_session)
    fprintf('%s\n',file_name_sessions(which_session(s)).name)
    load(fullfile(file_name_sessions(which_session(s)).folder,file_name_sessions(which_session(s)).name,'mid_processing.mat'),'data_struct')
    cell_ids = data_struct.nonempty_signals_index;
    for succ=0:1
        for periodic=1
            for bpm=1:6
                for c=1:numel(cell_ids)
                    SDFS_temp = [];
                    SDFS_temp_time = [];
                    SDFS_temp_time_unit = [];
                    for tr=1:numel(data_struct.SDF_RAW)
                        cell_found=find(data_struct.SDF_RAW{tr}.cell_ids==cell_ids(c));
                        if ~isempty(cell_found) && data_struct.correct(tr)==succ && data_struct.periodic_index(tr)==periodic && data_struct.interval(tr)==periods_vec(bpm)
                            
                            % This condition never seems to be satisfied which is a good thing.
                            if data_struct.SDF_RAW{tr}.time(1)>data_struct.stims{tr}(1)
                                fprintf('Cabrón!')
                                fprintf('%6.3f\n',data_struct.SDF_RAW{tr}.time(1)-data_struct.stims{tr}(1))
                                fprintf('%6.0f,',s,tr,cell_ids(c),succ,periodic,bpm)
                                fprintf('\n')
                            end
                            
                            % crop between chosen stims, relative to first
                            % stim or first tap-stim.
                            switch stim_init_tap_init
                                case 1
                                    if diff(data_struct.stims{tr}(1:2))>1
                                        center = data_struct.stims{tr}(2);
                                    else
                                        center = data_struct.stims{tr}(1);
                                    end
                                case 2
                                    center = data_struct.center(tr);
                            end
                            shift_time_units = round((data_struct.center(tr) - center)/periods_vec(bpm));
                            ind =find(data_struct.stims{tr}<=center,pre_tap_stims+1,'last');
                            ind2=find(data_struct.stims{tr}> center,post_tap_stims,'first');
                            stims = [data_struct.stims{tr}(ind);data_struct.stims{tr}(ind2)];
                            if diff(stims(1:2))>1;stims=stims(2:end);end
                            if diff(stims(end-1:end))>1;stims=stims(1:end-1);end
                            
                            index = logical(...
                                (data_struct.SDF_RAW{tr}.time<(stims(end)+post_stim_time)).*...
                                (data_struct.SDF_RAW{tr}.time>(stims(1)-pre_stim_time)));
                            
                            % first crop
                            time_cropped = data_struct.SDF_RAW{tr}.time(index);
                            time_unit_cropped = data_struct.SDF_RAW{tr}.time_units(index) + shift_time_units;
                            sdf_cropped = data_struct.SDF_RAW{tr}.sdf(index,cell_found);
                            
                            % second crop
                            [~,ind_zero] = min(abs(time_cropped-center));
                            index = int16((sdftime_zero_ind-ind_zero+1):(sdftime_zero_ind-ind_zero+numel(sdf_cropped)))';
                            SDFS_temp_temp = nan(size(sdftime));
                            SDFS_temp_temp(index) = sdf_cropped;
                            %->
                            sdf_time_unit = nan(size(sdftime));
                            sdf_time_unit(index) = time_unit_cropped;
                            sdf_time = nan(size(sdftime));
                            sdf_time(index) = (time_cropped-data_struct.center(tr));%.*data_struct.interval(tr);
                            
                            %plot(time_cropped,sdf_cropped)
                            %hold on
                            %plot(data_struct.stims{tr},data_struct.stims{tr}*0,'^r','linewidth',2)
                            %plot(data_struct.center(tr),10,'^b','linewidth',2)
                            %hold off
                            %pause
                            
                            % horzcat raw sdfs
                            SDFS_temp = horzcat(SDFS_temp,SDFS_temp_temp);
                            SDFS_temp_time = horzcat(SDFS_temp_time,sdf_time);
                            SDFS_temp_time_unit = horzcat(SDFS_temp_time_unit,sdf_time_unit);
                        end
                    end
                    SDFS.cell_id{succ+1,periodic+1,bpm} = horzcat(SDFS.cell_id{succ+1,periodic+1,bpm},cell_ids(c)+s*1e4);
                    SDFS.sdf{succ+1,periodic+1,bpm} = horzcat(SDFS.sdf{succ+1,periodic+1,bpm},nanmean(SDFS_temp,2));
                    SDFS.time{succ+1,periodic+1,bpm} = horzcat(SDFS.time{succ+1,periodic+1,bpm},nansum(SDFS_temp_time,2)./sum(~isnan(SDFS_temp_time),2));
                    SDFS.time_unit{succ+1,periodic+1,bpm} = horzcat(SDFS.time_unit{succ+1,periodic+1,bpm},nansum(SDFS_temp_time_unit,2)./sum(~isnan(SDFS_temp_time_unit),2));
                end
            end
        end
    end
    sess_latest = s;
    save('SDFS_temp','SDFS','sess_latest','-v7.3')
end

return