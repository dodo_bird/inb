function [sdf_time,sdf] = sdf_simple(sts,lims,varargin)

if size(varargin)<1
    sr = 24414.0625;
else
    sr = varargin{1};
end
if size(varargin)<2
    ds = 1e1; % Output sdfs downsampled to 1000 Hz.
else
    ds = varargin{2};
end
% if size(varargin)<2
%     srds = 1e3; % Output sdfs downsampled to 1000 Hz.
% else
%     srds = varargin{2};
% end
conv_win_len_ms = 50; % msec
gaussFilter = gausswin(round(conv_win_len_ms/1e3*sr));
%gaussFilter = gaussFilter./sum(gaussFilter);

% Empty vector, place spikes in it.
temp = zeros(round(diff(lims)*sr),1);
index = round(sts*sr)-round(lims(1)*sr);
if isempty(index)
    sdf=[];
    sdf_time=[];
    return
end

if round(sts(1)*sr)==round(lims(1)*sr)
    index(1) = index(1)+1;
end
if round(sts(end)*sr)==round(lims(2)*sr)
    index(end) = index(end)-1;
end
temp(index) = 1;

sdf = conv(temp,gaussFilter,'same'); % Check the sps from conv formula.
sdf = sdf(1:ds:end);
sdf_time = lims(1)+(1:size(sdf,1))'./(sr/ds);
% Too complicated trying to resample without ringing and other effects.
%[sdfds,sdf_time] = resample(sdf,lims(1)+((1:size(sdf,1))./sr)',sr,1,ds,'spline');


%{
conv_win_len_ms = 50;
sr=1e4;
gaussFilter = gausswin(round(conv_win_len_ms/1e3*sr*1));
n=8;
t=(0:(1/sr):2)';
x=zeros(numel(t),n);
for b=.5:.5:1.5
for k=1:n;
x(randi(round(sr*.1),1)'+find(t==b),k)=1;
end;
end;
clf;
for k=1:n;
line([t(find(x(:,k))) t(find(x(:,k)))]',[find(x(:,k)).*[0 0]]'+[0 1]','color',[k/n/4 .5+k/n/4 1-k/n/2]);
end;
hold on;
plot(t,conv(sum(x,2),gaussFilter,'same'),'-r');
hold off;
%}
