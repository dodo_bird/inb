%%
m = 1;
TRIALCELLNUM = [];
for n=1:size(out_SDF_PC_per_trial_on_peaks{m},2) %which_session
    if ~isempty(out_SDF_PC_per_trial_on_peaks{m}{n})
        
        temp = out_SDF_ind_cell_ind_trial_osc_params{m}{n}.params;
        temp = temp(logical((temp(:,3)==2).*(temp(:,7)==1)),:);
        
        period_vec = unique(temp(:,4))';
        trials = unique(temp(:,6));
        num_cells = zeros(size(trials));
        trials_periods = zeros(size(trials));
        for tr = 1:numel(trials)
            index = logical((temp(:,6)==trials(tr)).*(temp(:,8)==1));
            if sum(index)>0
                num_cells(tr) = sum(index);
                trials_periods(tr) = unique(temp(index,4));
            else
                num_cells(tr) = nan;
                trials_periods(tr) = nan;
            end
        end
    end
    TRIALCELLNUM = [TRIALCELLNUM;[trials_periods num_cells]];
end

scatter(TRIALCELLNUM(:,1),TRIALCELLNUM(:,2))

[r,p] = corr(TRIALCELLNUM(:,1),TRIALCELLNUM(:,2),'Rows','pairwise')
% If anything, fewer cells activated in longer periods. Hmmm.

