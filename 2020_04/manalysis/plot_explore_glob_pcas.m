%stim_init_tap_init=1;
stim_init_tap_init=2;
%% PCA in loops per condition and bpm (6x2x2)
for succ=0:1
    for periodic=1
        for bpm=1:6
            %index = sum(isnan(SDFS.sdf{succ+1,periodic+1,bpm}),2)>0;
            %tempx = SDFS.sdf{succ+1,periodic+1,bpm}(index,:);
            %time_unit=SDFS.time_unit{succ+1,periodic+1,bpm}(index,1);
            %tempt=SDFS.time{succ+1,periodic+1,bpm}(index,1);
            %[~,sc]=pca(tempx,'Rows','pairwise');
            tempx = SDFS{stim_init_tap_init}.sdf{succ+1,periodic+1,bpm};
            time_unit=SDFS{stim_init_tap_init}.time_unit{succ+1,periodic+1,bpm}(:,1);
            tempt=SDFS{stim_init_tap_init}.time{succ+1,periodic+1,bpm}(:,1);
            [~,sc]=pca(tempx);
            
            figure(1)
            plot3(time_unit,sc(:,1),sc(:,2))

            %figure(2)
            %plot([-3:5],0*[-3:5],'^r','linewidth',2)
            %hold on
            %plot(time_unit,sc(:,1:3))
            %hold off
            
            %figure(3)
            %plot(periods_vec(bpm)*[-3:5],0*[-3:5],'^r','linewidth',2)
            %hold on
            %plot(tempt,sc(:,1:4))
            %hold off
            pause
        end
    end
end

for succ=1
    for periodic=1
        for bpm=1:6
            tempx = SDFS{stim_init_tap_init}.sdf{succ+1,periodic+1,bpm};
            time_unit=SDFS{stim_init_tap_init}.time_unit{succ+1,periodic+1,bpm}(:,1);
            tempt=SDFS{stim_init_tap_init}.time{succ+1,periodic+1,bpm}(:,1);
            [~,sc]=pca(tempx);

            figure(succ+1)
            subplot(2,3,bpm)
            x=zeros(size(sc(:,1:3)));
            for d=1:3
                x(:,d)=smooth(sc(:,d),1e2);
            end
            plot_3d_cool_color(time_unit,x,{'PC1','PC2','PC3'})
            hold on
            stims = diff(mod(time_unit,1))<0;
            first = find(~isnan(time_unit),1,'first');
            plot3(x(first,1),x(first,2),x(first,3),'^g','MarkerSize',15,'linewidth',3)
            plot3(x(stims,1),x(stims,2),x(stims,3),'or','MarkerSize',15,'linewidth',3)
            hold off
        end
    end
end
