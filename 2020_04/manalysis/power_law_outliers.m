function [n_dims_removed,outlier_components_best_GMM] = power_law_outliers(PC,plotting_testing_distr_clusters)
%
% Do this by fitting a GMM and then apply the following criterion

max_clusters = 5;
max_components_to_remove = 5;
%plotting_testing_distr_clusters = 1;

outlier_components_best_GMM = [];
for n_dims_removed=0:max_components_to_remove
    R2flat=[];
    for tr=1:numel(PC)
        if numel(PC{tr}.r2)>=10
            PC{tr}.rank = (1:numel(PC{tr}.r2))';
            PC{tr}.data = PC{tr}.data(:,1:3);
            
            y = PC{tr}.r2((1+n_dims_removed):round(.5*numel(PC{tr}.r2)));
            x = PC{tr}.rank((1+n_dims_removed):round(.5*numel(PC{tr}.rank)));
            try b = [x.^0 log(x)]\log(y);catch me;keyboard;end
            y_flat = log10(y) - log10(exp(b(1)).*x.^b(2));
            R2flat = vertcat(R2flat,[x y_flat]);
        end
    end
    
    try E = evalclusters(R2flat(:,2),'kmeans','silhouette','klist',1:max_clusters,'ClusterPriors','equal');catch me;keyboard;end
    n_Gaussians_optimal=E.OptimalK;
    switch n_Gaussians_optimal
        case 2
            C = [0 .2]';
            [~,centroids] = kmeans(R2flat(:,2),n_Gaussians_optimal,'Start',C);
        case 3
            C = [-.2 0 .2]';
            [~,centroids] = kmeans(R2flat(:,2),n_Gaussians_optimal,'Start',C);
        otherwise
            [~,centroids] = kmeans(R2flat(:,2),n_Gaussians_optimal);
    end
    
    sd = std(R2flat(:,2));
    if plotting_testing_distr_clusters == 1
        subplot(2,1,1)
        plot(R2flat(:,1),R2flat(:,2),'sk')
        
        subplot(2,1,2)
        [c,pdfx]=hist(R2flat(:,2),1e2);
        plot(pdfx,c./sum(c))
        hold on

        %disp([centroids' 2*sd])
        %disp(abs(centroids)'>2*sd)
        %pause
    end
    
    AIC = zeros(1,max_clusters);
    GMModels = cell(1,max_clusters);
    for k = 1:max_clusters
        options = statset('MaxIter',400);
        skip_plotting = false;
        try 
            GMModels{k} = fitgmdist(R2flat(:,2),k,'Options',options);
        catch
            skip_plotting = true;
            GMModels{k}.AIC = inf;
            GMModels{k}.mu = 0;
        end
        AIC(k)= GMModels{k}.AIC;
    
        if plotting_testing_distr_clusters==1 && ~skip_plotting
            pdfy = pdf(GMModels{k},pdfx');
            plot(pdfx,pdfy./sum(pdfy))
            %GMModels{k}.display
            %pause
        end
    end
    [~,n_Gaussians_selected] = min(AIC);
    
    if plotting_testing_distr_clusters==1
        hold off
        %disp([n_dims_removed n_Gaussians_selected])
        pause
    end
    
    % The rule is, Less than two of the centers are farther than .01 from zero.
    rule_positive_centroids = sum(centroids>0)<2;
    rule_with_3_Gaussians = sum(GMModels{3}.mu>.01)<2;
    %rule_with_n_Gaussians = sum(GMModels{n_Gaussians_selected}.mu>.01)<2;
    rule_with_n_Gaussians = sum(GMModels{n_Gaussians_selected}.mu'>(2*sd))==0;
    outlier_components_best_GMM(n_dims_removed+1,:) = ...
        [n_dims_removed rule_positive_centroids n_Gaussians_optimal rule_with_3_Gaussians 3 ...
        rule_with_n_Gaussians n_Gaussians_selected];
end

%if plotting_testing_distr_clusters==1
fprintf('%10s,%10s,%10s,%10s,%10s,%10s,%10s\n','dims_out','crit1','n_mix_opt','crit2','n_three','crit3','n_mix_aic')
fprintf('%10.0f%10.0f%10.0f%10.0f%10.0f%10.0f%10.0f\n',outlier_components_best_GMM')
%end

% The minimum number of dims to remove to get rid of all outliers.
n_dims_removed = min(find(outlier_components_best_GMM(:,6),1,'first')-1);
% n_dims_removed = min([find(outlier_components_best_GMM(:,6),1,'first')-1 ...
%     find(outlier_components_best_GMM(:,7)==2,1,'first')-1]);
