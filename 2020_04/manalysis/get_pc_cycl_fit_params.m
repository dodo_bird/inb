function [SDF_PC_trial_fit,SDF_PC_trial_peaks,SDF_PC_which_ramping] = get_pc_cycl_fit_params(PC,data_struct,fpath1,plotting_flag,save_plots_flag,fpath_dump_summary_pics)

SDF_PC_which_ramping.param_labels(1:8) = {'sessionID','pc-num','task','periodic','correct','trial','trial-phase','model_selected'};
SDF_PC_which_ramping.params = [];

SDF_PC_trial_fit.param_labels(1:7) = {'sessionID','pc-num','task','periodic','correct','trial','trial-phase'};
SDF_PC_trial_fit.params = [];

SDF_PC_trial_peaks.param_labels(1:7) = {'sessionID','pc-num','task','periodic','correct','trial','trial-phase'};
SDF_PC_trial_peaks.params = [];

SDF_PC_trial_peaks.raw_peaks_labels = {'sessionID','pc-num','task',...
    'periodic','correct-trials','trial','trial-phase',...
    'event_num','delta','theta','amp','delta-rise','theta-rise',...
    'delta-bottom','amp-bottom','delta-pseudo'};
SDF_PC_trial_peaks.raw_peaks = [];


%%
row=0;
for tr = 1:numel(PC)
    if plotting_flag == 1
        figure(1);clf;figure(2);clf
    end
    cycles_trial_phases = [-inf data_struct.center(tr)+.5; data_struct.center(tr) inf];
    for trial_phase = [1 2]
        cycles_start_past_zero_stim = cycles_trial_phases(trial_phase,1);
        cycles_stop_past_zero_stim  = cycles_trial_phases(trial_phase,2);
        for which_pc_to_analyze = 1%:3
            row=row+1;
            SDF_PC_trial_fit.params(row,1:18) = nan;
            SDF_PC_trial_fit.params(row,1:7) = ...
                [PC{tr}.session_id which_pc_to_analyze data_struct.task(tr) PC{tr}.period PC{tr}.correct tr trial_phase];
            SDF_PC_trial_peaks.params(row,1:17) = nan;
            SDF_PC_trial_peaks.params(row,1:7) = ...
                [PC{tr}.session_id which_pc_to_analyze data_struct.task(tr) PC{tr}.period PC{tr}.correct tr trial_phase];
            
            fprintf('%6.0f,',SDF_PC_trial_fit.params(row,[2:3 5:7]))
            fprintf('%10.3f,',PC{tr}.period,nanmean(PC{tr}.data(:,which_pc_to_analyze)))
            fprintf('\n')
            if ~isempty(PC{tr}.data) && size(PC{tr}.data,1)>1e3
                
                xx = PC{tr}.data(:,which_pc_to_analyze);
                tt = PC{tr}.time;
                xx=xx(logical((tt < cycles_stop_past_zero_stim).*(tt > cycles_start_past_zero_stim)),:);
                tt=tt(logical((tt < cycles_stop_past_zero_stim).*(tt > cycles_start_past_zero_stim)));
                tt=tt(1:1:end);
                xx=xx(1:1:end,:);
                xx=xx-min(xx);
                xx(isnan(xx))=0;
                
                if (sum(xx==0)/numel(xx))<.3
                    % Fod debugging. plotting_flag = (data_struct.task(tr)==2);
                    if plotting_flag == 1
                        figure(1)
                        %subplot(2,3,which_pc_to_analyze+(trial_phase-1)*3)
                    end
                    % 1. Fit a cycloid-like function and get params.
                    period = PC{tr}.period;
                    if isnan(period);period=1;end
                    if any(data_struct.task(tr)==[2 3])
                        cyclofit_cont = ts_continuous_fit_of_sdf(xx,tt-data_struct.center(tr),plotting_flag,period);
                        SDF_PC_trial_fit.params(row,8:end) = cyclofit_cont.cont_nl_regression;
                    end
                    
                    if plotting_flag == 1
                        figure(2)
                        %subplot(2,3,which_pc_to_analyze+(trial_phase-1)*3)
                    end
                    
                    % 2. Cycle-by-cycle analysis of peaks. % data_struct.SDF_RAW{1}.time_warp_factor
                    %threshold_crossing = PC{1}.fr_ave(which_pc_to_analyze)+PC{1}.fr_sd(which_pc_to_analyze);
                    threshold_coef = .1;
                    threshold_crossing = PC{1}.fr_median(which_pc_to_analyze)+threshold_coef*PC{1}.fr_range(which_pc_to_analyze);
                    peaks_stats = ts_of_sdf_peak_params(xx,tt-data_struct.center(tr),plotting_flag,data_struct.SDF_RAW{tr}.sr,data_struct.stims{tr}-data_struct.center(tr),[],threshold_crossing);
                    SDF_PC_trial_peaks.params(row,8:17) = peaks_stats.params([5 1:4 6:10]);
                    % Concatenate ind cycle params in a long table.
                    if ~isempty(peaks_stats.raw_data)
                        switch trial_phase
                            case 1
                                event_num = (((size(peaks_stats.raw_data,1)-1)*-1):0)';
                            case 2
                                event_num = (1:size(peaks_stats.raw_data,1))';
                        end
                        SDF_PC_trial_peaks.raw_peaks = vertcat(SDF_PC_trial_peaks.raw_peaks,...
                            [repmat(SDF_PC_trial_fit.params(row,1:7),size(peaks_stats.raw_data,1),1) event_num peaks_stats.raw_data peaks_stats.raw_data(:,end-3)./2/pi*PC{tr}.period]);
                    end
                    
                    % 3. Laplace/Gaussian/Triangle (ramping) distributions;
                    if 0
                        stims = data_struct.stims{tr};
                        if trial_phase==1
                            stims(stims>data_struct.center(tr))=[];
                        else
                            stims(stims<data_struct.center(tr))=[];
                        end
                        stims = stims - data_struct.center(tr);
                        if (numel(stims)>2) && (numel(stims)<6) && any(data_struct.task(tr)==[2 3])
                            best_fit = ts_continuous_fit_of_sdf_laplace_pdfs(xx,tt-data_struct.center(tr),1,stims);
                            SDF_PC_which_ramping.params(row,8) = best_fit;
                            SDF_PC_which_ramping.params(row,1:7) = SDF_PC_trial_fit.params(row,1:7);
                        end
                    end
                else
                    SDF_PC_trial_fit.params(row,8:end) = nan;
                    SDF_PC_trial_peaks.params(row,8:17) = nan;
                    SDF_PC_which_ramping.params(row,8) = nan;
                end
            else
                SDF_PC_trial_fit.params(row,8:end) = nan;
                SDF_PC_trial_peaks.params(row,8:17) = nan;
                SDF_PC_which_ramping.params(row,8) = nan;
            end
        end
    end
    if ~isempty(PC{tr}.data) && size(PC{tr}.data,1)>1e3
        if plotting_flag == 1 && save_plots_flag==0
            pause
        end
        if plotting_flag == 1 && save_plots_flag==1
            figure(1)
            set(gcf,'Position',[1,1,1920,1280])
            f = fullfile(fpath1,'pics/',['cycloid_fit_pcs_correct' num2str(PC{tr}.correct,'%.0f') '_per' num2str(PC{tr}.period*1e2,'%.0f') '_trial' num2str(tr,'%03.0f') '_' datestr(now,'yyyy-mm-dd-HHMMSS') '.png']);
            print(f, '-dpng', '-r50');
            
            figure(2)
            set(gcf,'Position',[1,1,1920,1280])
            f = fullfile(fpath1,'pics/',['peaks_pcs_correct' num2str(PC{tr}.correct,'%.0f') '_per' num2str(PC{tr}.period*1e2,'%.0f') '_trial' num2str(tr,'%03.0f') '_' datestr(now,'yyyy-mm-dd-HHMMSS') '.png']);
            print(f, '-dpng', '-r50');
        end
    end
end
SDF_PC_trial_fit.param_labels(8:18) = cyclofit_cont.cont_nl_regression_labels;
SDF_PC_trial_fit.param_labels(19:21) = {'nan','\Delta2','nan'};
SDF_PC_trial_fit.params(:,19:21) = nan;
SDF_PC_trial_fit.params(:,20) = SDF_PC_trial_fit.params(:,15)./2/pi.*SDF_PC_trial_fit.params(:,4);

SDF_PC_trial_peaks.param_labels(8:17) = peaks_stats.labels([5 1:4 6:10]);


%% Regress params of peaks aggregated across all trials.
% Regress all?
regress_these_raw_peak_vars = [9:11 15];
sess = unique(SDF_PC_trial_peaks.raw_peaks(:,1));
SDF_PC_trial_peaks.raw_peaks_fits.table = cell(1,numel(regress_these_raw_peak_vars));
SDF_PC_trial_peaks.raw_peaks_fits.dv = cell(1,numel(regress_these_raw_peak_vars));
SDF_PC_trial_peaks.raw_peaks_fits.labels = ...
    {'sessionID','pc-num','trial-phase',...
    'reg-int-coef','reg-per-coef','reg-correct-coef','reg-order-coef','reg-rand-coef'...
    'reg-int-p','reg-per-p','reg-correct-p','reg-order-p','reg-rand-p'};
which_pc = 1;
for s = 1:numel(sess)
    for trial_phase = [1 2]
        index = logical((SDF_PC_trial_peaks.raw_peaks(:,1)==sess(s)).*...
            (SDF_PC_trial_peaks.raw_peaks(:,7)==trial_phase).*...
            (SDF_PC_trial_peaks.raw_peaks(:,2)==which_pc));
        if sum(index)>20
            % In some sessions there were not any incorrect trials.
            no_correct_range = numel(unique(SDF_PC_trial_peaks.raw_peaks(index,5)))==1;
            no_rand = sum(SDF_PC_trial_peaks.raw_peaks(index,3)<2)==0;
            x=[SDF_PC_trial_peaks.raw_peaks(index,[4 5 8]) ...
                SDF_PC_trial_peaks.raw_peaks(index,3)>1];
            if no_correct_range
                if no_rand
                    pred_cols = [1 3];
                else
                    pred_cols = [1 3 4];
                end
            else
                if no_rand
                    pred_cols = [1 2 3];
                else
                    pred_cols = [1 2 3 4];
                end
            end
            for dv=1:numel(regress_these_raw_peak_vars)
                %x(isnan(x),1)=.65;
                y = SDF_PC_trial_peaks.raw_peaks(index,regress_these_raw_peak_vars(dv));
                lm=fitlm(x(:,pred_cols),y,'linear');
                betas_temp = lm.Coefficients.Estimate'.*(lm.Coefficients.pValue'<.01)*(lm.coefTest<.05);
                ps_temp = lm.Coefficients.pValue'.^(lm.coefTest<.05);
                betas = nan(1,5);
                ps = nan(1,5);
                betas([1 1+pred_cols]) = betas_temp;
                ps([1 1+pred_cols]) = ps_temp;
                coefs = [sess(s) which_pc trial_phase betas ps];
                SDF_PC_trial_peaks.raw_peaks_fits.dv{dv} = SDF_PC_trial_peaks.raw_peaks_labels{regress_these_raw_peak_vars(dv)};
                SDF_PC_trial_peaks.raw_peaks_fits.table{dv} = vertcat(SDF_PC_trial_peaks.raw_peaks_fits.table{dv},coefs);
            end
        end
    end
end


%% Visualize summary stats.
if plotting_flag == 2
    close all
    figure('Position',[10 50 1600 900])
    subp = 0;
    for pc=1%:3
        for task = 0:3
            for trial_phase=1:2
                for dv = [9 12 14]
                    subp = subp + 1;
                    index = (SDF_PC_trial_fit.params(:,2) == pc).*...
                        (SDF_PC_trial_fit.params(:,3) == task).*...
                        (SDF_PC_trial_fit.params(:,7) == trial_phase).*...
                        (~isnan(SDF_PC_trial_fit.params(:,dv)));
                    index = logical(index);
                    subplot(4,2*3,subp)
                    if any(task==[0 1])
                        ivs = 5;
                    else
                        ivs = [4 5];
                    end
                    boxplot(SDF_PC_trial_fit.params(index,dv),SDF_PC_trial_fit.params(index,ivs));
                    title(SDF_PC_trial_fit.param_labels{dv})
                end
            end
        end
    end
    if save_plots_flag == 1
        f = fullfile(fpath_dump_summary_pics,['pc_cyclofit_ave_params_' data_struct.monkey '_' data_struct.session_id '_' datestr(now,'yyyy-mm-dd-HHMMSS') '.png']);
        print(f, '-dpng', '-r100');
    end
    
    for trial_phase=1:2
        figure('Position',[10 50 1600 900])
        subp = 0;
        for task = 0:3
            for dv = 8:16
                for pc=1
                    subp = subp + 1;
                    subplot(4,9,subp)
                    index = (SDF_PC_trial_peaks.params(:,2) == pc).*...
                        (SDF_PC_trial_peaks.params(:,3) == task).*...
                        (SDF_PC_trial_peaks.params(:,7)==trial_phase).*...
                        (~isnan(SDF_PC_trial_peaks.params(:,dv)));
                    index = logical(index);
                    if sum(index)>3
                        if any(task==[0 1])
                            ivs = 5;
                        else
                            ivs = [4 5];
                        end
                        boxplot(SDF_PC_trial_peaks.params(index,dv),SDF_PC_trial_peaks.params(index,ivs),...
                            'Colors',[1 0 1]./trial_phase,'PlotStyle','compact');
                    end
                end
                title(SDF_PC_trial_peaks.param_labels{dv})
            end
        end
        if save_plots_flag == 1
            f = fullfile(fpath_dump_summary_pics,['pc_peaks_ave_params_' data_struct.monkey '_' data_struct.session_id '_trial_part' num2str(trial_phase,'%1.0f') '_' datestr(now,'yyyy-mm-dd-HHMMSS') '.png']);
            print(f, '-dpng', '-r100');
        end
    end
end
end
