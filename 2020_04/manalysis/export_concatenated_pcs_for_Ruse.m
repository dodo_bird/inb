%% Prep paths
clear
switch computer
    case 'PCWIN64'
        home_folder = 'C:\Users\Dobri\Desktop\inb2\';
        data_folder = 'C:\Users\Dobri\Desktop\inb_data';
    case 'GLNXA64'
        home_folder = '~/inb/inb2/';
        data_folder = '/media/dobri/disk2/inb_data';
end
addpath(fullfile(home_folder,'manalysis','feb2020'))


%%
% m = 2;
base_folder = fullfile(data_folder,'M2-TBC');
cd(base_folder)
% These mat files are sessions w/ sorted spike times and behav events.
file_name_sessions = dir('Behav*.mat');

% Don't use all sessions. The signal in some is just too poor.
% which_session = 1:numel(file_name_sessions);
which_session = dlmread(fullfile(base_folder,'selected_sessions.csv'),',',0,0);
which_session = find(which_session>1);

ds = 1e1;

PC1.xx = cell(1*6,1);
PC1.tt = cell(1*6,1);
PC1.stims = cell(1*6,1);
PC1.session = cell(1*6,1);
PC1.trial = cell(1*6,1);

for n=1:numel(which_session)
    fpath_dump_data = fullfile(base_folder,file_name_sessions(which_session(n)).name(1:end-4));
    load(fullfile(fpath_dump_data,'mid_processing.mat'),'data_struct')
    
    % Only use data from trials in task 2, correct response, per period.
    for task = 2
        for correct = 1
            for period = 1:6
                fprintf('%s%5.0f%s%4.3f\n','Session #',which_session(n),', Period ',data_struct.period_vec(period))
                for tr = 1:numel(data_struct.PC)
                    if data_struct.interval(tr)==data_struct.period_vec(period) && ...
                            data_struct.task(tr)==task && ...
                            data_struct.correct(tr)==correct
                        
                        stims = data_struct.stims{tr}(data_struct.stims{tr}<=data_struct.center(tr));
                        stims = flipud(stims);stims = stims(1:min([end,4]));stims = flipud(stims);
                        index = logical(...
                            (data_struct.PC{tr}.time<(data_struct.center(tr)+.4)).*...
                            (data_struct.PC{tr}.time>(stims(1)-.2)));
                        
                        if sum(index)>1e3
                            xx = data_struct.PC{tr}.data(index,1);
                            tt = data_struct.PC{tr}.time(index,1);
                            tt = tt - data_struct.center(tr);
                            % Also, ds one 1e1 further.
                            PC1.xx{period} = vertcat(PC1.xx{period},xx(1:ds:end));
                            PC1.tt{period} = vertcat(PC1.tt{period},tt(1:ds:end));
                            
                            % Make a pseudo-stim series. Concat all, then unique().
                            %pseudo_stims = flipud((0:-data_struct.period_vec(period):(data_struct.stims{tr}(1)-data_struct.center(tr)))');
                            pseudo_stims = flipud((0:-data_struct.period_vec(period):min(tt))');
                            PC1.stims{period} = vertcat(PC1.stims{period},pseudo_stims);
                            
%                             stims = data_struct.stims{tr}(data_struct.stims{tr}<=data_struct.center(tr));
%                             stims = stims - data_struct.center(tr);
%                             disp(stims')
%                             plot(tt,xx,'-k',stims,stims*0,'vr')
%                             pause
                            
                            PC1.session{period} = vertcat(PC1.session{period},tt(1:ds:end)*0+str2double(data_struct.session_id));
                            PC1.trial{period} = vertcat(PC1.trial{period},tt(1:ds:end)*0+tr);
                        end
                    end
                end
                PC1.stims{period} = unique(PC1.stims{period});
                PC1.stims{period} = sort(PC1.stims{period});
                removeind = [];
                for s=2:numel(PC1.stims{period})
                    if abs(PC1.stims{period}(s)-PC1.stims{period}(s-1))<.001
                        removeind = vertcat(removeind,s);
                    end
                end
                if ~isempty(removeind)
                    PC1.stims{period}(removeind) = [];
                end
                disp(PC1.stims{period}');
            end
        end
    end
    clear data_struct
    whos
    %pause
end

f = fullfile(base_folder,['full_sdfs_pop_pc1_diva-all_sessions_' ...
    datestr(now,'yyyymmdd-HHMMSS') '.mat']);
save(f,'PC1')
%clear PC1
