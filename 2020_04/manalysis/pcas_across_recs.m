%%
clear
monkey = 2;
stim_init_tap_init = 2;
periods_vec = [.45 .55 .65 .75 .85 .95];


%% Prep paths
%{
lib_folder = 'C:\Users\Dobri\Desktop\inb2\manalysis';
data_folder = 'C:\Users\Dobri\Desktop\inb_data';
lib_folder = '~/inb/inb2/manalysis';
data_folder = '/media/dobri/disk2/inb_data';
%}
lib_folder = '~/Desktop/inb/2020_04/manalysis';
data_folder = '~/Desktop/inb_data';
addpath(lib_folder)


%% Choose monkey
base_folder = fullfile(data_folder,['M' num2str(monkey,'%1.f') '-TBC']);
cd(base_folder)


%% These mat files are sessions w/ sorted spike times and behav events.
file_name_sessions = dir('Behav*');
file_name_sessions = file_name_sessions([file_name_sessions.isdir]);


w=dir(fullfile(base_folder,'selected_sessions.csv'));
if ~isempty(w)
    which_session = dlmread(fullfile(w.folder,w.name),',',0,0);
    which_session = find(which_session>=1);
else
    which_session = (1:numel(file_name_sessions))';
end


%% Trial time limits
switch stim_init_tap_init
    case 1
        pre_tap_stims = 0;
        post_tap_stims = 2;
        pre_stim_time = .2;
        post_stim_time = .0;
    case 2
        pre_tap_stims = 3;
        post_tap_stims = 4;
        pre_stim_time = .5;
        post_stim_time = .0;
end
SDFS = aggredate_sdfs_over_trials_sessions(...
    periods_vec,file_name_sessions,which_session,pre_tap_stims,post_tap_stims,stim_init_tap_init,pre_stim_time,post_stim_time);


%% load(['SDFS_aggregate_stage' num2str(stim_init_tap_init,0) '.mat'])


%% get the max # of cells.
found_cells = [];
for counter = 1:numel(SDFS.cell_id)
    found_cells = horzcat(found_cells,SDFS.cell_id{counter});
end
% for succ = 0:1
%     for periodic = 1
%         for bpm=1:numel(periods_vec)
%             found_cells = horzcat(found_cells,SDFS.cell_id{succ+1,periodic+1,bpm});
%         end
%     end
% end
found_cells = unique(found_cells);


%% Resample
sr = 1000; % assume that's what the previous SDF scripts used.
SDF_huge = [];
var_cells = found_cells*nan;
for counter = 7:numel(SDFS.sdf)
    % Resample per interval and concat. This will give 50 samples per stimulus interval.
    sdfds = resample([nan(1,size(SDFS.sdf{counter},2));SDFS.sdf{counter};nan(1,size(SDFS.sdf{counter},2))],5,round(SDFS.bpm(counter)*1e2));
    sdfds_temp = nan(size(sdfds,1),numel(found_cells));
    for c = 1:size(sdfds,2)
        ind = find(SDFS.cell_id{counter}(c)==found_cells);
        if ~isempty(ind)
            var_cells(counter,ind) = nanvar(sdfds(:,c));
            sdfds_temp(:,ind) = sdfds(:,c);
        end
    end
    SDF_huge = [SDF_huge;sdfds_temp];
end


%% Remove totally empty cells and periods
select_these = ~isnan(nanmean(SDF_huge,1));
cell_ids_retained_c = found_cells(select_these);
max_var_cells_c = max(var_cells(:,select_these));
max_var_cells_c = max([max_var_cells_c;max_var_cells_c*0+1]);
SDF_huge_c = SDF_huge(~isnan(nanmean(SDF_huge,2)),select_these);
SDF_huge_c(sum(isnan(SDF_huge_c),2)./size(SDF_huge_c,2)>.01,:) = [];
SDF_huge_c(isnan(SDF_huge_c)) = 0;


%% More cropping. all cells w/ at least 1 nan
% switch stim_init_tap_init
%     case 1
%         morenans = find(sum(isnan(SDF_huge_c))>0);
%     case 2
%         % ?
%         morenans = [];
%         for n=1:size(SDF_huge_c,1)
%             morenans = horzcat(morenans,find(isnan(SDF_huge_c(n,:))));
%         end
%         morenans = unique(morenans);
% end
% cell_ids_retained_c(morenans)=[];
% max_var_cells_c(morenans)=[];
% SDF_huge_c(:,morenans)=[];


%% Normalize: softmax, divide by the max variance from all conditions.
SDF_huge_c = SDF_huge_c./max_var_cells_c;
SDF_huge_c = SDF_huge_c - nanmean(SDF_huge_c);


%% Holy grail
% [P,sc,~,~,explained_c,mu_cells_c] = pca(SDF_huge_c(1:10:end,:),'algorithm','als');
[P,sc,~,~,explained_c,mu_cells_c] = pca(SDF_huge_c);
% [P,sc,~,~,explained_c,mu_cells_c] = pca(SDF_huge_c,'Rows','pairwise'); % by now it's the same deal cause we've cleaned the sdf mat a lot.


%% Save
%{
save(fullfile(base_folder,['P_stage',num2str(stim_init_tap_init,0),'.mat']),...
    'P','cell_ids_retained_c','max_var_cells_c','mu_cells_c','explained_c')
%}


%% Load again
load(fullfile(base_folder,['P_stage',num2str(stim_init_tap_init,0),'.mat']))
load(['SDFS_aggregate_stage' num2str(stim_init_tap_init,0) '.mat'])


%% Project neurons over population activity. First, use averaged neurons across trials in the same condition.
counter_plus_session = 0;
LV.lv = cell(numel(SDFS),1);
LV.cell_id = cell(numel(SDFS),1);
LV.time = cell(numel(SDFS),1);
for counter = 7:numel(SDFS.sdf)
    session_ids = unique(round(SDFS.cell_id{counter}./1e4));
    for s = 1:numel(session_ids)
        
        index_matching_cells_in_session_for_SDF = find(round(SDFS.cell_id{counter}./1e4) == session_ids(s));
        cells_in_session = SDFS.cell_id{counter}(index_matching_cells_in_session_for_SDF);
        index_matching_cells_in_session_for_P = find(sum(cell_ids_retained_c==(cells_in_session'),1)==1);
        
        x = SDFS.sdf{counter}(:,index_matching_cells_in_session_for_SDF);
        index_nan_all = (sum(isnan(x),2)./size(x,2))>.01;
        x = x(~index_nan_all,:);
        x = resample(x,5,round(SDFS.bpm(counter)*1e2)); % resample(x,10,round(SDFS.bpm(counter)*1e2));
        %x = x./max_var_cells_c(index_matching_cells_in_session_for_P);
        x = x - mu_cells_c(index_matching_cells_in_session_for_P);
        %x = x - nanmean(x);
        
        
        t = SDFS.time{counter}(:,index_matching_cells_in_session_for_SDF(1));
        t = linspace(min(t),max(t),size(x,1))';
        %t = t(~index_nan_all,:);
        %t = resample(t,10,round(SDFS.bpm(counter)*1e2));
        
        %p = P(index_matching_cells_in_session_for_P,:);
        %lv = (p\x')'; % plot(lv(:,1:6))

        %p = P(:,index_matching_cells_in_session_for_P);
        %lv = (p*x')';
        p = P(index_matching_cells_in_session_for_P,:);
        lv = (p'*x')';
        
        fprintf('%6.0f,',SDFS.succ(counter),SDFS.periodic(counter),...
            SDFS.bpm(counter)*1e3,...
            session_ids(s),size(lv,1),size(lv,2));
        fprintf('\n')
        if 0
            figure(1)
            for d=1:6
                plot(t./SDFS.bpm(counter),smooth(lv(:,d),1e1),'linewidth',2)
                hold on
            end
            hold off
            set(gca,'color','k')
            
            %             figure(2)
            %             smlv = zeros(size(lv,1),3);
            %             c = 0;
            %             for d=3:5
            %                 c = c + 1;
            %                 smlv(:,c)=smooth(lv(:,c),1e1);
            %             end
            %             plot_3d_cool_color(t,smlv)
            
            pause
        end
        
        counter_plus_session = counter_plus_session + 1;
        LV.conditions(counter_plus_session,:) = ...
            [SDFS.succ(counter),SDFS.periodic(counter),...
            SDFS.bpm(counter)];
        LV.lv{counter_plus_session,1} = lv;
        LV.cell_id{counter_plus_session,1} = cells_in_session;
        LV.time{counter_plus_session,1} = t;
    end
end


LVave.conditions = zeros(numel(periods_vec)*1,3);
LVave.lv = cell(numel(periods_vec)*1,1);
LVave.cell_id = cell(numel(periods_vec)*1,1);
LVave.time = cell(numel(periods_vec)*1,1);
counter = 0;
for succ = 1 %0:1
    for p = 1 %0:1
        for i = 1:numel(periods_vec)
            counter = counter + 1;
            min_t = nan;
            max_t = nan;
            dt = nan;
            for l = 1:numel(LV.time)
                if all(LV.conditions(l,:) == [succ p periods_vec(i)])
                    min_t = max(min_t,min(LV.time{l}));
                    max_t = min(max_t,max(LV.time{l}));
                    dt = min(dt,mean(diff(LV.time{l})));
                end
            end
            
            LVave.conditions(counter,:) = [succ p periods_vec(i)];
            LVave.time{counter} = (min_t:dt:max_t)';
            LVave.lv{counter} = zeros(size(LVave.time{counter},1),size(LV.lv{1},2));
            for t = 1:numel(LVave.time{counter})
                n = 0;
                for l = 1:numel(LV.time)
                    if all(LV.conditions(l,:) == [succ p periods_vec(i)])
                        n = n + 1;
                        [~,ind] = min(abs(LVave.time{counter}(t)-LV.time{l}));
                        LVave.lv{counter}(t,:) = LVave.lv{counter}(t,:) + LV.lv{l}(ind,:);
                    end
                end
                LVave.lv{counter}(t,:) = LVave.lv{counter}(t,:)./n;
            end
        end
    end
end

for l=1:6
    stims = find(diff(mod(LVave.time{l}./LVave.conditions(l,3),1))<-.5);
    subplot(2,3,l)
    plot_3d_cool_color(LVave.time{l},LVave.lv{l}(:,1:3),1,10,stims);
end

% That's the beautiful graph!
% But is it just a way to return at the mean, or power of the original signal?
colorvec = cool(size(LVave.lv{1},2))*.9;
for first_n_lvs = 10:50:size(LVave.lv{1},2)
    for l=1:numel(LVave.time)
        subplot(2,3,l)
        plot(LVave.time{l}(2:end-1)./LVave.conditions(l,3),dot(LVave.lv{l}(2:end-1,1:first_n_lvs),LVave.lv{l}(2:end-1,1:first_n_lvs),2).^.5,'color',colorvec(first_n_lvs,:))
        %plot(LVave.time{1}./.450,std(LVave.lv{1},1,2))
        ylim([3 8])
        hold on
    end
end
for l=1:numel(LVave.time)
    subplot(2,3,l)
    hold off
    set(gca,'color',ones(1,3)*.2)
    set(gcf,'color',ones(1,3)*.1)
    ax=gca;
    ax.XColor=ones(1,3)*.9;
    ax.YColor=ones(1,3)*.9;
    ax.ZColor=ones(1,3)*.9;
end

