function SDFS = aggredate_sdfs_over_trials_sessions_2021(file_name_sessions)

% these are the conditions that define how to group trials. at the end of
% this, each "trial" in the output array will stand for the average of all
% trials satisfying the given conditions.
intervals_vec = [.45 .55 .65 .75 .85 .95];
succ_vec = [0 1];
periodic_cond_vec = 1;

if ~isempty(dir('SDFS_aggregate_stage.mat'))
    load('SDFS_aggregate_stage.mat','SDFS','sess_latest')
    sess_latest=sess_latest+1;
else
    sess_latest=1;

    % loops for recording sessions, condition, and bpm.
    % average neuron across trials.
    counter = 0;
    for succ=succ_vec
        for p=periodic_cond_vec
            for int=intervals_vec
                counter = counter + 1;
                SDFS.conds_succ(counter,1)=succ;
                SDFS.conds_periodic(counter,1)=p;
                SDFS.conds_interval(counter,1)=int;
            end
        end
    end

    SDFS.sdf=cell(counter,1);
    SDFS.time=cell(counter,1);
    SDFS.time_unit=cell(counter,1);
    SDFS.cell_id=cell(counter,1);
    SDFS.session_id_neurons=cell(counter,1);
end

for s = sess_latest:numel(file_name_sessions)
    fprintf('%s\n',file_name_sessions(s).name)
    
    load(fullfile(file_name_sessions(s).folder,file_name_sessions(s).name),'data_struct')
    cell_ids = data_struct.nonempty_signals_index;
    counter = 0;
    for succ=succ_vec
        for p=periodic_cond_vec
            for int=intervals_vec
                counter = counter + 1;
                for c=1:numel(cell_ids)
                    SDFS_temp = [];
                    SDFS_temp_time = [];
                    SDFS_temp_time_unit = [];
                    SDFS_temp_trial_count = [];
                    stims_temp = cell(0);
                    taps_temp = cell(0);
                    stims_before_tap = [];
                    firstnan = inf;
                    lastnan = -inf;
                    cell_trial_counter = 0;
                    for tr=1:numel(data_struct.SDF_RAW)
                        cell_found = find(data_struct.SDF_RAW{tr}.cell_ids==cell_ids(c),1);
                        if ~isempty(cell_found) && ...
                                data_struct.correct(tr)==succ && ...
                                data_struct.periodic_index(tr)==p && ...
                                data_struct.interval(tr)==int
                            
%                             % This condition never seems to be satisfied which is a good thing.
%                             if data_struct.SDF_RAW{tr}.time(1)>data_struct.stims{tr}(1)
%                                 fprintf('Cabrón!')
%                                 fprintf('%6.3f\n',data_struct.SDF_RAW{tr}.time(1)-data_struct.stims{tr}(1))
%                                 fprintf('%6.0f,',s,tr,cell_ids(c),succ,periodic,bpm)
%                                 fprintf('\n')
%                             end
                            
%                             % crop between chosen stims, relative to first
%                             % stim or first tap-stim.
%                             switch stim_init_tap_init
%                                 case 1
%                                     if diff(data_struct.stims{tr}(1:2))>1
%                                         center = data_struct.stims{tr}(2);
%                                     else
%                                         center = data_struct.stims{tr}(1);
%                                     end
%                                 case 2
%                                     center = data_struct.center(tr);
%                             end
%                             shift_time_units = round((data_struct.center(tr) - center)/periods_vec(bpm));
%                             ind1=find(data_struct.stims{tr}<=center,pre_tap_stims+1,'last' );
%                             ind2=find(data_struct.stims{tr}> center,post_tap_stims ,'first');
%                             stims = [data_struct.stims{tr}(ind1);data_struct.stims{tr}(ind2)];
%                             if diff(stims(1:2))>1;stims=stims(2:end);end
%                             if diff(stims(end-1:end))>1;stims=stims(1:end-1);end
%                             
%                             % first crop
%                             index = logical(...
%                                 (data_struct.SDF_RAW{tr}.time<(stims(end)+post_stim_time)).*...
%                                 (data_struct.SDF_RAW{tr}.time>(stims(1)-pre_stim_time)));
%                             time_cropped = data_struct.SDF_RAW{tr}.time(index);
%                             time_unit_cropped = data_struct.SDF_RAW{tr}.time_units(index) + shift_time_units;
%                             sdf_cropped = data_struct.SDF_RAW{tr}.sdf(index,cell_found);
%                             
%                             % second crop
%                             [~,ind_zero] = min(abs(time_cropped-center));
%                             index = int16((sdftime_zero_ind-ind_zero+1):(sdftime_zero_ind-ind_zero+numel(sdf_cropped)))';
%                             SDFS_temp_temp = nan(size(sdftime));
%                             SDFS_temp_temp(index) = sdf_cropped;
%                             sdf_time_unit = nan(size(sdftime));
%                             sdf_time_unit(index) = time_unit_cropped;
%                             sdf_time = nan(size(sdftime));
%                             sdf_time(index) = (time_cropped-center);%.*data_struct.interval(tr);
%                             
%                             %plot(time_cropped,sdf_cropped)
%                             %hold on
%                             %plot(data_struct.stims{tr},data_struct.stims{tr}*0,'^r','linewidth',2)
%                             %plot(data_struct.center(tr),10,'^b','linewidth',2)
%                             %hold off
%                             %pause
                            
                            cell_trial_counter = cell_trial_counter + 1;
                            stims_before_tap(cell_trial_counter,1) = data_struct.stims_to_tap(tr);
                            stims_temp{cell_trial_counter} = data_struct.stims{tr}-data_struct.center(tr);
                            taps_temp{cell_trial_counter} = data_struct.tap{tr}-data_struct.center(tr);

                            % horzcat raw sdfs
                            SDFS_temp_temp = data_struct.SDF_RAW{tr}.sdf(:,cell_found);
                            SDFS_temp = nansum([SDFS_temp,SDFS_temp_temp],2);
                            firstnan = min([firstnan,find(~isnan(SDFS_temp_temp),1,'first')]);
                            lastnan = max([lastnan,find(~isnan(SDFS_temp_temp),1,'last')]);
                            SDFS_temp_trial_count = nansum([SDFS_temp_trial_count,~isnan(SDFS_temp_temp)],2);
                            sdf_time = data_struct.SDF_RAW{tr}.time;
                            if any(diff([SDFS_temp_time,sdf_time],1,2));keyboard;end
                            SDFS_temp_time = sdf_time;
                            sdf_time_unit = data_struct.SDF_RAW{tr}.time_units;
                            SDFS_temp_time_unit = horzcat(SDFS_temp_time_unit,sdf_time_unit);
                        end
                    end
                    %SDFS.cell_id{succ+1,periodic+1,bpm} = horzcat(SDFS.cell_id{succ+1,periodic+1,bpm},cell_ids(c)+s*1e4);
                    %SDFS.sdf{succ+1,periodic+1,bpm} = horzcat(SDFS.sdf{succ+1,periodic+1,bpm},nanmean(SDFS_temp,2));
                    %SDFS.time{succ+1,periodic+1,bpm} = horzcat(SDFS.time{succ+1,periodic+1,bpm},nansum(SDFS_temp_time,2)./sum(~isnan(SDFS_temp_time),2));
                    %SDFS.time_unit{succ+1,periodic+1,bpm} = horzcat(SDFS.time_unit{succ+1,periodic+1,bpm},nansum(SDFS_temp_time_unit,2)./sum(~isnan(SDFS_temp_time_unit),2));
                    
                    if ~isempty(SDFS_temp)
                        if firstnan==Inf;firstnan=1;end
                        if lastnan==-Inf;lastnan=size(SDFS_temp,1);end
                        try SDFS_temp(1:(firstnan-1)) = nan;SDFS_temp((lastnan+1):end) = nan;catch;keyboard;end
                        
                        SDFS.cell_id{counter} = horzcat(SDFS.cell_id{counter},cell_ids(c)+s*1e4);
                        SDFS.session_id_neurons{counter} = horzcat(SDFS.session_id_neurons{counter},s);
                        SDFS.sdf{counter} = horzcat(SDFS.sdf{counter},SDFS_temp./SDFS_temp_trial_count);
                        SDFS.time{counter} = SDFS_temp_time;
                        SDFS.time_unit{counter} = SDFS_temp_time_unit;
                        
                        SDFS.stims{counter,1} = stims_temp;
                        SDFS.taps{counter,1} = taps_temp;
                        SDFS.stims_to_tap{counter,1} = stims_before_tap;
                    end
                end
            end
        end
    end
    sess_latest = s;
    save('SDFS_aggregate_stage','SDFS','sess_latest','-v7.3')
end

return