function [best_model_ind] = ts_continuous_fit_of_sdf_laplace_pdfs(xx,tt,plotting,varargin)
%%Fit a function with n Laplace distributions centered around the stims.
%sdf_fit_n_laplace,sdf_fit_n_gauss,sdf_fit_n_triang
if numel(varargin)>0
    mus = varargin{1};
else
    mus = (round(min(tt)):round(max(tt)))';
end

%mus = mus - min(tt);
%tt = tt - min(tt);

if max(mus)==0;xx=xx(tt<=0);tt=tt(tt<=0);end
tt1 = unique(tt);

if plotting == 1
    plot(tt,xx,'.','LineWidth',2)
    set(gca,'color','k')
    hold on
    plot(mus,0,'vm','linewidth',2)
    hold off
end

% fo = fitoptions('Method','LinearLeastSquares',...
%     'Lower',[mus'-1,-1,-1,0,-100],...
%     'Upper',[mus'+1, 1, 1,50,100]);

% Are these the right boundaries?
fol = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',[mus'-.25,-.5,-.5,10,-10,-3],...
    'Upper',[mus'+.25, .5, .5,50,50,5],...
    'StartPoint',[mus', .1, -.01,10,25,0]);

fog = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',[mus'-.25,-.5,-.5,10,-10,-3],...
    'Upper',[mus'+.25, .5, .5,50,50,5],...
    'StartPoint',[mus', .1, -.01,10,25,0]);

fot = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',[mus'-.25,-1.5,-1.5,0,-100,-2],...
    'Upper',[mus'+.25, 1.5, .5,100,100,15],...
    'StartPoint',[mus', 1, -1.,50,-2,10]);

switch numel(mus)
    case 1
        ftl = fittype('fun_additive_laplace1(a,b,c,d,e,f,x)','options',fol);
        ftg = fittype('fun_additive_gauss1(a,b,c,d,e,f,x)','options',fog);
        ftt = fittype('fun_additive_triang1(a,b,c,d,e,f,x)','options',fot);
    case 2
        ftl = fittype('fun_additive_laplace2(a,b,c,d,e,f,g,x)','options',fol);
        ftg = fittype('fun_additive_gauss2(a,b,c,d,e,f,g,x)','options',fog);
        ftt = fittype('fun_additive_triang2(a,b,c,d,e,f,g,x)','options',fot);
    case 3
        ftl = fittype('fun_additive_laplace3(a,b,c,d,e,f,g,h,x)','options',fol);
        ftg = fittype('fun_additive_gauss3(a,b,c,d,e,f,g,h,x)','options',fog);
        ftt = fittype('fun_additive_triang3(a,b,c,d,e,f,g,h,x)','options',fot);
    case 4
        ftl = fittype('fun_additive_laplace4(a,b,c,d,e,f,g,h,k,x)','options',fol);
        ftg = fittype('fun_additive_gauss4(a,b,c,d,e,f,g,h,k,x)','options',fog);
        ftt = fittype('fun_additive_triang4(a,b,c,d,e,f,g,h,k,x)','options',fot);
    case 5
        ftl = fittype('fun_additive_laplace5(a,b,c,d,e,f,g,h,k,l,x)','options',fol);
        ftg = fittype('fun_additive_gauss5(a,b,c,d,e,f,g,h,k,l,x)','options',fog);
        ftt = fittype('fun_additive_triang5(a,b,c,d,e,f,g,h,k,l,x)','options',fot);
end
try [curvel,gofl] = fit(tt,xx,ftl);catch;best_model_ind=0;return;end
try [curveg,gofg] = fit(tt,xx,ftg);catch;best_model_ind=0;return;end
try [curvet,goft] = fit(tt,xx,ftt);catch;best_model_ind=0;return;end

sdf_fit_n_laplace = eval_and_deltas(curvel,gofl,mus);
sdf_fit_n_gauss = eval_and_deltas(curveg,gofg,mus);
sdf_fit_n_triang = eval_and_deltas(curvet,goft,mus);

sdf_fit_n_laplace.n_laplace_reg_main_params_labels = {'\Delta-Ave','\Delta-SD','b0','b1','a0','c0','c1','R^2'};
sdf_fit_n_laplace.n_laplace_reg_params_labels = {'\mu_i','b0','b1','a0','c0','c1'};
sdf_fit_n_laplace.n_laplace_reg_params_ci = {'\mu_i','b0','b1','a0','c0','c1'};

disp([gofl.rsquare gofg.rsquare goft.rsquare])
disp([gofl.adjrsquare gofg.adjrsquare goft.adjrsquare])
[~,best_model_ind] = max([gofl.adjrsquare gofg.adjrsquare goft.adjrsquare]);

yyl=fun_additive_laplace(sdf_fit_n_laplace.n_laplace_reg_params(1:end-5),...
    sdf_fit_n_laplace.n_laplace_reg_params(end-4:end-3),...
    sdf_fit_n_laplace.n_laplace_reg_params(end-2:end),tt1);

yyg=fun_additive_gauss(sdf_fit_n_gauss.n_laplace_reg_params(1:end-5),...
    sdf_fit_n_gauss.n_laplace_reg_params(end-4:end-3),...
    sdf_fit_n_gauss.n_laplace_reg_params(end-2:end),tt1);

yyt=fun_additive_triang(sdf_fit_n_triang.n_laplace_reg_params(1:end-5),...
    sdf_fit_n_triang.n_laplace_reg_params(end-4:end-3),...
    sdf_fit_n_triang.n_laplace_reg_params(end-2:end),tt1);

if plotting == 1
    %plot(tt,xx,'.','LineWidth',2)
    %set(gca,'color','k')
    %hold on
    %plot(mus,0,'vm','linewidth',2)
    hold on
    plot(tt1,yyl,'-y','linewidth',3)
    plot(tt1,yyg,'-g','linewidth',3)
    plot(tt1,yyt,'-c','linewidth',3)
    plot(sdf_fit_n_laplace.n_laplace_reg_params(1:end-5),0,'^r','linewidth',2)
    hold off
    %pause
end
end

% Weird, but seems to work like this.
% for k=1:numel(mus)
%     eval(['mu' num2str(k,'%1.0f') ' = mus(' num2str(k,'%1.0f') ');'])
% end

function sdf_fit_n_laplace = eval_and_deltas(curve,gof,mus)

cofs=coeffvalues(curve);
cofci=confint(curve);

muc = cofs(1:numel(mus));
b = cofs(end-4:end-3);
a = cofs(end-2);
c = cofs(end-1:end);

[~,ind]=min(abs(mus-muc(muc>0)),[],1);
deltas = muc(muc>0)-mus(ind)';

sdf_fit_n_laplace.n_laplace_reg_main_params = [mean(deltas) std(deltas) b a c gof.rsquare];
sdf_fit_n_laplace.n_laplace_reg_params = [muc b a c];
sdf_fit_n_laplace.n_laplace_reg_params_ci = cofci;

end