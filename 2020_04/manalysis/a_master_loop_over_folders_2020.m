%% Prep paths
clear
[~,pc]=system('uname -a');
if ~isempty(regexp(pc,'pop-os','once'))
    %data_folder = '~/Desktop/inb_data/';
    addpath('~/logos/inb/2021_05/manalysis')
end
if ~isempty(regexp(pc,'ideapad','once'))
    data_folder = '~/Desktop/inb_data/';
    addpath('~/Desktop/inb/2021_05/manalysis/')
end
if ~isempty(regexp(pc,'PCWIN64','once'))
    data_folder = 'C:\Users\Dobri\Desktop\inb_data';
    addpath('C:\Users\Dobri\Desktop\inb\2021_05\manalysis')
end


%%
m = 1;
f=dir(fullfile(data_folder,['M' num2str(m,'%1.f') '*']));
base_folder = fullfile(f.folder,f.name);
clear f;clear data_folder;clear pc
cd(base_folder)


%% Prep folders for storing raw pics.
if 0 && any(m==[1 2])
    % These mat files are sessions w/ sorted spike times and behav events.
    file_name_sessions = dir('Behav*.mat');
    file_name_sessions = file_name_sessions(~[file_name_sessions.isdir]);
    
    for n=1:numel(file_name_sessions)
        fpath_dump_data = [fullfile(base_folder,file_name_sessions(n).name(1:end-4)),'/'];
        system(['mkdir ' fpath_dump_data]);
        system(['mkdir ' fullfile(fpath_dump_data,'pics')]);
        system(['mv ' file_name_sessions(n).name ' ' [fpath_dump_data,'/']]);
    end
end


%% Load, arrange in data_struct, pre-process, trial-based.
if 1
    
    file_name_sessions = dir('raw/Behav*');
    
    % Load, arrange in data_struct, pre-process, trial-based.
    w = dir(fullfile(base_folder,'selected_sessions.csv'));
    if ~isempty(w)
        which_session = dlmread(fullfile(w.folder,w.name),',',0,0);
        which_session = find(which_session>0);
    else
        which_session = (1:numel(file_name_sessions))';
    end
    clear w

    num_cells = 0;
    for n=(which_session(1:end)')

        % Get the data first.
        % data_file = fullfile(file_name_sessions(n).folder,file_name_sessions(n).name,[file_name_sessions(n).name,'.mat']);
        data_file = fullfile(file_name_sessions(n).folder,file_name_sessions(n).name);
        data_struct = load_2020_format_data_feb(data_file);
        num_cells = num_cells + size(data_struct.Neurons,2);
        
        % Raw SDFs, single neuron/trial
        data_struct = sdf_raw_over_trials(data_struct,[-7 7],0);
        
        % PCs per trial
        plotting_flag = 0;save_plots_flag = 0;
        data_struct.PC = get_pcs_looping_over_trials_from_raw_sts(data_struct,fullfile(base_folder,'pics'),plotting_flag,save_plots_flag);
        
        plotting_flag = 0;save_plots_flag = 0;
        if plotting_flag == 1
            pcs_plot_aves_per_period_etc(data_struct,save_plots_flag,fullfile(base_folder,'summary_pics'))
        end
        
        % Clean some of the raw time series data to save space.
        if 0
            data_struct.Neurons = data_struct.Neurons(:,data_struct.nonempty_signals_index);
            data_struct.fr_ave = data_struct.fr_ave(data_struct.nonempty_signals_index);
            data_struct.fr_sd = data_struct.fr_sd(data_struct.nonempty_signals_index);
            for tr=1:size(data_struct.SDF_RAW,1)
                new_ind = logical(data_struct.SDF_RAW{tr}.cell_ids*0);
                for ind = 1:numel(data_struct.SDF_RAW{tr}.cell_ids)
                    new_ind(ind) = any(data_struct.SDF_RAW{tr}.cell_ids(ind)==data_struct.nonempty_signals_index);
                end
                data_struct.num_cells(tr,1) = sum(new_ind);
                data_struct.SDF_RAW{tr}.sdf = data_struct.SDF_RAW{tr}.sdf(:,new_ind);
                data_struct.SDF_RAW{tr}.cell_ids = data_struct.SDF_RAW{tr}.cell_ids(:,new_ind);
                data_struct.SDF_RAW{tr}.sps_ave = data_struct.SDF_RAW{tr}.sps_ave(:,new_ind);
                data_struct.SDF_RAW{tr}.sps_sd = data_struct.SDF_RAW{tr}.sps_sd(:,new_ind);
            end
        end

        % Save mid structure. By now data_struct could weigh 1GB+!
        save_mid_proc_data = 1;
        if save_mid_proc_data == 1
            % save(fullfile(fpath_dump_data,'mid_processing.mat'),'data_struct','-v7.3')
            save(fullfile(base_folder,'mid_processing',['mid_processing_' file_name_sessions(n).name '.mat']),'data_struct','-v7.3')
        end
    end
end


%% Measures, trial-based.
if 0
    % Get a list of all files and folders in this folder.
    folder_names_sessions = dir('Behav*');
    folder_names_sessions = folder_names_sessions([folder_names_sessions.isdir]);

    w=dir(fullfile(base_folder,'selected_sessions.csv'));
    if ~isempty(w)
        which_session = dlmread(fullfile(w.folder,w.name),',',0,0);
        which_session = find(which_session>=1);
    else
        which_session = (1:numel(folder_names_sessions))';
    end
    clear w

    out_SDF_ind_cell_ind_trial_osc_params{1} = [];
    for n = 1:numel(which_session)
        if 1;try load('sdf_and_pc_params_temp.mat');catch me;end;end
        
        fpath_dump_data = fullfile(base_folder,folder_names_sessions(which_session(n)).name);
        fpath_dump_summary_pics = fullfile(base_folder,'summary_pics');

        if 1
            clear data_struct -global % Doesn't help. Matlab doesn't manage memory properly on linux.
            load(fullfile(fpath_dump_data,'mid_processing.mat'),'data_struct')
            %keyboard
            %keyboard
        end
        
        % Ind neurons, Ind trials. T.s. and cycle-by-cycle peak analysis for oscillation parameters
        if 1
            plotting_flag = 0;save_plots_flag = 0;
            out_SDF_ind_cell_ind_trial_osc_params{m}{which_session(n)} = ...
                ind_neurons_ind_trials_osc_params_nonwarped(data_struct,data_struct.session_id,...
                plotting_flag,fpath_dump_data,save_plots_flag);
        end        
        
        % PCs. Oscillation analysis by peak-picking (cycle-by-cycle analysis).
        % Also fit a custom (cycloid-like) continuous function to PCed trial sdfs.
        % And even compare between dynamic shapes (exponential, Gaussian, sawtooth (linear ramping).
        if 1
            plotting_flag = 0;save_plots_flag = 0;
            [out_SDF_PC_per_trial_cyclofit{m}{which_session(n)},...
                out_SDF_PC_per_trial_on_peaks{m}{which_session(n)},...
                out_SDF_PC_per_trial_which_ramping{m}{which_session(n)}] = ...
                get_pc_cycl_fit_params(data_struct.PC,data_struct,...
                fpath_dump_data,plotting_flag,save_plots_flag,fpath_dump_summary_pics);
        end
        
        
        % Scaling of eigenspace. Log-log fits of eigenloadings.
        % Outlier detection using clustering.
        if 1
            plotting_flag = 0;save_plots_flag = 0;
            [~,out_PC_beta_exponents{m}{which_session(n)}] = ...
                pcs_plot_power_law_eigenvalues(data_struct,...
                plotting_flag,save_plots_flag,fpath_dump_summary_pics); % PC{m}{which_session(n)}
        end
        
        if 1
            n_completed = n;
            save('sdf_and_pc_params_temp.mat',...
                'out_SDF_ind_cell_ind_trial_osc_params',...
                'out_SDF_PC_per_trial_cyclofit',...
                'out_SDF_PC_per_trial_on_peaks',...
                'out_SDF_PC_per_trial_which_ramping',...
                'out_PC_beta_exponents',...
                'n_completed')
        end
    end
    
    %% Count the retained cells with a signal.
    % num_cells = 6548;
    % num_selected_cells = 2228; 3032
    % up to which_session 22 num_selected_cells(m) = 0;
    num_selected_cells(m) = 0;
    for n = 1:numel(which_session)
        num_selected_cells(m) = num_selected_cells(m) + ...
            numel(unique(out_SDF_ind_cell_ind_trial_osc_params{m}{which_session(n)}.params(...
            out_SDF_ind_cell_ind_trial_osc_params{m}{which_session(n)}.params(:,8)==1,2)));
    end
    
    %% Concat params in tables to do global stats in R.
    % PC1 raw cycle peak params and trial-averaged.
    all_pc1_peak_raw_params.table=[];
    all_pc1_peak_ave_params.table=[];
    all_pc1_peak_raw_params.labels = [];
    all_pc1_peak_ave_params.labels = [];
    for n=1:size(out_SDF_PC_per_trial_on_peaks{m},2) %which_session
        if ~isempty(out_SDF_PC_per_trial_on_peaks{m}{n})
            all_pc1_peak_raw_params.table = vertcat(all_pc1_peak_raw_params.table,...
                out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks);
            all_pc1_peak_ave_params.table = vertcat(all_pc1_peak_ave_params.table,...
                out_SDF_PC_per_trial_on_peaks{m}{n}.params);
            % out_SDF_PC_per_trial_cyclofit{m}{n}.params ?
            % Get the column labels only once.
            if isempty(all_pc1_peak_ave_params.labels)
                if ~isempty(out_SDF_PC_per_trial_on_peaks{m}{n})
                    all_pc1_peak_raw_params.labels = out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks_labels;
                    all_pc1_peak_ave_params.labels = out_SDF_PC_per_trial_on_peaks{m}{n}.param_labels;
                end
            end
        end
    end
    
    
    %% Now save to a csv.
    fid = fopen(['pc1_cpc_raw_params_31sessions_m' num2str(m,'%1.0f') '_' datestr(now,'yyyymmdd-HHMMSS') '.csv'],'w');
    for l=1:numel(all_pc1_peak_raw_params.labels);fprintf(fid,'%s,',all_pc1_peak_raw_params.labels{l});end;fprintf(fid,'\n');
    for r=1:size(all_pc1_peak_raw_params.table,1);fprintf(fid,'%.4f,',all_pc1_peak_raw_params.table(r,:));fprintf(fid,'\n');end
    fclose(fid);
    
    fid = fopen(['pc1_cpc_ave_params_31sessions_m' num2str(m,'%1.0f') '_' datestr(now,'yyyymmdd-HHMMSS') '.csv'],'w');
    for l=1:numel(all_pc1_peak_ave_params.labels);fprintf(fid,'%s,',all_pc1_peak_ave_params.labels{l});end;fprintf(fid,'\n');
    for r=1:size(all_pc1_peak_ave_params.table,1);fprintf(fid,'%.4f,',all_pc1_peak_ave_params.table(r,:));fprintf(fid,'\n');end
    fclose(fid);
end


return


% PC1 fits. Not really to be used, just checking.
all_pc1_peak_fits.table=cell(1,numel(out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks_fits.table));
for m=1:numel(out_SDF_PC_per_trial_on_peaks)
    for n=1:numel(out_SDF_PC_per_trial_on_peaks{m})
        for d=1:numel(out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks_fits.table)
            all_pc1_peak_fits.table{d}=vertcat(all_pc1_peak_fits.table{d},...
                out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks_fits.table{d});
        end
    end
end
all_pc1_peak_fits.labels = out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks_fits.labels;


return


% Ind neuron fits. To be used for summary.
all_ind_neuron_peak_fits.table=cell(1,numel(out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks_fits.table));
for n=which_session
    for d=1:numel(out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks_fits.table)
        all_ind_neuron_peak_fits.table{d}=vertcat(all_ind_neuron_peak_fits.table{d},...
            out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks_fits.table{d});
    end
end
all_ind_neuron_peak_fits.labels = out_SDF_PC_per_trial_on_peaks{m}{n}.raw_peaks_fits.labels;


%{
table_pc1_peak_fits.table{4}(table_pc1_peak_fits.table{3}(:,3)==1,4:end)

ttest(table_pc1_peak_fits.table{3}(table_pc1_peak_fits.table{3}(:,3)==1,6),0,.05,'right')

index = logical((table_pc1_peak_fits.table{3}(:,3)==1).*...
    (table_pc1_peak_fits.table{3}(:,4)>20));
table_pc1_peak_fits.table{3}(index,4:end)
scatter(table_pc1_peak_fits.table{3}(index,4),table_pc1_peak_fits.table{3}(index,6))
%}
