function peaks_stats = ts_of_sdf_peak_params(xx,tt,plotting,varargin) % ,tt_is_abs_time

if numel(varargin)>0
    sr = varargin{1};
else
    sr=1/mean(diff(tt));
end
if numel(varargin)>1
    stims = varargin{2};
else
    stims = [];
end
if numel(varargin)>2
    dfactor = varargin{3};
else
    dfactor = [];
end
if numel(varargin)>3
    rise_threshold = varargin{4};
else
    rise_threshold = [];
end


% Why am I using spline fit and not another conv w/ longer Gaussian kernel?
% Because of easier interpretation when the source data is full.
% fs=fit(tt,xx,'smoothingspline','SmoothingParam',1-1e-2);
% xxf=feval(fs,tt);
conv_win_len_ms = 2e2; % msec
gaussFilter = gausswin(round(conv_win_len_ms/1e3*sr*1));
gaussFilter = gaussFilter./sum(gaussFilter);
xxf = conv(xx,gaussFilter,'same'); % Check the sps from conv formula.

[peak_amps,peak_event_locs] = findpeaks(xxf./max(xxf),'MinPeakDistance',.2,'MinPeakProminence',.1);
peak_event_times = tt(peak_event_locs);
peak_amps(peak_event_times<stims(1))=[];
peak_event_locs(peak_event_times<stims(1))=[];
peak_event_times(peak_event_times<stims(1))=[];
peak_amps = peak_amps*max(xxf);

[valley_amps,valley_event_locs] = findpeaks(-(xxf./max(xxf)),'MinPeakDistance',.2,'MinPeakProminence',.1);
valley_event_times = tt(valley_event_locs);
valley_amps(valley_event_times<stims(1))=[];
valley_event_locs(valley_event_times<stims(1))=[];
valley_event_times(valley_event_times<stims(1))=[];
valley_amps = (-valley_amps)*max(xxf);

% Define the start of neural activity as the threshold crossing or halfpoint b/w peak and valley.
rise_event_times = nan(size(peak_event_times));
rise_event_amps = nan(size(peak_event_times));
for e = 1:numel(peak_event_times)
    ind = find(valley_event_times < peak_event_times(e),1,'last');
    if ~isempty(ind)
        if ~isempty(rise_threshold) && (valley_amps(ind)<rise_threshold) && (peak_amps(e)>rise_threshold)
            temp = xxf(valley_event_locs(ind):peak_event_locs(e));
            first_zero_crossing_index = find(diff((temp-rise_threshold)>0)==1,1,'first');
            rise_event_times(e) = tt(valley_event_locs(ind)+first_zero_crossing_index);
            rise_event_amps(e) = xxf(valley_event_locs(ind)+first_zero_crossing_index);
        else
            rise_event_times(e) = tt(round((valley_event_locs(ind) + peak_event_locs(e))/2));
            rise_event_amps(e) = xxf(round((valley_event_locs(ind) + peak_event_locs(e))/2));
        end
    end
end

if isempty(peak_event_times)
    peak_event_deltas = [];
    peak_event_phases = [];
    rise_event_deltas = [];
    rise_event_phases = [];
    valley_amps=[];
    valley_event_times=[];
    valley_event_deltas = [];
    valley_event_phases = [];
    b = [1 1]*nan;
else
    [peak_event_deltas,peak_event_phases] = reference_deltas(peak_event_times,peak_event_times,stims,dfactor); % tt_is_abs_time,
    [rise_event_deltas,rise_event_phases] = reference_deltas(rise_event_times,peak_event_times,stims,dfactor); % tt_is_abs_time,
    [valley_event_deltas,valley_event_phases] = reference_deltas(valley_event_times,peak_event_times,stims,dfactor); % tt_is_abs_time,
    try b = [peak_event_deltas.^0 (1:numel(peak_event_deltas))']\peak_event_deltas;catch;b = [1 1]*nan;end
end


peaks_stats.labels = {'\Delta','\theta',...
    'peakA','amp','b_{\Delta}','\Delta_{rise}','\theta_{rise}',...
    '\Delta_{bott}','amp_{bott}','\SD_{delta}'};
p1=mean(peak_event_deltas);if isempty(p1);p1=nan;end
p2=circ_mean(peak_event_phases*2*pi);if isempty(p2);p2=nan;end
p3=mean(peak_amps);if isempty(p3);p3=nan;end
p4=range(xxf)/2;if isempty(p4);p4=nan;end
try p5=b(2);if isempty(p5);p5=nan;end;catch me;keyboard;end
p6=nanmean(rise_event_deltas);if isempty(p6);p6=nan;end
p7=circ_mean(rise_event_phases(~isnan(rise_event_phases))*2*pi);if isempty(p7);p7=nan;end
p8=mean(valley_event_deltas);if isempty(p8);p8=nan;end
p9=mean(valley_amps);if isempty(p9);p9=nan;end
p10=std(peak_event_deltas);if isempty(p10);p10=nan;end
peaks_stats.params = [p1 p2 p3 p4 p5 p6 p7 p8 p9 p10];

% Crop/nan-pad valley events that might be different numbers than peaks.
if isempty(valley_event_deltas) && ~isempty(peak_event_deltas)
    valley_event_deltas=nan(size(peak_event_deltas));
    valley_amps=nan(size(peak_event_deltas));
    valley_event_times=nan(size(peak_event_deltas));
end

if ~isempty(peak_event_deltas)
    if numel(valley_event_deltas)>numel(peak_event_deltas)
        valley_event_deltas(1:(numel(valley_event_deltas)-numel(peak_event_deltas)))=[];
        valley_amps(1:(numel(valley_amps)-numel(peak_event_deltas)))=[];
        valley_event_times(1:(numel(valley_event_times)-numel(peak_event_deltas)))=[];
    end
    if numel(valley_event_deltas)<numel(peak_event_deltas)
        valley_event_times(numel(peak_event_deltas),1)=nan;
        valley_amps(numel(peak_event_deltas),1)=nan;
        valley_event_deltas(numel(peak_event_deltas),1)=nan;
        
        valley_event_times(valley_event_deltas==0,1)=nan;
        valley_amps(valley_event_deltas==0,1)=nan;
        valley_event_deltas(valley_event_deltas==0,1)=nan;
    end
end

peaks_stats.raw_data_labels = {'deltas','thetas','amps',...
    'delta_rise','theta_rise',...
    'bottom_deltas','bottom_amps'};
peaks_stats.raw_data = [peak_event_deltas peak_event_phases peak_amps ...
    rise_event_deltas rise_event_phases ...
    valley_event_deltas valley_amps];

if plotting==1
    try bp = [peak_event_times.^0 (1:numel(peak_event_times))']\peak_amps;catch;bp = [1 1]*0;end
    %disp([valley_event_times valley_event_deltas]')
    %disp([rise_event_times rise_event_deltas]')
    %disp([peak_event_times peak_event_deltas]')
    plot(tt,xx,tt,xxf,'linewidth',2)
    hold on
    plot(peak_event_times,peak_amps,'dr','linewidth',2,'MarkerSize',10)
    plot(valley_event_times,valley_amps,'om','linewidth',2,'MarkerSize',10)
    plot(rise_event_times,rise_event_amps,'vb','linewidth',2,'MarkerSize',10)
    plot(stims,stims*0,'+','linewidth',3,'MarkerSize',10)
    if ~isempty(rise_threshold)
        plot(rise_event_times,rise_event_times*0+rise_threshold,'--k','linewidth',2,'MarkerSize',10)
    end
    plot(rise_event_times,rise_event_amps,'vb','linewidth',2,'MarkerSize',10)
    plot(peak_event_times,peak_event_times.^0*bp(1)+(1:numel(peak_event_times))'*bp(2),'--r','linewidth',2,'MarkerSize',10)
    hold off
    pause
    %keyboard
end

end

function [event_deltas,event_phases] = reference_deltas(event_times,lead_times,stims,dfactor)
if isempty(stims);stims=-100:100;end
if isempty(dfactor);dfactor=stims.^0;end
event_deltas = nan(size(event_times));
event_phases = nan(size(event_times));
if isempty(stims);keyboard;end
% There shouldn't be any events pre the first stimulus at this point.
%event_deltas(event_times<stims(1)) = event_times(event_times<stims(1))-stims(1)+diff(stims(1:2));
%event_phases(event_times<stims(1)) = event_deltas(event_times<stims(1))./diff(stims(1:2));
% As delta, s
for s=1:numel(event_times)
    if ~isnan(event_times(s))
        % Find the lead event closest to the event. That will be the probe.
        ind = find(event_times(s)<=lead_times,1,'first');
        if isempty(ind)
            try probe = lead_times(end);catch me;keyboard;end
        else
            probe = lead_times(ind);
        end
        % Then find the reference closest to the probe.
        [~,ind] = min(abs(probe-stims));
        event_deltas(s) = event_times(s)-stims(ind);
        if event_deltas(s)<0
            event_deltas(s) = event_deltas(s)*dfactor(ind-1);
        else
            try event_deltas(s) = event_deltas(s)*dfactor(ind);catch;keyboard;end
        end
    else
        event_deltas(s) = nan;
    end
end
% As phase, proportion
for s=1:numel(stims)-1
    index = logical((event_times>stims(s)).*(event_times<=stims(s+1)));
    event_phases(index) = (event_times(index)-stims(s))./diff(stims(s:s+1));
end
index = event_times>stims(end);
event_phases(index) = (event_times(index)-stims(end))./diff(stims(end-1:end));

%event_deltas(event_times>stims(end)) = event_times(event_times>stims(end))-stims(end);
% if tt_is_abs_time == 1
% else
%     if ~isempty(stims) && ~isempty(dfactor)
%         event_deltas = nan(size(event_times));
%         event_phases = nan(size(event_times));
%         for s=1:numel(event_deltas)
%             [~,ind] = min(abs(event_times(s)-stims));
%             event_deltas(s) = (event_times(s)-stims(ind))*dfactor(s+1);
%             %event_phases(s) = ?
%         end
%     else
%         event_phases = mod(event_times,1);
%         event_deltas = mod(event_times,1);
%     end
% end
end