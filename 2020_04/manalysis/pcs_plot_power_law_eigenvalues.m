function [PC,PC_beta_exponents] = pcs_plot_power_law_eigenvalues(data_struct,plotting_flag,save_plots_flag,fpath1)

PC = data_struct.PC;

%% Find outlier clusters in the histogram of the residuals.
tasks = unique(data_struct.task);
r2_outliers = data_struct.task*0;
for t = 1:numel(tasks)
    for correct = 0:1
        index = logical((data_struct.task==tasks(t)).*(data_struct.correct==correct));
        if sum(index)>0
            if numel(data_struct.PC{find(index,1)}.r2)>40
                r2_outliers(index) = power_law_outliers(data_struct.PC(index),0);
            else
                r2_outliers(index) = 0;
            end
        end
    end
end

for tr = 1:numel(PC)
    PC{tr}.r2_outliers = r2_outliers(tr);
end

if plotting_flag == 1
    close all
    figure
end


PC_beta_exponents = [];
trvec = cell(18,1);
bvec = cell(18,1);
linen = num2cell(zeros(18,1));
clear lo

R2flat = [];
for tr=1:numel(PC)
    if data_struct.task(tr)==0
        p = 8;
    end
    if data_struct.task(tr)==1
        p = 9;
    end
    if data_struct.task(tr)==2
        p = find(PC{tr}.period == data_struct.period_vec);
    end
    if data_struct.task(tr)==3
        p = 7;
    end
    correct = data_struct.correct(tr);
    c = p + correct*9;
    
    if ~isempty(PC{tr}.r2)
        if numel(PC{tr}.r2)<10;continue;end
        
        PC{tr}.rank = (1:numel(PC{tr}.r2))';
        
        y_excl = PC{tr}.r2(round(.5*numel(PC{tr}.r2))+1:end);
        x_excl = PC{tr}.rank(round(.5*numel(PC{tr}.rank))+1:end);

        y_outl = PC{tr}.r2(1:PC{tr}.r2_outliers);
        x_outl = PC{tr}.rank(1:PC{tr}.r2_outliers);

        y = PC{tr}.r2((1+PC{tr}.r2_outliers):round(.5*numel(PC{tr}.r2)));
        x = PC{tr}.rank((1+PC{tr}.r2_outliers):round(.5*numel(PC{tr}.rank)));
        
        b = [x.^0 log(x)]\log(y);
        y_flat = log10(y) - log10(exp(b(1)).*x.^b(2));
        R2flat = vertcat(R2flat,[x y_flat]);
        
        PC{tr}.b_exponent = b(2);
        PC_beta_exponents = vertcat(PC_beta_exponents,[PC{tr}.correct,c,tr,b(2) PC{tr}.r2_outliers]);
        
        bvec{c}=[bvec{c} b(2)];
        
        if plotting_flag == 1
            linen{c}=linen{c}+1;
            trvec{c}=[trvec{c} tr];
            col = [(numel(PC) - tr)/numel(PC) .2 (0 + tr)/numel(PC)];
            subplot(2,9,c)
            lo{c}(linen{c})=loglog(x,y,'-','Color',col,'linewidth',1);
            hold on
            loglog(x_excl,y_excl,'--','Color',col,'linewidth',1);
            loglog([x_outl;x],exp(b(1)).*[x_outl;x].^b(2),'--m')
            loglog(x_outl+(randn(size(x_outl))-.5)/20,y_outl,'s','MarkerFaceColor',col,'color',col)
            %pause
        end
    end
end


if plotting_flag == 1
    for c=1:18
        if linen{c}>0
            %legend(lo{correct+1,per}(1:5:end),num2str(trvec{correct+1,per}(1:5:end)'))
            subplot(2,9,c)
            hold off
            axis square
            if numel(x)>10
                xlim([.5 1e2])
                ylim([1e-1 1e2])
                set(gca,'XTick',10.^(0:1:2))
            end
            grid on
            xlabel('PC dimension')
            ylabel('R^2')
            text(.4,.9,['\langle\beta\rangle=' num2str(mean(bvec{c}),'%.2f')],'units','normalized','fontsize',16,'fontname','Arial')
        end
    end
end

if save_plots_flag == 1
    set(gcf,'Position',[1 1 2400 800])
    f=fullfile(fpath1,['loglog_pcs_per_trials_' data_struct.monkey '_' num2str(PC{1}.session_id) '_' datestr(now,'yyyy-mm-dd-HHMMSS') '.jpeg']);
    print('-djpeg','-r300',f)
end
