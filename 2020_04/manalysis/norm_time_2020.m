function [sdftime_units,stims_norm,lims_norm,time_warp_factor] = norm_time(sdftime,lims,events,center)

num_prior_stims = sum(events<center);
num_post_stims = sum(events>center);
time_warp_factor=[];

sdftime_units = sdftime;
index = logical(sdftime<events(1));
if sum(index)>0
    sdftime_units(index) = (sdftime_units(index)-events(1)-num_prior_stims);
end
time_warp_factor=horzcat(time_warp_factor,1);
for c=1:num_prior_stims
    index = logical((sdftime>=events(c)).*(sdftime<events(c+1)));
    d = (events(c+1)-events(c));
    time_warp_factor=horzcat(time_warp_factor,d);
    if sum(index)>0
        sdftime_units(index) = (sdftime_units(index)-events(c))./d-num_prior_stims+(c-1);
        
    end
end
for c=(num_prior_stims+1):(num_prior_stims+1+num_post_stims-1)
    index = logical((sdftime>=events(c)).*(sdftime<events(c+1)));
    d = (events(c+1)-events(c));
    time_warp_factor=horzcat(time_warp_factor,d);
    if sum(index)>0
        sdftime_units(index) = (sdftime_units(index)-events(c))./(events(c+1)-events(c))+(c-1-num_prior_stims);
    end
end
index = logical(sdftime>=events(end));
time_warp_factor=horzcat(time_warp_factor,1);
if sum(index)>0
    sdftime_units(index) = (sdftime_units(index)-events(end))+num_post_stims;
end

stims_norm = ((-num_prior_stims):num_post_stims)';
lims_norm(1) = lims(1) - events(1) - num_prior_stims;
lims_norm(2) = lims(2) - events(end);

% subplot(2,1,1)
% plot(sdftime,sdftime*0,'-k')
% hold on
% plot(lims,lims*0,'s')
% plot(stims,stims*0,'v')
% plot(center,center*0,'^')
% hold off
%
% subplot(2,1,2)
% plot(sdftime_units,sdftime_units*0,'-k')
% hold on
% plot(lims_norm,lims_norm*0,'s')
% plot(stims_norm,stims_norm*0,'v')
% plot(0,0,'^')
% hold off