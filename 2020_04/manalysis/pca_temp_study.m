x=sin((1:1e3)'./1e1+pi./(2:5)).*[1:4]+randn(1e3,4)*.3+(0:1:3);
[p,sc,~,~,~,mu]=pca(x);

% Дурак!
figure(1)
plot(0+x(:,:),'linewidth',2)
hold on
plot(10+sc(:,:),'linewidth',2)

selected = [1 2 3 4];
plot(20+(p(selected,:)\((x(:,selected)-mu(selected)))')','-','linewidth',2)
plot(30+(p(selected,:)'*((x(:,selected)-mu(selected)))')','-','linewidth',2)
hold off

% plot(20+(p(:,:)*((x(:,:)-mu)./std(x))')','linewidth',2)
% plot(30+sc(:,:)*p(:,:)'+mu,'linewidth',2)
% plot(40+(x(:,:)-mu)./std(x)*p(:,:)','linewidth',2)
% plot(-10+sc(:,:)*p(:,:)+mu,'linewidth',2)


figure(2)
plot(var(x-mean(x),1,2))
hold on
plot(dot(x-mean(x),x-mean(x),2).^.5,'linewidth',2)
plot(mean(x-mean(x),2))
plot(dot(sc,sc,2).^.5,'--','linewidth',2)
hold off

