function out = ts_amp_and_noise_of_oscillations_to_select_neurons(sdf,params_set,cut_point_start,cut_point_end)
% Neuron selection params. A "weak" and a "strong" set. Try the stonger 
% thresholds first, then go to the weaker if selected neurons set is empty.

% Different levels of selection criteria. Modulation and max amp. (Later, 
% positive slope along the peaks.)
threshold_mod = [1 2 5];
threshold_amp = [1 2 5];

threshold_mod = threshold_mod(params_set);
threshold_amp = threshold_amp(params_set);

if (max(sdf.time)>cut_point_end) && (min(sdf.time)<cut_point_start)
    temp1 = sdf.sdf(logical((sdf.time<cut_point_end).*(sdf.time>cut_point_start)));
    temp2 = sdf.var(logical((sdf.time<cut_point_end).*(sdf.time>cut_point_start)));
else
    temp1 = sdf.sdf;
    temp2 = sdf.var;
end
temp2(temp2<0)=0;

amp_max = max(temp1);
amp_mean = mean(temp1);
amp_sig = range(temp1)/2;
amp_noise = mean(temp2.^.5);
amptonoise = amp_sig/amp_noise;

if ~isreal(amp_noise);keyboard;end

% Select based on total amp and modulation depth, and that it's increasing.
try b=[sdf.time.^0 sdf.time]\sdf.sdf;catch me;b=[1 1]*0;end
selected = (amp_max > threshold_amp).*(amp_sig > threshold_mod).*(b(2)>0);
out = [selected,amp_max,amp_mean,amp_sig,amp_noise,amptonoise];