function speed_stats = ts_cbc_speed(xx,tt,plotting,varargin)

if numel(varargin)>0
    sr = varargin{1};
    dt = 1/sr;
else
    dt = mean(diff(tt));
    sr = 1/dt;
end
if numel(varargin)>1
    stims = varargin{2};
else
    stims = ceil(min(tt./1)):1:floor(max(tt./1));
end

conv_win_len_ms = 2e2; % msec
gaussFilter = gausswin(round(conv_win_len_ms/1e3*sr*1));
gaussFilter = gaussFilter./sum(gaussFilter);
xxf = nan(size(xx));
for k = 1:size(xx,2)
    xxf(:,k) = conv(xx(:,k),gaussFilter,'same');
end

cycle_t = [];
speed_in_each_cycle = [];
speed_in_each_cycle_f = [];
for k = 1:numel(stims)+1
    if k==1
        if tt(1) == stims(k)
            continue
        end
        start = tt(1);
        stop = stims(1);
    elseif k==numel(stims)+1
        if tt(end) == stims(k-1)
            continue
        end
        start = stims(k-1);
        stop = tt(end);
    else
        start = stims(k-1);
        stop = stims(k);
    end
    index = tt>=start & tt<=stop;
    cycle_t = vertcat(cycle_t,mean(tt(index)));
    s = nanmean(dot(diff(xx(index,:)),diff(xx(index,:)),2).^.5./dt);
    speed_in_each_cycle = vertcat(speed_in_each_cycle,s);
    s = nanmean(dot(diff(xxf(index,:)),diff(xxf(index,:)),2).^.5./dt);
    speed_in_each_cycle_f = vertcat(speed_in_each_cycle_f,s);
end


if isempty(speed_in_each_cycle) || numel(speed_in_each_cycle)<2
    speed_stats.params = nan(1,6);
    speed_stats.speed = [];
    speed_stats.speed_f = [];
    return
end

b = [speed_in_each_cycle.^0 (1:numel(speed_in_each_cycle))']\speed_in_each_cycle;
bf = [speed_in_each_cycle_f.^0 (1:numel(speed_in_each_cycle_f))']\speed_in_each_cycle_f;

p1 = mean(speed_in_each_cycle);if isempty(p1);p1=nan;end
p2 = b(1);if isempty(p2);p2 = nan;end
p3 = b(2);if isempty(p3);p3 = nan;end
p4 = mean(speed_in_each_cycle_f);if isempty(p4);p4=nan;end
p5 = bf(1);if isempty(p5);p5 = nan;end
p6 = bf(2);if isempty(p6);p6 = nan;end
speed_stats.params = [p1 p2 p3 p4 p5 p6];

speed_stats.speed = [cycle_t speed_in_each_cycle speed_in_each_cycle_f];

speed_stats.labels = {'Speed','b0_{speed}','b1_{speed}',...
    'Speed*','b0_{speed*}','b1_{speed*}'};

if plotting==1
    figure(198627);clf
    subplot(2,1,1)
    plot(tt,xx,'-','linewidth',2);
    hold on
    plot(tt,xxf,'--','linewidth',2);
    hold off
    subplot(2,1,2)
    l(1)=plot(cycle_t,speed_in_each_cycle,'dr','linewidth',2,'MarkerSize',10);
    hold on
    l(2)=plot(cycle_t,speed_in_each_cycle_f,'om','linewidth',2,'MarkerSize',10);
    hold off
    legend(l,'speed','speed of smoothed')
    set(legend,'location','northwest','fontsize',12);
end

end