function plot_3d_cool_color(t,x,varargin)

% Strange behavior of ds. Needs correction.
if ~isempty(varargin)
    ds = varargin{1};
else
    ds = 1e0;
end

if numel(varargin)>1
    smooth_param = varargin{2};
else
    smooth_param = 1e0;
end

if smooth_param > 1
    for d = 1:size(x,2)
        slv = smooth(x(:,d),smooth_param);
        ind = ~isnan(x(:,d));
        x(ind,d) = slv(ind);
    end
end
x = x(1:ds:end,:);
t = t(1:ds:end);

for d = 1:size(x,2)
    ind = ~isnan(x(:,d));
    start = find(ind,1,'first');
    stop = find(ind,1,'last');
end

if numel(varargin) > 2
    markers = varargin{3};
    [~,markers] = min(abs(t - markers));
else
    markers = [];
end

if numel(varargin)>3 && ~isempty(varargin{4})
    center = varargin{4};
    [~,center] = min(abs(t - center));
else
    center = [];
end
ds = 1;

color_vec = cool(numel(t))*.9;
for n=(ds+1):ds:numel(t)
    if isnan(t(n))
        continue
    end
    plot3(x([n-ds,n],1),x([n-ds,n],2),x([n-ds,n],3),'-','MarkerSize',1,'Color',[color_vec(n,:) .7],'linewidth',3)
    % plot3(x([n-ds,n],1),x([n-ds,n],2),x([n-ds,n],3),'-','MarkerSize',1,'Color',[color_vec(n,:)],'linewidth',3)
    hold on
end

p(1) = plot3(x(start,1),x(start,2),x(start,3),'^','Color',color_vec(1,:),'linewidth',2,'MarkerSize',8);
p(2) = plot3(x(stop,1),x(stop,2),x(stop,3),'v','Color',color_vec(end,:),'linewidth',2,'MarkerSize',8);
if ~isempty(markers)
    p(3) = plot3(x(round(markers./ds),1),x(round(markers./ds),2),x(round(markers./ds),3),...
        'ob','linewidth',3,'MarkerSize',10); % ,'MarkerFaceColor',[.6 .7 .9]
end
if ~isempty(center)
    p(4) = plot3(x(round(center./ds),1),x(round(center./ds),2),x(round(center./ds),3),'s',...
        'Color',color_vec(round(size(color_vec,1)./2),:),'linewidth',2,'MarkerSize',8); % ,'MarkerFaceColor',[.9 .7 .4]
end
hold off
grid on
set(gca,'color',ones(1,3)*.2)
set(gcf,'color',ones(1,3)*.1)

ax=gca;
ax.XColor=ones(1,3)*.9;
ax.YColor=ones(1,3)*.9;
ax.ZColor=ones(1,3)*.9;

l=legend(p,'Start','End','Stimuli','Tapping Start','location','northwest');
set(l,'color',[.32 .32 .32]);

if ~isempty(varargin) && numel(varargin)>4
    xlabel(['PC_' num2str(varargin{5}(1), '%1.f')]);
    ylabel(['PC_' num2str(varargin{5}(2), '%1.f')]);
    zlabel(['PC_' num2str(varargin{5}(3), '%1.f')]);
else
    xlabel('x');ylabel('y');zlabel('z');
end
