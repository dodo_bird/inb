

%% Prep paths
[~,pc]=system('uname -a');
if ~isempty(regexp(pc,'pop-os','once'))
    data_folder = '~/storage/inb_data/';
    addpath('~/logos/inb/2021/manalysis')
end
if ~isempty(regexp(pc,'M93p','once'))
    data_folder = '~/storage/inb_data/';
    addpath('~/logos/inb/2021/manalysis/')
end


C = [];
for monkey = 1:3
    
    %% Move to the target folder
    switch monkey
        case 1
            f = dir(fullfile(data_folder,['M' num2str(monkey,'%1.f') '-TBC*']));
        case 2
            f = dir(fullfile(data_folder,['M' num2str(monkey,'%1.f') '-TBC*']));
        case 3
            f = dir(fullfile(data_folder,['M' num2str(monkey,'%1.f') '*']));
    end
    base_folder = fullfile(f.folder,f.name);
    cd(base_folder)
    
    file_name_sessions = dir('mid_processing/mid_processing_*');
    
    %% these are the conditions that define how to group trials. at the end of
    % this, each "trial" in the output array will stand for the average of all
    % trials satisfying the given conditions.
    if monkey==3
        intervals_vec = [.55 .65 .75 .85];
        task_vec = [1 2];
        periodic_cond_vec = 1;
    else
        intervals_vec = [.45 .55 .65 .75 .85 .95];
        task_vec = 1; % This doesn't actually matter
        periodic_cond_vec = 1;
    end
    
    
    %%
    for s = 1:numel(file_name_sessions)
        fprintf('%s\n',file_name_sessions(s).name)
        load(fullfile(file_name_sessions(s).folder,file_name_sessions(s).name),'data_struct')
        for task=task_vec
            for p=periodic_cond_vec
                for int=intervals_vec
                    for tr=1:numel(data_struct.correct)
                        if ((monkey ~= 3) || (data_struct.task(tr)==task)) && ...
                                data_struct.periodic_index(tr)==p && ...
                                data_struct.interval(tr)==int
                            if monkey == 1;session=data_struct.session_id;end
                            if monkey == 2;session=str2double(data_struct.session_id);end
                            if monkey == 3;session=nan;end
                            C = vertcat(C,[monkey session data_struct.task(tr) p int data_struct.correct(tr)]);
                        end
                    end
                end
            end
        end
    end
end

C = array2table(C,'VariableNames',{'Monkey','Session','Task','Periodic','Interval','Correct'});
writetable(C,fullfile(base_folder,'..',['trials_correct_' datestr(date) '.csv']));
