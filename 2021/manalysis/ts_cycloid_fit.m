function sdf_fit_by_cont_osc = ts_cycloid_fit(xx,tt,plotting,varargin)
%%Fit a cycloid-like function with increasing amplitude and return params
% and their 95% CI.

resfun=@(a0,a1,b0,b1,theta,fr,x)((a0+a1*x)+(b1*x+b0).*(-abs(cos(2*pi*fr*x./2+pi*fr/2-theta*fr/2))+1));

if numel(varargin)>0
    period = varargin{1};
else
    period = 1;
end

sdf_fit_by_cont_osc.cont_nl_regression_labels={...
    'a_{0,-95%}','a_{0}','a_{0,+95%}',...
    'a_{1,-95%}','a_{1}','a_{1,+95%}',...
    'b_{0,-95%}','b_{0}','b_{0,+95%}',...
    'b_{1,-95%}','b_{1}','b_{1,+95%}',...
    '\Delta','\theta','nan',...
    'R^2','RMSE'};

fo = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',[-10,-10,-100,-100,-2*pi,(1/period)],...
    'Upper',[100, 10, 200, 100, 2*pi,(1/period)],...
    'StartPoint',[0,0,10,10,pi/4,(1/period)]);

ft = fittype(resfun,'options',fo);
[curve,gof] = fit(repmat(tt,size(xx,2),1),xx(:),ft);

cofs=[coeffvalues(curve);confint(curve)]';
cofs=cofs(1:5,[2 1 3]);
cofs(5,:)=nan;

yy=resfun(curve.a0,curve.a1,curve.b0,curve.b1,curve.theta,curve.fr,tt);
[~,events]=findpeaks(yy./max(yy),'MinPeakDistance',.2,'MinPeakProminence',.05);
[~,ind]=min(abs(tt(events)));
delta=tt(events(ind));
if isempty(delta);delta=nan;end
cofs(5,1)=delta;
cofs(5,2)=mod(delta,period)/period*2*pi;

res = xx-yy;
stim_events = min(ceil(tt)):max(floor(tt));
r2vec = nan(numel(stim_events)-1,2);
for s = 1:(numel(stim_events)-1)
    r2vec(s,1) = stim_events(s+1);
    r2vec(s,2) = corr(xx((tt>stim_events(s)) & (tt<stim_events(s+1))),yy((tt>stim_events(s)) & (tt<stim_events(s+1)))).^2;
end

sdf_fit_by_cont_osc.cont_nl_regression = [reshape(cofs',1,[]) gof.rsquare std(xx-yy)];
sdf_fit_by_cont_osc.r2vec = r2vec;

if plotting == 1
    figure(198623);clf
    plot(tt./period,xx,'-','LineWidth',1)
    hold on
    plot(tt./period,yy,'-r','linewidth',3)
    plot(tt./period,res,'-k','linewidth',2)
    plot(curve.theta/2/pi./period,0,'^r','linewidth',2)
    plot(delta./period,0,'vm','linewidth',2)
    hold off
end
