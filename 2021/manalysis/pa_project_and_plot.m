%%
clearvars -except 'monkey'
if ~exist('monkey','var')
    monkey = 1;
end
scale_ind_cell_activity = false;
target_sr_per_cycle = 50;
plotting_flag = 0;
lnl = 1;
switch lnl
    case 1
        linear_or_nonlinear_pca = 'linear';
    case 2
        linear_or_nonlinear_pca = 'nonlinear';
end


%% Prep paths
set_workspace_path;


%% Load again
load('pca_global_space.mat')
load('SDFS_aggregate_stage.mat')
selected_dimensions = 1:num_dims;


%% Project neurons over population activity per session (M 1-2) or trial (M 3).
% Session means condition x session.
if any(monkey==[1 2])
    counter_plus_session = 0;
    LV.lv = cell(numel(SDFS.sdf),1);
    LV.cell_id = cell(numel(SDFS.sdf),1);
    LV.time = cell(numel(SDFS.sdf),1);
    for counter = 1:numel(SDFS.sdf)
        session_ids = unique(round(SDFS.cell_id{counter}./1e4));
        for s = 1:numel(session_ids)
            cells_alive = ~all(isnan(SDFS.sdf{counter}),1); % remove any period with at least one unit in a nan state.
            matching_mat = (((cell_ids_retained_c==SDFS.cell_id{counter}').*(cells_alive')).*(round(cell_ids_retained_c./1e4) == session_ids(s))).*((round(SDFS.cell_id{counter}./1e4) == session_ids(s))');
            index_matching_cells_in_session_for_P = find(sum(matching_mat,1)==1);
            index_matching_cells_in_session_for_SDF = find(sum(matching_mat,2)==1)';
            
            cells_in_session = SDFS.cell_id{counter}(index_matching_cells_in_session_for_SDF);
            x = SDFS.sdf{counter}(:,index_matching_cells_in_session_for_SDF);
            index_nan_in_time = any(isnan(x),2); % remove rows with at least one nan.
            if ~all(index_nan_in_time) && ~isempty(x)
                x = x(~index_nan_in_time,:);
                x = resample(x, round(target_sr_per_cycle/SDFS.conds_interval(counter)), round(1/mean(diff(SDFS.time{counter}))));
                
                x = x - mean_cells_c(index_matching_cells_in_session_for_P); % ? why not for sdf?
                x = x./scale_by_var_cells_c(index_matching_cells_in_session_for_P);
                xfull = zeros(size(x,1),numel(mean_cells_c));
                xfull(:,index_matching_cells_in_session_for_P) = x;
                
                t = linspace(min(SDFS.time{counter}(~index_nan_in_time,:)),...
                    max(SDFS.time{counter}(~index_nan_in_time,:)),size(x,1))';
                
                lv = (P(index_matching_cells_in_session_for_P,:)'*x')';
                switch linear_or_nonlinear_pca
                    case 'linear'
                    case 'nonlinear'
                        % lv = nlpca_get_components(npcanet, xfull')';
                        pre_pca_dims = npcanet.units_per_layer(1);
                        lv = nlpca_get_components(npcanet, lv(:,1:pre_pca_dims)')';
                end

                fprintf('%6.0f,',SDFS.conds_succ(counter),SDFS.conds_periodic(counter),...
                    SDFS.conds_interval(counter)*1e3,...
                    session_ids(s),size(lv,1),size(lv,2));
                fprintf('\n')
                
                counter_plus_session = counter_plus_session + 1;
                LV.conditions(counter_plus_session,:) = ...
                    [SDFS.conds_succ(counter),SDFS.conds_periodic(counter),...
                    SDFS.conds_interval(counter),SDFS.conds_task(counter)];
                LV.lv{counter_plus_session,1} = lv;
                LV.cell_id{counter_plus_session,1} = cells_in_session;
                LV.time{counter_plus_session,1} = t;
            end
        end
    end
end

% Trial means condition x a trial picked from each session. So data is
% merged across sessions on a trial order x condition basis.
if monkey==3
    file_name_sessions = dir('mid_processing_*');
    load(fullfile(file_name_sessions(1).folder,file_name_sessions(1).name),'data_struct')
    session_ids = 1;
    
    trial = 0;
    LV.lv = cell(numel(data_struct.SDF_RAW),1);
    LV.cell_id = cell(numel(data_struct.SDF_RAW),1);
    LV.time = cell(numel(data_struct.SDF_RAW),1);
    for counter = 1:numel(data_struct.SDF_RAW)
        cells_alive = ~all(isnan(data_struct.SDF_RAW{counter}.sdf),1); % remove any period with at least one unit in a nan state.
        matching_mat = (cell_ids_retained_c==(data_struct.SDF_RAW{1}.cell_ids+1e4)').*(cells_alive');
        index_matching_cells_in_session_for_P = find(sum(matching_mat,1)==1);
        index_matching_cells_in_session_for_SDF = find(sum(matching_mat,2)==1)';
        
        cells_in_session = data_struct.SDF_RAW{counter}.cell_ids(index_matching_cells_in_session_for_SDF);
        x = data_struct.SDF_RAW{counter}.sdf(:,index_matching_cells_in_session_for_SDF);
        index_nan_in_time = any(isnan(x),2); % remove rows with at least one nan.
        if ~all(index_nan_in_time) && ~isempty(x)
            x = x(~index_nan_in_time,:);
            % x = resample(x,5,round(SDFS.conds_interval(counter)*1e2));
            x = resample(x, round(target_sr_per_cycle/data_struct.interval(counter)), ...
                round(1/mean(diff(data_struct.SDF_RAW{counter}.time))));
            
            x = x - mean_cells_c(index_matching_cells_in_session_for_P);
            x = x./scale_by_var_cells_c(index_matching_cells_in_session_for_P);
            
            
            t = linspace(min(data_struct.SDF_RAW{counter}.time(~index_nan_in_time,:)),...
                max(data_struct.SDF_RAW{counter}.time(~index_nan_in_time,:)),size(x,1))';
            
            switch linear_or_nonlinear_pca
                case 'linear'
                    lv = (P(index_matching_cells_in_session_for_P,:)'*x')';
                case 'nonlinear'
                    lv = nlpca_get_components(npcanet, xfull')';
            end
            
            fprintf('%6.0f,',data_struct.correct(counter),data_struct.periodic_index(counter),...
                data_struct.interval(counter)*1e3,...
                session_ids,size(lv,1),size(lv,2));
            fprintf('\n')
            
            trial = trial + 1;
            LV.conditions(trial,:) = ...
                [data_struct.correct(counter),data_struct.periodic_index(counter),...
                data_struct.interval(counter),data_struct.task(counter)];
            LV.lv{trial,1} = lv;
            LV.cell_id{trial,1} = cells_in_session;
            LV.time{trial,1} = t;
        end
    end
end

% Quick illustration
if plotting_flag == 1
    for counter_plus_session = 1:numel(LV.lv)
        fprintf('%6.0f,',LV.conditions(counter_plus_session,:).*[1 1 1e2 1],...
            size(LV.lv{counter_plus_session,1},1),size(LV.lv{counter_plus_session,1},2));
        fprintf('\n')
        index = (LV.time{counter_plus_session,1}./LV.conditions(counter_plus_session,3))>-3;
        for d=1:6
            %plot(LV.time{counter_plus_session,1}./LV.conditions(counter_plus_session,3),smooth(LV.lv{counter_plus_session,1}(:,d),1e1),'-','linewidth',2)
            plot(LV.time{counter_plus_session,1}(index)./LV.conditions(counter_plus_session,3),LV.lv{counter_plus_session,1}(index,d),'-','linewidth',2)
            hold on
        end
        clear index
        hold off
        set(gca,'color','k')
        pause
    end
end


%% Average neurons across sessions in the same condition
clear LVave
LVave.conditions = zeros(0,4);
LVave.lv = cell(0);
LVave.cell_id = cell(0);
LVave.time = cell(0);
LVave.count_n = cell(0);
counter = 0;
for task = unique(LV.conditions(:,4))'
    for correct = unique(LV.conditions(:,1))'
        for p = 1 % only periodic
            for interval = unique(SDFS.conds_interval)'
                counter = counter + 1;
                min_t = nan;
                max_t = nan;
                sr = [];
                for l = 1:numel(LV.time)
                    if all(LV.conditions(l,1:4) == [correct p interval task])
                        %if task==LV.conditions(l,4) || isnan(LV.conditions(l,4))
                            min_t = min(min_t,min(LV.time{l}));
                            max_t = max(max_t,max(LV.time{l}));
                            sr = vertcat(sr,1/mean(diff(LV.time{l})));
                        %end
                    end
                end
                min_t = min_t - .1;
                max_t = max_t + .1;
                sr = mean(sr);
                
                LVave.conditions(counter,:) = [correct p interval task];
                LVave.time{counter} = linspace(min_t,max_t,round((max_t-min_t)*sr))';
                LVave.lv{counter} = nan(size(LVave.time{counter},1),size(LV.lv{1},2)); % as many pc dimensions
                LVave.count_n{counter} = LVave.time{counter}*0;
                [~,ind1] = min(abs(LVave.time{counter}-0));
                for l = 1:numel(LV.lv)
                    if all(LV.conditions(l,:) == [correct p interval task])
                        [~,ind2] = min(abs(LV.time{l}-0));
                        index = ((ind1-ind2+1):(size(LV.time{l},1)-ind2+ind1))';
                        if any(index<1);keyboard;index(index<1)=[];end
                        LVave.count_n{counter}(index) = LVave.count_n{counter}(index) + 1;
                        for n=1:size(LV.lv{l},2)
                            LVave.lv{counter}(index,n) = nansum([LVave.lv{counter}(index,n),LV.lv{l}(:,n)],2);
                            % LVave.cell_id{counter}(n) = LV.cell_id{l}(n);
                        end
                        clear index
                    end
                end
                index = any(isnan(LVave.lv{counter}),2);
                LVave.lv{counter}(index,:) = [];
                LVave.time{counter}(index) = [];
                LVave.count_n{counter}(index) = [];
                LVave.lv{counter} = LVave.lv{counter}./LVave.count_n{counter};
                clear index
            end
        end
    end
end

% Save
% save(fullfile(base_folder,'mid_processing','pca_LVs.mat'),'LV','LVave','-v7.3')


%% See each dimension as a timeseries.
% load('pca_LVs.mat')
if plotting_flag == 2
    figure(163237)
    color_vec = winter(numel(selected_dimensions))*.9;
    for l = 1:numel(LVave.lv)
        if any(monkey==[1 2]);subplot(2,6,l);end
        if any(monkey==3);subplot(2,4,l);end
        t = LVave.time{l}./LVave.conditions(l,3);
        x = LVave.lv{l}(:,selected_dimensions);
        for d = selected_dimensions
            plot(t(t>-inf),x(t>-inf,d)./max(abs(x(:,d)))+2*d,'color',color_vec(d,:),'linewidth',2)
            hold on
        end
        hold off
        set(gca,'YTick',[])
        set(gca,'XTick',-10:2:10)
        text(.1,.95,char(64+l),'units','normalized','fontsize',14)
        xlabel('Stimulus cycles')
        if any(monkey==1);xlim([-8 7]);ylim([0 2*d+1]);line([0 0],[-1e3 0],'color','k','linewidth',1);end
        if any(monkey==2);xlim([-5 5]);ylim([0 2*d+1]);line([0 0],[-1e3 0],'color','k','linewidth',1);end
        if any(monkey==3);xlim([-9 6]);ylim([0 2*d+1]);line([0 0],[-1e3 0],'color','k','linewidth',1);end
        set(gca,'fontsize',12)
    end
    legend(['PC '.*ones(6,1) num2str((1:6)')],'location','southwest')
    %{
    set(gcf,'Position',[1 1 2040 680])
    set(gcf,'InvertHardcopy','off')
    f=fullfile(base_folder,'mid_processing',['pca_trial_1d_trajectories_per_condition_monkey' num2str(monkey) '_' ...
        char(datetime('now','TimeZone','local','Format','y-MM-d-hhmmss')) '.jpeg']);
    print('-djpeg','-r300',f)
    %}
end


%% Classification of tapping stage of trial-averaged LVs and visualizing in 3D
% load('pca_LVs.mat')
% Concatenate each condition of the LVave in the same long array.
LVALL.conditions = [];
LVALL.t = [];
LVALL.Y = [];
LVALL.X = [];
for l=1:numel(LVave.lv)
    t = LVave.time{l}./LVave.conditions(l,3);
    index = t>-5; % that many stims before first tap.
    t = t(index);
    x = LVave.lv{l}(index,:);
    LVALL.conditions = [LVALL.conditions; repmat(LVave.conditions(l,:),numel(t),1)];
    LVALL.t = [LVALL.t; t];
    LVALL.Y = [LVALL.Y; t>0];
    LVALL.X = [LVALL.X; x];
end
svmModel = pa_svm(LVALL.Y,LVALL.X(:,selected_dimensions),0);


%% Nicer looking 3D trajectories of LVs averaged across trials
% load('pca_LVs.mat')
decision_plane_flag = false;
if plotting_flag == 2
    figure(163236)
    if any(monkey==[1 2]);show_dimensions = [1 2 3];end
    if any(monkey==3);show_dimensions = [3 4 5];end
    for l=1:numel(LVave.lv)
        t = LVave.time{l}./LVave.conditions(l,3);
        index = t>-5; % that many stims before first tap.
        t = t(index);
        x = LVave.lv{l}(index,show_dimensions);
        stims = find(diff(mod(t,1))<-.5);
        if any(monkey==[1 2]);subplot(2,6,l);end
        if any(monkey==3);subplot(2,4,l);end
        plot_3d_cool_color(t,x,1,10,t(stims)',0,show_dimensions);
        xlabel(['PC_' num2str(show_dimensions(1))])
        ylabel(['PC_' num2str(show_dimensions(2))])
        zlabel(['PC_' num2str(show_dimensions(3))])
        set(gca,'XTickLabels',[]) %axis off
        set(gca,'YTickLabels',[]) %axis off
        set(gca,'ZTickLabels',[]) %axis off
        
        if decision_plane_flag
            model_equation = [svmModel.Bias svmModel.Beta'];
            [x1Grid, x2Grid, ~] = meshgrid(...
                linspace(min(x(:,show_dimensions(1))), max(x(:,show_dimensions(1))), 50), ...
                linspace(min(x(:,show_dimensions(2))), max(x(:,show_dimensions(2))), 50), ...
                linspace(min(x(:,show_dimensions(3))), max(x(:,show_dimensions(3))), 50));
            hold on
            p = draw_plane(model_equation([1 show_dimensions+1]), x1Grid(:), x2Grid(:));
            p.FaceColor = [.8 .5 .4];
            p.FaceAlpha = .2;
            hold off
            g = gcf;
            c = g.get("Children");
            c(1).String{end} = 'Classifier';
            c(1).TextColor = 'w';
            c(1).Box = 'off';
        end
        % set(gca,'view',[31 15])
        set(gca,'view',[-140 20])
    end
    %{
    set(gcf,'visible','off')
    set(gcf,'InvertHardcopy','off')
    set(gcf, 'PaperPosition', [0 0 30 10])
    f=fullfile(base_folder,['pca_pop_trajectories_per_conditions_and_plane_d1-3_monkey' num2str(monkey) '_' char(datetime('now','TimeZone','local','Format','y-MM-d-hhmmss')) '.jpeg']);
    print('-djpeg','-r300',f)
    set(gcf,'visible','on')
    %}
end
