%%
clearvars -except 'monkey'
if ~exist('monkey','var')
    monkey = 1;
end
scale_ind_cell_activity = false;
target_sr_per_cycle = 50;


%% Prep paths
set_workspace_path;


%% These mat files are sessions w/ sorted spike times and behav events
file_name_sessions = dir('mid_processing_*');


%% Put SDFs in the same structure, w/ trial time limits and aggregated by condition
SDFS = aggredate_sdfs_over_trials_sessions(file_name_sessions, monkey);


%% load
% load('SDFS_aggregate_stage.mat','SDFS')


%% Get the cell ids
found_cells = [];
if monkey == 1
    % Monkey 1 has more trials w/ .85
    comb_conditions = [1 2 3 4 6 7 8 9 10 12];
else
    comb_conditions = 1:numel(SDFS.cell_id);
end
for c = comb_conditions
    found_cells = horzcat(found_cells,SDFS.cell_id{c});
end
found_cells = sort(unique(found_cells));


%% Resample per condition and concat.
% This will give target_sr_per_cycle samples per stimulus interval.
SDF_huge = [];
var_cells = found_cells*nan;
mean_cells = found_cells*nan;

if any(monkey==[1 2])
    conditions_vec = 7:12;
else
    conditions_vec = 1:8;
end

for counter = conditions_vec
    sdfds = resample([nan(1,size(SDFS.sdf{counter},2));SDFS.sdf{counter};nan(1,size(SDFS.sdf{counter},2))], ...
        round(target_sr_per_cycle/SDFS.conds_interval(counter)), round(1/mean(diff(SDFS.time{counter}))));
    sdfds_temp = nan(size(sdfds,1),numel(found_cells));
    for c = 1:size(sdfds,2)
        ind = find(SDFS.cell_id{counter}(c)==found_cells);
        if ~isempty(ind)
            var_cells(counter,ind) = nanvar(sdfds(:,c));
            mean_cells(counter,ind) = nanmean(sdfds(:,c));
            sdfds_temp(:,ind) = sdfds(:,c);
        end
    end
    SDF_huge = [SDF_huge;sdfds_temp];
end
% btw, it would be quicker to place in one long array w/out resampling.


%% Remove empty cells and periods
temp = ~all(isnan(SDF_huge),1);
cell_ids_retained_c = found_cells(temp);
SDF_huge_clean = SDF_huge(~all(isnan(SDF_huge),2),temp);

percent_cells_threshold = .1;
% Why? At this level, pretty much all data from bad trials will be excluded.
SDF_huge_clean(sum(isnan(SDF_huge_clean),2)./size(SDF_huge_clean,2)>percent_cells_threshold,:) = [];

SDF_huge_clean(isnan(SDF_huge_clean)) = 0;
mean_cells_c = nanmean(SDF_huge_clean);
if scale_ind_cell_activity
    scale_by_var_cells_c = nanvar(SDF_huge_clean);
else
    scale_by_var_cells_c = ones(1,size(SDF_huge_clean,2));
end


%% Normalize or not.
% Softmax, divide by the max variance from all conditions, or divide by 1.
% Or divide by neuron maximum?
SDF_huge_clean = SDF_huge_clean - mean_cells_c;
SDF_huge_clean = SDF_huge_clean./scale_by_var_cells_c;


%% Holy grail
% Linear
[P,SDF_reduced_pca,~,~,explained_c,mu_cells_c] = pca(SDF_huge_clean);
% [P,sc,~,~,explained_c,mu_cells_c] = pca(SDF_huge_clean(1:10:end,:),'algorithm','als');
% [P,sc,~,~,explained_c,mu_cells_c] = pca(SDF_huge_clean,'Rows','pairwise'); % by now it's the same deal cause we've cleaned the sdf mat a lot.
[outliers_removed,total_outliers,zfit,sumr2,outliers_table] = rank_frequency_outliers_from_top(explained_c,0);

% Non-linear
num_dims = total_outliers;
% [~, npcanet] = nlpca(SDF_huge_clean', num_dims, 'plotting','no');
% SDF_reduced = nlpca_get_components(npcanet, SDF_huge_clean');


%% To cross-validate the linear and nonlinear PCA approach to the manifold.
compare_nlpca_pca_r; % This re-trains the nlpca in k folds, so long duration.


%% Save the PC coefficients
%{
save(fullfile(base_folder,'mid_processing','pca_global_space.mat'),...
    'SDF_huge_clean','P','cell_ids_retained_c','scale_by_var_cells_c','mu_cells_c','explained_c','mean_cells_c','scale_by_var_cells_c',...
    'npcanet','nlpca_training_scenario','num_dims')
%}


%% Junk
% ts_len = size(SDFS.sdf{1},1);
% total_unit_count = [];
% for c = 1:size(SDFS.cell_id,1)
%     total_unit_count(c,1) = numel(SDFS.cell_id{c});
%     if ts_len ~= size(SDFS.sdf{c},1)
%         keyboard
%     end
% end
% [~,ind] = max(total_unit_count);
% count_id_across_all_sessions = SDFS.cell_id{ind};
% if any(found_cells~=sort(count_id_across_all_sessions));keyboard;end
