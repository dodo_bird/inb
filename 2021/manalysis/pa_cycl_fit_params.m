clearvars -except 'monkey'
if ~exist('monkey','var')
    monkey = 3;
end


%% Prep paths and move to the target folder
[~,pc]=system('uname -a');
if ~isempty(regexp(pc,'pop-os','once'))
    data_folder = '/media/dobri/disk2/storage/inb_data/';
    addpath('~/logos/inb/2021/manalysis')
    addpath('~/logos/inb/2021/manalysis/gramm_repo')
end
if ~isempty(regexp(pc,'M93p','once'))
    data_folder = '~/storage/inb_data/';
    addpath('~/logos/inb/2021/manalysis/')
    addpath('~/logos/inb/2021/manalysis/gramm_repo')
end
cd(data_folder)


%%
plotting_flag = 0;
save_plots_flag = 0;

cycles_trial_phases = [-inf 0+.5; 0 inf];
which_pc_to_analyze = 1:10; % 10 decided with the scaling law outliers
max_d = 10; % see the scaling outliers script
min_d = 1; % Because the first 1-2 are obvious.

color_vec = cool(3);


%% Move to the target folder
switch monkey
    case 1
        base_folder = fullfile(data_folder,['M' num2str(monkey,'%1.f') '-TBC']);
    case 2
        base_folder = fullfile(data_folder,['M' num2str(monkey,'%1.f') '-TBC2']);
    case 3
        base_folder = fullfile(data_folder,['M' num2str(monkey,'%1.f')]);
end
load(fullfile(base_folder,'mid_processing','pca_LVs.mat'))


%%
act_median = zeros(numel(LV.lv),size(LV.lv{1},2));
act_range = zeros(numel(LV.lv),size(LV.lv{1},2));
for tr = 1:numel(LV.lv)
    act_median(tr,:) = median(LV.lv{tr}-min(LV.lv{tr}));
    act_range(tr,:) = range(LV.lv{tr});
end
act_median = mean(act_median);
act_range = mean(act_range);


%%
OSC.trial_params_labels(1:9) = {'monkey','session','condition','correct','periodic','interval','task','d','trial-phase'};
OSC.trial_params_labels(10:26) = [{'a_{0,-95%}'}    {'a_{0}'}    {'a_{0,+95%}'}    {'a_{1,-95%}'}    {'a_{1}'}    {'a_{1,+95%}'}    {'b_{0,-95%}'}    {'b_{0}'}, ...
    {'b_{0,+95%}'}    {'b_{1,-95%}'}    {'b_{1}'}    {'b_{1,+95%}'}    {'\Delta'}    {'\theta'}    {'nan'}    {'R^2'}    {'SD'}];
OSC.trial_params = nan(0,26);

OSC.r2vec_labels = {'monkey','session','condition','correct','periodic','interval','task','d','trial-phase','stim_cycle','r2cyclo'};
OSC.r2vec = nan(0,11);

CBC.trial_params_labels(1:9) = {'monkey','session','condition','correct','periodic','interval','task','d','trial-phase'};
CBC.trial_params_labels(10:21) = {'\Delta'    '\theta'    'peakA'    'amp'    'b0_{peaks}'    'b1_{peaks}'    'b1_{\Delta}' ...
    '\Delta_{rise}'    '\theta_{rise}' ...
    '\Delta_{bott}'    'amp_{bott}'    '\SD_{Delta}'};
CBC.trial_params = nan(0,21);

CBC.ind_cycle_labels = {'monkey','session','condition','correct','periodic','interval','task','d','trial-phase','event_num','event_time','x'};
CBC.peak_amp = nan(0,12);
CBC.peak_delta = nan(0,12);
CBC.peak_theta = nan(0,12);
CBC.bottom_amp = nan(0,12);
CBC.bottom_delta = nan(0,12);
CBC.bottom_theta = nan(0,12);
CBC.rise_delta = nan(0,12);
CBC.rise_theta = nan(0,12);

%%
j=0; % rows in the growing array of average trial CBC parameters
for trial_phase = [1 2]
    cycles_start_past_zero_stim = cycles_trial_phases(trial_phase,1);
    cycles_stop_past_zero_stim  = cycles_trial_phases(trial_phase,2);
    
    for tr = 1:numel(LV.lv)
        for d = which_pc_to_analyze
            j=j+1;
            OSC.trial_params(j,1:9) = [monkey tr nan LV.conditions(tr,:) d trial_phase];
            CBC.trial_params(j,1:9) = [monkey tr nan LV.conditions(tr,:) d trial_phase];
            
            fprintf('%6.0f,',OSC.trial_params(j,1:9))
            fprintf('\n')
            if ~isempty(LV.lv{tr}) && size(LV.lv{tr},1)>1e1
                
                xx = LV.lv{tr}(:,d);
                tt = LV.time{tr};
                xx = xx(logical((tt < cycles_stop_past_zero_stim).*(tt > cycles_start_past_zero_stim)),:);
                tt = tt(logical((tt < cycles_stop_past_zero_stim).*(tt > cycles_start_past_zero_stim)));
                xx = xx-min(xx);
                xx(isnan(xx)) = 0;
                
                interval = LV.conditions(tr,3);
                if isnan(interval);interval=1;end
                unit_stims = ceil(min(tt./interval)):floor(max(tt./interval));
                if (sum(xx==0)/numel(xx))<.3 && ~isempty(unit_stims)
                    % 1. Fit a cycloid-like function and get params.
                    cyclofit_cont = ts_cycloid_fit(xx,tt./interval,plotting_flag,1);
                    OSC.trial_params(j,10:end) = cyclofit_cont.cont_nl_regression;
                    OSC.r2vec = [OSC.r2vec; [repmat(OSC.trial_params(j,1:9),size(cyclofit_cont.r2vec,1),1) cyclofit_cont.r2vec]];
                    
                    % 2. Cycle-by-cycle analysis of peaks. % data_struct.SDF_RAW{1}.time_warp_factor
                    % threshold_coef = .5;
                    % threshold_crossing = act_median(d)+threshold_coef*act_range(d);
                    % threshold_crossing = min(xx) + threshold_coef*act_range(d);
                    threshold_crossing = [];
                    peaks_stats = ts_cbc_peaks(xx,tt./interval,plotting_flag,1/mean(diff(tt)),unit_stims,[],threshold_crossing);
                    CBC.trial_params(j,10:end) = peaks_stats.params;
                    % Concatenate ind cycle params in long tables.
                    if ~isempty(peaks_stats.peak_amp)
                        CBC.peak_amp = vertcat(CBC.peak_amp,...
                            [repmat(CBC.trial_params(j,1:9),size(peaks_stats.peak_amp,1),1) peaks_stats.peak_amp]);
                        CBC.peak_delta = vertcat(CBC.peak_delta,...
                            [repmat(CBC.trial_params(j,1:9),size(peaks_stats.peak_delta,1),1) peaks_stats.peak_delta]);
                        CBC.peak_theta = vertcat(CBC.peak_theta,...
                            [repmat(CBC.trial_params(j,1:9),size(peaks_stats.peak_theta,1),1) peaks_stats.peak_theta]);
                        CBC.bottom_amp = vertcat(CBC.bottom_amp,...
                            [repmat(CBC.trial_params(j,1:9),size(peaks_stats.bottom_amp,1),1) peaks_stats.bottom_amp]);
                        CBC.bottom_delta = vertcat(CBC.bottom_delta,...
                            [repmat(CBC.trial_params(j,1:9),size(peaks_stats.bottom_delta,1),1) peaks_stats.bottom_delta]);
                        CBC.bottom_theta = vertcat(CBC.bottom_theta,...
                            [repmat(CBC.trial_params(j,1:9),size(peaks_stats.bottom_theta,1),1) peaks_stats.bottom_theta]);
                        CBC.rise_delta = vertcat(CBC.rise_delta,...
                            [repmat(CBC.trial_params(j,1:9),size(peaks_stats.rise_delta,1),1) peaks_stats.rise_delta]);
                        CBC.rise_theta = vertcat(CBC.rise_theta,...
                            [repmat(CBC.trial_params(j,1:9),size(peaks_stats.rise_theta,1),1) peaks_stats.rise_theta]);
                    end
                    
                    % 3. Laplace/Gaussian/Triangle (ramping) distributions;
                    % This is not informative when it's impossible to tell the shapes apart.
                else
                    OSC.trial_params(j,10:end) = nan;
                    CBC.trial_params(j,10:end) = nan;
                end
            else
                OSC.trial_params(j,10:end) = nan;
                CBC.trial_params(j,10:end) = nan;
            end
            if plotting_flag == 1 && save_plots_flag==0
                pause
            end
            if plotting_flag == 1 && save_plots_flag==1
                figure(198623)
                set(gcf,'Position',[1,1,1920,1280])
                f = fullfile(base_folder,'mid_processing',...
                    ['cycloid_fit_' num2str(LV.conditions(tr,:),'%.0f-%.0f-%0.2f-%.0f') '_d' num2str(d,'%01.0f') '_session' num2str(tr,'%03.0f') '_' ...
                    char(datetime('now','TimeZone','local','Format','y-MM-d-hhmmss')) '.png']);
                print(f, '-dpng', '-r50');
                
                pause(.1)
                
                figure(198624)
                set(gcf,'Position',[1,1,1920,1280])
                f = fullfile(base_folder,'mid_processing',...
                    ['peaks_pcs_correct_' num2str(LV.conditions(tr,:),'%.0f-%.0f-%0.2f-%.0f') '_d' num2str(d,'%01.0f') '_session' num2str(tr,'%03.0f') '_' ...
                    char(datetime('now','TimeZone','local','Format','y-MM-d-hhmmss')) '.png']);
                print(f, '-dpng', '-r50');
            end
        end
    end
end
OSC.trial_params_labels(27:29) = {'nan','\Delta2','nan'};
OSC.trial_params(:,27:29) = nan;
OSC.trial_params(:,28) = OSC.trial_params(:,23)./2/pi.*OSC.trial_params(:,5);


%% 4. Radius and speed of population oscillation
% and dispersion at event time
AMP.trial_params_labels(1:9) = {'monkey','session','condition','correct','periodic','interval','task','d','trial-phase'};
AMP.trial_params_labels(10:12) = {'b_{0,amp}','b_{1,amp}','dispersion'};
AMP.trial_params = nan(0,12);
A = [];

S.trial_params_labels = {'monkey','session','condition','correct','periodic','interval','task','d','trial-phase',...
    'speed','b0_{speed}','b1_{speed}',...
    'speed*','b0_{speed*}','b1_{speed*}'};
S.trial_params = nan(0,15);
S.speed_ts = [];

j2 = 0;
for trial_phase = [1 2]
    cycles_start_past_zero_stim = cycles_trial_phases(trial_phase,1);
    cycles_stop_past_zero_stim  = cycles_trial_phases(trial_phase,2);
    
    for tr = 1:numel(LV.lv)
        j2=j2+1;
        AMP.trial_params(j2,1:9) = [monkey tr nan LV.conditions(tr,:) max_d trial_phase];
        S.trial_params(j2,:) = [monkey tr nan LV.conditions(tr,:) 10 trial_phase nan(1,6)];
        
        fprintf('%6.0f,',AMP.trial_params(j2,1:9))
        fprintf('\n')
        if ~isempty(LV.lv{tr}) && size(LV.lv{tr},1)>1e1
            
            xx = LV.lv{tr}(:,min_d:max_d);
            tt = LV.time{tr};
            xx = xx(logical((tt < cycles_stop_past_zero_stim).*(tt > cycles_start_past_zero_stim)),:);
            tt = tt(logical((tt < cycles_stop_past_zero_stim).*(tt > cycles_start_past_zero_stim)));
            xx(any(isnan(xx),2),:) = 0;
            
            interval = LV.conditions(tr,3);
            if isnan(interval);interval=1;end
            unit_stims = ceil(min(tt./interval)):floor(max(tt./interval));
            if (sum(all(xx==0,2))/numel(xx))<.3 && ~isempty(unit_stims)
                % Speed
                speed_stats = ts_cbc_speed(xx,tt./interval,plotting_flag,1/mean(diff(tt)),unit_stims);
                % Concatenate ind cycle params in long tables.
                if ~isempty(speed_stats.speed)
                    S.trial_params(end,end-5:end) = speed_stats.params;
                    S.speed_ts = vertcat(S.speed_ts,...
                        [repmat(S.trial_params(j2,1:9),size(speed_stats.speed,1),1) ...
                        speed_stats.speed]);
                end
        
                % Amp
                amp_stats = ts_amp(xx,tt./interval,plotting_flag,unit_stims,1); % sr for fixing the smoothing scale?
                AMP.trial_params(j2,10:end) = amp_stats.params;
                A = vertcat(A,[AMP.trial_params(j2,1:9).*amp_stats.tt.^0 amp_stats.tt amp_stats.amp]);
            else
                AMP.trial_params(j2,10:end) = nan;
            end
        else
            AMP.trial_params(j2,10:end) = nan;
        end
        if plotting_flag == 1 && save_plots_flag == 0
            pause
        end
    end
end


if plotting_flag == 2 || save_plots_flag == 1

    % Amp
    if any(monkey == [1 2])
        first_stim = -3;
        a = A(A(:,10)>first_stim,:);
        cond1 = repmat({'Incorrect'},size(a,1),1);
        cond1(a(:,4)==1) = {'Correct'};
    end
    if any(monkey == 3)
        first_stim = -inf;
        a = A(A(:,10)>first_stim,:);
        cond1 = repmat({'Short'},size(a,1),1);
        cond1(a(:,7)==2) = {'Long'};
    end

    cond2 = repmat({'Listening'},size(a,1),1);
    cond2(a(:,9)==2) = {'Tapping'};

    g=gramm('x',a(:,10),'y',a(:,11),'color',a(:,6));
    g.facet_grid(cond1,cond2);
    g.stat_glm('geom','area');
    
    if any(monkey == [1 2])
        g.set_names('column','Trial phase','row','Trial outcome','x','Stimulus cycles','y','Amplitude','color','Stimulus interval','Lightness',0);
        g.axe_property('YLim',[0 15]);
    end
    if any(monkey == 3)
        g.set_names('column','Trial phase','row','Wait time','x','Stimulus cycles','y','Amplitude','color','Stimulus interval','Lightness',0);
        g.axe_property('YLim',[10 70]);
    end
    
    % g.set_title('Oscillation amplitude');
    figure('Position',[100 100 1080 640]);
    g.draw();

    if save_plots_flag==1
        f = fullfile(base_folder,'mid_processing',['osc_amp_monkey' num2str(monkey,'%.0f_') char(datetime('now','TimeZone','local','Format','y-MM-d-hhmmss')) '.png']);
        print(f, '-dpng', '-r300');
    end
    
    % Speed
    if any(monkey == [1 2])
        first_stim = -3;
        a = S.speed_ts(S.speed_ts(:,10)>first_stim,:);
        cond1 = repmat({'Incorrect'},size(a,1),1);
        cond1(a(:,4)==1) = {'Correct'};
    end
    if any(monkey == 3)
        first_stim = -inf;
        a = S.speed_ts(S.speed_ts(:,10)>first_stim,:);
        cond1 = repmat({'Short'},size(a,1),1);
        cond1(a(:,7)==2) = {'Long'};
    end
    % 'monkey','session','condition','correct','periodic','interval','task','d','trial-phase'

    cond2 = repmat({'Listening'},size(a,1),1);
    cond2(a(:,9)==2) = {'Tapping'};

    g=gramm('x',a(:,10),'y',a(:,11),'color',a(:,6));
    g.facet_grid(cond1,cond2);
    g.stat_glm('geom','area');
    
    if any(monkey == [1 2])
        g.set_names('column','Trial phase','row','Trial outcome','x','Stimulus cycles','y','Speed','color','Stimulus interval','Lightness',0);
        %g.axe_property('YLim',[0 15]);
    end
    if any(monkey == 3)
        g.set_names('column','Trial phase','row','Wait time','x','Stimulus cycles','y','Speed','color','Stimulus interval','Lightness',0);
        %g.axe_property('YLim',[10 70]);
    end
    
    % g.set_title('Oscillation amplitude');
    figure('Position',[100 100 1080 640]);
    g.draw();

    if save_plots_flag==1
        f = fullfile(base_folder,'mid_processing',['osc_amp_monkey' num2str(monkey,'%.0f_') char(datetime('now','TimeZone','local','Format','y-MM-d-hhmmss')) '.png']);
        print(f, '-dpng', '-r300');
    end
    
end


%% Export.
save(fullfile(base_folder,'mid_processing',['CBCOSCAMPA_monkey' num2str(monkey,'%.0f') '.mat']),'OSC','CBC','AMP','A','S')
