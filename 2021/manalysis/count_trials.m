cd("P:\pCloud Backup\precision\storage\inb_data\M2-TBC\mid_processing")
load SDFS_aggregate_stage.mat
for k=1:numel(SDFS.taps)
    fprintf('%2.0f: ',k)
    fprintf('%.0f ',cellfun(@numel,SDFS.taps{k}))
    fprintf('\n')
end