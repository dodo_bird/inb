[~,pc] = system('hostname');
if ~isempty(regexp(pc,'pop-os','once'))
    data_folder = '/media/dobri/disk2/storage/inb_data/';
    addpath('~/logos/inb/2021/manalysis')
    addpath('~/logos/inb/2021/manalysis/nonlinear-pca')
end
if ~isempty(regexp(pc,'precision','once'))
    data_folder = '~/storage/inb_data/';
    addpath('~/logos/inb/2021/manalysis/')
    addpath('~/logos/inb/2021/manalysis/nonlinear-pca')
end
if ~isempty(regexp(pc,'OEDBIBRB206DDDT','once'))
    data_folder = 'P:\pCloud Backup\precision\storage\inb_data';
    addpath('\\wsl.localhost\Ubuntu-22.04\home\dobri\logos\inb\2021\manalysis\')
    addpath('\\wsl.localhost\Ubuntu-22.04\home\dobri\logos\inb\2021\manalysis\nonlinear-pca')
end
if ~isempty(regexp(pc,'BRB-PF44XD65-LT','once'))
    data_folder = 'P:\pCloud Backup\precision\storage\inb_data';
    addpath('\\wsl.localhost\Ubuntu\home\dobri\logos\inb\2021\manalysis\')
    addpath('\\wsl.localhost\Ubuntu\home\dobri\logos\inb\2021\manalysis\nonlinear-pca')
end


%% Move to the target folder
switch monkey
    case 1
        base_folder = fullfile(data_folder,['M' num2str(monkey,'%1.f') '-TBC']);
    case 2
        base_folder = fullfile(data_folder,['M' num2str(monkey,'%1.f') '-TBC2']);
    case 3
        base_folder = fullfile(data_folder,['M' num2str(monkey,'%1.f')]);
end
cd(fullfile(base_folder,'mid_processing'))