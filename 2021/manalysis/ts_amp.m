function amp_stats = ts_amp(xx0,tt,plotting,varargin)

if numel(varargin)>0
    stims = varargin{1};
else
    stims = [];
end
[stims,stim_ind] = event_samples(tt,stims);

if numel(varargin)>1
    smoothflag = varargin{2};
else
    smoothflag = 0;
end

if numel(varargin)>2
    sr = varargin{3};
else
    sr=1/mean(diff(tt));
end

if smoothflag == 1
    conv_win_len_ms = 2e2; % msec
    gaussFilter = gausswin(round(conv_win_len_ms/1e3*sr*1));
    gaussFilter = gaussFilter./sum(gaussFilter);
    xx = xx0*0;
    for c = 1:size(xx0,2)
        xx(:,c) = conv(xx0(:,c),gaussFilter,'same');
    end
else
    xx = xx0;
end

amp = dot(xx-nanmean(xx),xx-nanmean(xx),2).^.5;
b = [tt.^0 (1:numel(tt))']\amp;

xxe = xx(stim_ind,:);
dispersion = var(xxe(:));

amp_stats.params = [b(1) b(2) dispersion];
amp_stats.tt = tt;
amp_stats.amp = amp;

if plotting==1
    figure(198625);clf
    l(1) = plot(tt,xx(:,1),'linewidth',1.6,'color',[.6 .6 .6]);
    hold on
    plot(tt,xx(:,2:end),'linewidth',1.6,'color',[.6 .6 .6]);
    l(2) = plot(tt(stim_ind),xx(stim_ind,1),'om','linewidth',2);
    plot(tt(stim_ind),xx(stim_ind,2:end),'om','linewidth',2);
    % plot(tt,var(xx,[],2),'linewidth',1.6);    
    l(3) = plot(stims,stims*0,'+','linewidth',3,'MarkerSize',10);
    l(4) = plot(tt,amp,'linewidth',3,'color','r');
    l(5) = plot(tt(stim_ind),amp(stim_ind,:),'sk','linewidth',2);
    l(6) = plot(tt,tt.^0*b(1)+(1:numel(tt))'*b(2),'--k','linewidth',2,'MarkerSize',10);
    hold off
    legend(l,'x','x@event','events','amp','amp@event','amp trend')
    set(legend,'location','northwest','fontsize',12);
end

end

function [e,i] = event_samples(x,probe)
if size(x,1)<size(x,2)
    x = x';
end
if size(probe,1)>size(probe,2)
    probe = probe';
end
asyncs = probe-x;
[~,i] = min(abs(asyncs));
e = x(i);
end