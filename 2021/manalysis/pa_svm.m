function [svmModel,scores] = pa_svm(Y,X,plotting_flag,varargin)

category = unique(Y);

if ~isempty(varargin)
    show_dims = varargin{1};
else
    show_dims = 1:min([3 size(X,2)]);
end

% Prepare to train an SVM with cross-validation.
c = cvpartition(Y, 'KFold', 10);
auto_optimize = 2;
if auto_optimize == 1
    folds = 1;
else
    folds = c.NumTestSets;
end
accuracy = zeros(1, folds);
precision = zeros(1, folds);
recall = zeros(1, folds);
f1Scores = zeros(1, folds);

% The k-fold loops.
for i = 1:folds
    % Split the data into training and test sets.
    trainIdx = c.training(i);
    testIdx = c.test(i);

    Xtrain = X(trainIdx,:);
    ytrain = Y(trainIdx);

    Xtest = X(testIdx,:);
    ytest = Y(testIdx);

    % Train the SVM.
    kernel_type = 2;
    switch kernel_type
        case 0
            kernel_type_label = 'linear';
        case 1
            kernel_type_label = 'rbf';
        case 2
            kernel_type_label = 'polynomial';
    end
    switch auto_optimize
        case 0
            svmModel = fitcsvm(Xtrain, ytrain, 'Standardize', false, 'KernelFunction', kernel_type_label);
        case 1
            svmModel = fitcsvm(Xtrain,ytrain, 'OptimizeHyperparameters','auto', 'KernelFunction',kernel_type_label);
    end

    % Evaluate the model.
    predictions = predict(svmModel, Xtest);
    accuracy(i) = sum(ytest == predictions) / length(ytest);

    % Calculate confusion matrix.
    [confMat,~] = confusionmat(ytest, predictions);

    % Extract TP, FP, FN from confusion matrix.
    TP = confMat(2,2);
    FP = confMat(1,2);
    FN = confMat(2,1);

    % Calculate precision and recall.
    precision(i) = TP / (TP + FP);
    recall(i) = TP / (TP + FN);

    % F1-score.
    f1Scores(i) = 2*(precision(i)*recall(i))/(precision(i)+recall(i));
end

% Average scores.
Accuracy = mean(accuracy, 'omitnan');
Precision = mean(precision, 'omitnan');
Recall = mean(recall, 'omitnan');
F1Score = mean(f1Scores, 'omitnan');

scores.Accuracy = Accuracy;
scores.Precision = Precision;
scores.Recall = Recall;
scores.F1Score = F1Score;

fprintf('%15s','Accuracy', 'Precision', 'Recall', 'F1-Score')
fprintf('\n')
fprintf('%15.3f', Accuracy, Precision, Recall, F1Score)
fprintf('\n\n')

% For visualizing the decision boundary,
% train a full SVM model, instead of using partitioned data.
svmModel = fitcsvm(X, Y, 'Standardize', false, 'KernelFunction', 'Linear');

if plotting_flag == 1
    % Create a grid over the feature space
    ds = 1e1;
    X_ds = X(1:ds:end,:);
    Y_ds = Y(1:ds:end,:);

    % Plot the original data
    colorvec = hsv(numel(category)+1);
    clear l
    for c = 1:numel(category)
        l(c) = scatter3(X_ds(Y_ds==category(c), 1), X_ds(Y_ds==category(c), 2), X_ds(Y_ds==category(c), 3), 'ok', 'MarkerFaceColor', colorvec(c,:), 'MarkerFaceAlpha', .5);
        hold on
    end

    % Show the decision boundary (plane)
    model_equation = [svmModel.Bias svmModel.Beta'];
    [x1Grid, x2Grid, ~] = meshgrid(...
        linspace(min(X_ds(:,show_dims(1))), max(X_ds(:,show_dims(1))), 50), ...
        linspace(min(X_ds(:,show_dims(2))), max(X_ds(:,show_dims(2))), 50), ...
        linspace(min(X_ds(:,show_dims(3))), max(X_ds(:,show_dims(3))), 50));

    l(c+1) = draw_plane(model_equation([1 show_dims+1]), x1Grid(:), x2Grid(:));
    l(c+1).FaceColor = colorvec(end,:);
    hold off

    set(gca,'view',[50 10])
    set(gca,'XTickLabels',[],'YTickLabels',[],'ZTickLabels',[])
    leg = legend(l,'-1','1','Decision Boundary');
    leg.Location = 'east';
    leg.Box = 'on';
    leg.Color = [.5 .5 .5];
end