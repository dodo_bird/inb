clear
monkey = 1;


%% Prep paths
[~,pc]=system('uname -a');
if ~isempty(regexp(pc,'pop-os','once'))
    data_folder = '~/storage/inb_data/';
    addpath('~/logos/inb/2021/manalysis')
end
if ~isempty(regexp(pc,'M93p','once'))
    data_folder = '~/storage/inb_data/';
    addpath('~/logos/inb/2021/manalysis/')
end


%% Move to the target folder
switch monkey
    case 1
        f = dir(fullfile(data_folder,['M' num2str(monkey,'%1.f') '-TBC*']));
    case 2
        f = dir(fullfile(data_folder,['M' num2str(monkey,'%1.f') '-TBC*']));
    case 3
        f = dir(fullfile(data_folder,['M' num2str(monkey,'%1.f') '*']));
end
base_folder = fullfile(f.folder,f.name);
clear f;clear data_folder;clear pc
cd(base_folder)


%% Load raw data. Compute the sdfs of each unit, each trial.
% Monkey 1 and 2
if any(monkey == [1 2])
    % Convert to a trial-based data_struct. Each data_struct is one session.
    file_name_sessions = dir('Behav*.mat');
    
    % Load, arrange in data_struct, pre-process, trial-based.
    w = dir(fullfile(base_folder,'selected_sessions.csv'));
    if ~isempty(w)
        which_session = table2array(readtable(fullfile(w.folder,w.name),'ReadVariableNames',false));
        which_session = find(which_session>0);
    else
        which_session = (1:numel(file_name_sessions))';
    end
    clear w

    num_cells = 0;
    for n=(which_session(1:end)')

        % Get the data first.
        % data_file = fullfile(file_name_sessions(n).folder,file_name_sessions(n).name,[file_name_sessions(n).name,'.mat']);
        data_file = fullfile(file_name_sessions(n).folder,file_name_sessions(n).name);
        data_struct = load_2020_format_data(data_file);
        if ~isfield(data_struct,'task');continue;end
        
        num_cells = num_cells + size(data_struct.Neurons,2);
        % Raw SDFs, single neuron/trial
        % figure(n)
        data_struct = sdf_over_trials(data_struct,[-7 7],0);
        
        % Save mid structure. By now data_struct could weigh 1GB+!
        save_mid_proc_data = 1;
        if save_mid_proc_data == 1
            % save(fullfile(fpath_dump_data,'mid_processing.mat'),'data_struct','-v7.3')
            save(fullfile(base_folder,'mid_processing',['mid_processing_' file_name_sessions(n).name '.mat']),'data_struct','-v7.3')
        end
    end
end

% Monkey 3
if any(monkey == 3)
    % PopST is spike times in a population of units (neurons) + stim + mov times.
    % PopLongPe is spike times in a population of units (neurons) + stim + mov times.
    % In Long the animal waits longer to start tapping.
    % Here you've got two sessions of 20 trials, so you can concat them together.
    data_struct = load_2021_format_data(fullfile(base_folder,'raw'));
    data_struct = sdf_over_trials(data_struct,[-7 5],0);
    save_mid_proc_data = 1;
    if save_mid_proc_data == 1
        save(fullfile(base_folder,['mid_processing/mid_processing_' char(datetime('now','TimeZone','local','Format','y-MM-d-hhmmss')) '.mat']),'-v7.3');
    end
end


%% Behavioral performance
count_correct_trials


%% Population activity with PCA, after aggregating and resampling the SDFs
pa_pcas_across_recs
% -- aggredate_sdfs_over_trials_sessions.m


%% Variance structure of the population activity space
% pa_plot_loading_scaling_eigenvalues
% -- rank_frequency_outliers_from_top.m


%% Project neuronal activities on the population space
pa_project_and_plot
% -- plot_3d_cool_color.m


%% CBC analysis of population activity. Export for stats in R
pa_cycl_fit_params
% -- ts_cbc_peaks.m
% -- ts_cycloid_fit.m
