function data_struct = sdf_over_trials(data_struct,varargin)
% single neuron/trial sdfs

if numel(varargin)>0
    trial_time_space = varargin{1};
else
    trial_time_space = [-7 5];
end
if numel(varargin)>1
    debugging_plot_flag = varargin{2};
else
    debugging_plot_flag = 1;
end

sdfsr = 2e4;
ds = 2e1;
conv_win_ms = 50;
sdf_time = linspace(trial_time_space(1),trial_time_space(2),sdfsr*diff(trial_time_space))';
sdf_time_ds = sdf_time(1:ds:end);
pp=0;
for tr=1:numel(data_struct.id_trials)
    fprintf('SDFS, Trial #%3.0d.\n',tr)
    [~,ind1]=min(abs(data_struct.lims(tr,1)-data_struct.center(tr)-sdf_time));
    [~,ind2]=min(abs(data_struct.lims(tr,2)-data_struct.center(tr)-sdf_time));
    sdf = nan(size(sdf_time_ds,1),size(data_struct.Neurons,2));
    ids_temp = nan(1,size(data_struct.Neurons,2));
    for l=1:size(data_struct.Neurons,2)
        sdftemp = sdf_from_conv(data_struct.Neurons{tr,l}-data_struct.center(tr),sdf_time,sdfsr,conv_win_ms);
        sdftemp(1:ind1) = nan;
        sdftemp(ind2:end) = nan;
        sdftemp = sdftemp(1:ds:end);
        sdf(:,l) = sdftemp;
        ids_temp(l) = l;
    end
    % Norm time and behav events
    [sdftime_units,time_warp_factor] = norm_time(sdf_time_ds,data_struct.stims{tr}-data_struct.center(tr),data_struct.center(tr)-data_struct.center(tr));
    [taps_norm,~] = norm_time(data_struct.tap{tr}-data_struct.center(tr),data_struct.stims{tr}-data_struct.center(tr),data_struct.center(tr)-data_struct.center(tr));
    [stims_norm,~] = norm_time(data_struct.stims{tr}-data_struct.center(tr),data_struct.stims{tr}-data_struct.center(tr),data_struct.center(tr)-data_struct.center(tr));
    [lims_norm,~] = norm_time(data_struct.lims(tr,:)-data_struct.center(tr),data_struct.stims{tr}-data_struct.center(tr),data_struct.center(tr)-data_struct.center(tr));
    
    % Store everything
    data_struct.SDF_RAW{tr,1}.sdf = sdf;
    data_struct.SDF_RAW{tr,1}.cell_ids = ids_temp;
    data_struct.SDF_RAW{tr,1}.time = sdf_time_ds;
    data_struct.SDF_RAW{tr,1}.sr = round(sdfsr/ds);
    data_struct.SDF_RAW{tr,1}.time_units = sdftime_units;
    data_struct.SDF_RAW{tr,1}.stims_norm = stims_norm;
    data_struct.SDF_RAW{tr,1}.time_warp_factor = time_warp_factor;
    data_struct.SDF_RAW{tr,1}.taps_norm = taps_norm;
    data_struct.SDF_RAW{tr,1}.lims_norm = lims_norm;
    data_struct.SDF_RAW{tr,1}.sps_ave = nanmean(sdf);
    data_struct.SDF_RAW{tr,1}.sps_sd = nanstd(sdf);
    
    if debugging_plot_flag == 1 % for debugging
        subplot(2,1,1)
        plot(data_struct.SDF_RAW{tr}.time(1:5:end),isnan(data_struct.SDF_RAW{tr,1}.sdf(1:5:end,:)));
        hold on
        plot(data_struct.SDF_RAW{tr}.time(1:5:end),data_struct.SDF_RAW{tr,1}.sdf(1:5:end,:));
        plot(data_struct.stims{tr}-data_struct.center(tr),data_struct.stims{tr}*0,'^','LineWidth',2);
        plot(data_struct.tap{tr}-data_struct.center(tr),data_struct.tap{tr}*0,'v','LineWidth',2);
        plot(data_struct.SDF_RAW{tr}.time(1:5:end),nanmean(data_struct.SDF_RAW{tr,1}.sdf(1:5:end,:),2),'LineWidth',4);
        hold off
        
        subplot(2,1,2)
        pp = pp+1;
        subplot(3,1,pp)
        plot(data_struct.SDF_RAW{tr}.time_units(1:5:end),data_struct.SDF_RAW{tr}.sdf(1:5:end,:));
        hold on
        plot(data_struct.SDF_RAW{tr}.stims_norm,data_struct.SDF_RAW{tr}.stims_norm*0,'^','LineWidth',2);
        plot(data_struct.SDF_RAW{tr}.taps_norm,data_struct.SDF_RAW{tr}.taps_norm*0,'v','LineWidth',2);
        plot(data_struct.SDF_RAW{tr}.lims_norm,data_struct.SDF_RAW{tr}.lims_norm*0,'+','LineWidth',2);
        hold off
    end
end

fr_ave_vec = nan(size(data_struct.Neurons));
fr_sd_vec = nan(size(fr_ave_vec));
for tr=1:size(data_struct.Neurons,1)
    for l=1:size(data_struct.Neurons,2)
        ind = find(data_struct.SDF_RAW{tr}.cell_ids == l);
        if ~isempty(ind)
            fr_ave_vec(tr,l) = data_struct.SDF_RAW{tr,1}.sps_ave(ind);
            fr_sd_vec(tr,l) = data_struct.SDF_RAW{tr,1}.sps_sd(ind);
        end
    end
end

data_struct.fr_ave = nanmean(fr_ave_vec);
data_struct.fr_sd = nanmean(fr_sd_vec);

%fprintf('%s\n','A small check for cell_id alignment after some of the pre-processing steps.')
%fprintf('%3.0f',data_struct.nonempty_signals_index'-find(~isnan(data_struct.fr_ave)))
%fprintf('\n')
