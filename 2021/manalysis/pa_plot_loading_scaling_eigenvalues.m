clear


%% Prep paths and move to the target folder
[~,pc]=system('uname -a');
if ~isempty(regexp(pc,'pop-os','once'))
    addpath('~/logos/inb/2021/manalysis')
end
if ~isempty(regexp(pc,'ideapad','once'))
    data_folder = '~/Desktop/inb_data/';
    addpath('~/Desktop/inb/2021/manalysis/')
end
if ~isempty(regexp(pc,'PCWIN64','once'))
    data_folder = 'C:\Users\Dobri\Desktop\inb_data';
    addpath('C:\Users\Dobri\Desktop\inb\2021\manalysis')
end
cd(data_folder)


%% Plotting settings
plotting_flag = 1;
save_plots_flag = 0;
color_vec = cool(3);


%% If it's necessary to reproduce exactly the outcomes.
rng(134)


%% Loop over each of the three monkeys
for monkey = 1:3
    if any(monkey == [1 2])
        base_folder = fullfile(data_folder,['M' num2str(monkey,'%1.f') '-TBC']);
    end
    if any(monkey == 3)
        base_folder = fullfile(data_folder,['M' num2str(monkey,'%1.f')]);
    end
    load(fullfile(base_folder,'mid_processing','pca_global_space.mat'))
    x = explained_c;
    
    [~,outliers_n,zfit,sumr2] = rank_frequency_outliers_from_top(explained_c,0);
    disp([monkey,outliers_n,sumr2,zfit.b'])
    x_out = x(1:outliers_n);
    
    if plotting_flag == 1
        subplot(1,3,monkey)
        loglog(x,'s','MarkerSize',6,'MarkerFaceColor',color_vec(2,:).*.8,'Color',color_vec(2,:).*.0,'linewidth',.2)
        hold on
        loglog([(1:outliers_n)';zfit.x],exp(zfit.b(1)).*[(1:outliers_n)';zfit.x].^zfit.b(2),'--','linewidth',3,'Color',[1 .3 .0])
        loglog(1:numel(x_out),x_out,'o','MarkerSize',10,'color','k','linewidth',1)
        axis square
        
        xlim([.5 1e3])
        ylim([1e-2 1e2])
        set(gca,'XTick',10.^(0:1:3))
        xlabel('PC dimension')
        ylabel('R^2')
        hold off
    end
end

if plotting_flag == 1
    hold off
end

if save_plots_flag == 1
    set(gcf,'Position',[1 1 1600 480])
    f=fullfile(data_folder,['loglog_pcs_' datestr(now,'yyyy-mm-dd-HHMMSS') '.jpeg']);
    print('-djpeg','-r300',f)
end

%{
The clustering algorithm is not fully deterministic, so don't expect always
exactly the same outcomes.
    1.0000    6.0000   36.5531    1.1888   -0.6095
    2.0000    5.0000   37.1296    1.4796   -0.7930
    3.0000   10.0000   75.3533    1.8633   -0.9480
%}
