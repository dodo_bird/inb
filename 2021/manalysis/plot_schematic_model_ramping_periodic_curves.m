function plot_schematic_model_ramping_periodic_curves(m)
%{
 for m=1:4;subplot(2,2,m);plot_schematic_model_ramping_periodic_curves(m);end
 print('-djpeg','-r300',['curves_ramping_models_schematic_' datestr(now,'yyyy-mm-dd') '.jpg'])
%}

t=(-5:.01:.3)';
theta=pi/3;
b0=100;
b1=20;
delta=.7;
switch m
    case 1
        model = 'Sine';
        resfun=@(b0,b1,delta,theta,x)((b1*x+b0).*(cos(2*pi*x-theta)./2+.5));
        clear model_string
        model_string{1} = [model '$: $'];
        model_string{2} = '$f(t; b_0,b_1,\theta) = (b_0+b_1t)cos(2\pi t-\theta/2+.5)$';
    case 2
        model = 'Cycloid';
        resfun=@(b0,b1,delta,theta,x)((b1*x+b0).*abs(cos(2*pi*x./2-theta/2)));
        clear model_string
        model_string{1} = [model '$: $'];
        model_string{2} = '$f(t; b_0,b_1,\theta) = (b_0+b_1t)|cos(2\pi t-\theta/2)|$';
    case 3
        model = 'Inverse cycloid';
        resfun=@(b0,b1,delta,theta,x)((b1*x+b0).*(-abs(cos(2*pi*x./2-theta/2-pi/2))+1));
        clear model_string
        model_string{1} = [model ': '];
        model_string{2} = '$f(t; b_0,b_1,\theta) = (b_0+b_1t)(1-|cos(2\pi t-\theta/2)|)$';
    case 4
        model = 'Sawtooth';
        resfun=@(b0,b1,delta,theta,x)((b1*x+b0).*((1-mod((x+.5-theta/(2*pi)),1))./(1-delta).*(mod((x+.5-theta/(2*pi)),1)>delta) + mod((x+.5-theta/(2*pi)),1)./delta.*(mod((x+.5-theta/(2*pi)),1)<=delta)));
        clear model_string
        model_string{1} = [model ': $f(t; b_0,b_1,\theta,\delta) = $'];
        model_string{2} = '$(b_0+b_1t)(1-t~mod~1)/(1-\delta), for~t~mod~1 > \delta$';
        model_string{3} = '$(b_0+b_1t)(t~mod~1)/\delta, for~t~mod~1 \le \delta$';
end

plot(t,resfun(b0,b1,delta,theta,t),'-ko')
hold on
plot(t,b0*t.^0,'--','LineWidth',2)
plot(t,b1*t+b0,'--','LineWidth',2)
plot((0:-1:-5)+theta/2/pi,(0:-1:-5)*0,'^r','MarkerSize',10,'LineWidth',2)
hold off
legend('Model','b_0: Intercept','b_1: Slope','\theta: Phase','location','West')
xlim([min(t) max(t)])
ylim([0 1.3*b0])
text(.05,.95,model_string,'units','normalized','interpreter','latex')
xlabel('Stimulus cycle')
ylabel('Firing rate, sps')
box off

return

%{
%fo = fitoptions('Method','NonlinearLeastSquares','Lower',[0,-10,0],'Upper',[100,10,2*pi],'StartPoint',[1 6 pi/8]);
%ft = fittype(resfun,'options',fo);


tt=trial_time_start_stop_vec(logical((trial_time_start_stop_vec<0).*(trial_time_start_stop_vec>-3)));
xx=temp0_conv(logical((trial_time_start_stop_vec<0)),:);



resfun=@(b0,b1,theta,x)(b0*b1*(x+6+1/b1).*abs(cos(2*pi*x./2+theta)));
fo = fitoptions('Method','NonlinearLeastSquares',...
               'Lower',[-50,-10,0],...
               'Upper',[50,100,2*pi],...
               'StartPoint',[5 5 pi/8]);
ft = fittype(resfun,'options',fo);
[curve,gof] = fit(repmat(tt,size(xx,2),1),xx(:),ft);



plot(tt(1:10:end),xx(1:10:end,:),'-k')
hold on
plot(tt(1:10:end),resfun(curve.b0,curve.b1,curve.theta,tt(1:10:end)),'-o')
hold off


% Try a synthetic ts to see what the function looks like.
resfun=@(b0,b1,b2,theta,x)(b0*b2*(x+6+1/b2).*abs(cos(2*pi*x./2+theta))+b1);
fo = fitoptions('Method','NonlinearLeastSquares',...
               'Lower',[0,0,-10,0],...
               'Upper',[100,20,100,2*pi],...
               'StartPoint',[1 1 5 pi/8]);
ft = fittype(resfun,'options',fo);

t=-5:.01:0;b0=1;b1=1;b2=5;theta=pi/8;
plot(t,resfun(b0,b1,b2,theta,t),'-o',t,b2*(t+6+1/b2)+b1,'-ro')
%}