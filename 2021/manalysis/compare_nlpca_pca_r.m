%%
clearvars -except 'monkey'
if ~exist('monkey','var')
    monkey = 1;
end


%% Prep paths
set_workspace_path;


%% Start after pa_pcas_across_recs has completed.
load pca_global_space.mat


%%
compare_linear_vs_nonlinear_with_cv = 1;
final_full_model = 0;
plotting = 0;
% num_dims_circular = num_dims;
% num_dims_circular = 6;
% num_dims = 6;
pre_pca_dims = 1e2;
learning_rate = .0001;
nlpca_training_scenario = 1;

if compare_linear_vs_nonlinear_with_cv == 1
    plot_scatter = 0;
    num_folds = 10;
    MSE = nan(num_folds,2);
    R = nan(num_folds,2);
    parfor k = 1:num_folds
        if num_folds < 2
            i_test = 1:size(SDF_huge_clean,1);
            i_train = i_test;
        else
            index = randperm(size(SDF_huge_clean,1));
            i_test = index(1:round(size(SDF_huge_clean,1)/num_folds));
            i_train = setdiff(1:size(SDF_huge_clean,1),i_test);
        end

        [P,sc_train,~,~,~,~] = pca(SDF_huge_clean(i_train,:));
        SDF_reduced_pca_test = SDF_huge_clean(i_test,:)*P(:,1:num_dims);
        SDF_huge_c_reconstructed_pca_test = SDF_reduced_pca_test*P(:,1:num_dims)';
        [r_pca,p_pca] = corr( reshape(SDF_huge_clean(i_test,:),[],1), SDF_huge_c_reconstructed_pca_test(:));
        ep = mean((reshape(SDF_huge_clean(i_test,:),[],1) - SDF_huge_c_reconstructed_pca_test(:)).^2);

        switch nlpca_training_scenario
            case 1
                [~, npcanet] = nlpca(sc_train(:, 1:pre_pca_dims)', num_dims, 'plotting', 'no', ...
                    'max_iteration', 1e4, 'weight_decay_coefficient', learning_rate, ...
                    'circular', 'no', 'pre_pca' , 'no');
                sc_test = SDF_huge_clean(i_test,:)*P(:,1:pre_pca_dims);
                SDF_reduced_nl_train = nlpca_get_components(npcanet)';
                SDF_reduced_nl_test = nlpca_get_components(npcanet, sc_test')';
                SDF_huge_c_reconstructed_nlpca_test = nlpca_get_data(npcanet, SDF_reduced_nl_test')';
                SDF_huge_c_reconstructed_nlpca_test = SDF_huge_c_reconstructed_nlpca_test*P(:,1:pre_pca_dims)';
            case 2
                [~, npcanet] = nlpca(SDF_huge_clean(i_train,:)', num_dims, 'plotting', 'no', ...
                    'max_iteration', 1e4, 'weight_decay_coefficient', learning_rate, ...
                    'circular', 'no', 'pre_pca' , 'yes');
                SDF_reduced_nl_test = nlpca_get_components(npcanet, SDF_huge_clean(i_test,:)')';
                SDF_huge_c_reconstructed_nlpca_test = nlpca_get_data(npcanet, SDF_reduced_nl_test')';
            case 3
                [~, npcanet] = nlpca(SDF_huge_clean(i_train,:)', num_dims, 'plotting', 'no', ...
                    'max_iteration', 1e4, 'weight_decay_coefficient', learning_rate, ...
                    'circular', 'yes');
                SDF_reduced_nl_test = nlpca_get_components(npcanet, SDF_huge_clean(i_test,:)')';
                SDF_huge_c_reconstructed_nlpca_test = nlpca_get_data(npcanet, SDF_reduced_nl_test')';
        end
        [r_nlpca, p_nlpca] = corr( reshape(SDF_huge_clean(i_test,:),[],1), SDF_huge_c_reconstructed_nlpca_test(:));
        enl = mean((reshape(SDF_huge_clean(i_test,:),[],1) - SDF_huge_c_reconstructed_nlpca_test(:)).^2);

        R(k,:) = [r_pca r_nlpca];
        MSE(k,:) = [ep enl];

        if plot_scatter == 1
            figure(193465)
            subplot(1,2,1)
            scatter(reshape(SDF_huge_clean(i_test,:),[],1), SDF_huge_c_reconstructed_pca_test(:))

            subplot(1,2,2)
            scatter(reshape(SDF_huge_clean(i_test,:),[],1), SDF_huge_c_reconstructed_nlpca_test(:))

            pause
        end
        fprintf('======= Completed fold %.0f out of %.0f ======= \n',k,num_folds)
    end
    fprintf('======= Mean (SD) test performance ======= \nMSE:\n%5.2f %5.2f, (%.2f %5.2f)\nR:\n%5.2f %5.2f, (%.2f %5.2f)\n\n', ...
        mean(MSE), std(MSE), mean(R), std(R))
end


if final_full_model == 1
    [P,SDF_reduced_pca,~,~,explained_c,mu_cells_c] = pca(SDF_huge_clean);
    switch nlpca_training_scenario
        case 1
            [~, npcanet] = nlpca(SDF_reduced_pca(:, 1:pre_pca_dims)', num_dims, 'plotting', 'no', ...
                'max_iteration', 1e4, 'weight_decay_coefficient', learning_rate, ...
                'circular', 'no', 'pre_pca' , 'no');
            SDF_reduced_nl = nlpca_get_components(npcanet)';
            SDF_huge_c_reconstructed_nlpca = nlpca_get_data(npcanet, SDF_reduced_nl')';
            SDF_huge_c_reconstructed_nlpca = SDF_huge_c_reconstructed_nlpca*P(:,1:pre_pca_dims)';
        case 2
            [~, npcanet] = nlpca(SDF_huge_clean', num_dims, 'plotting','no', ...
                'max_iteration', 1e4, 'weight_decay_coefficient', learning_rate, ...
                'pre_pca', 'yes');
            SDF_reduced_nl = nlpca_get_components(npcanet)';
            SDF_huge_c_reconstructed_nlpca = nlpca_get_data(npcanet, SDF_reduced_nl')';
        case 3
            [~, npcanet] = nlpca(SDF_huge_clean', num_dims, 'plotting', 'no', ...
                'max_iteration', 1e4, 'weight_decay_coefficient', learning_rate, ...
                'circular', 'yes');
            SDF_reduced_nl = nlpca_get_components(npcanet)';
            SDF_huge_c_reconstructed_nlpca = nlpca_get_data(npcanet, SDF_reduced_nl')';
    end
end


if final_full_model == 1 && plotting == 1
    figure(193466)
    for d = 1:num_dims
        subplot(1,2,1)
        plot(SDF_reduced_pca(:,d)+d*1e2)
        hold on
        subplot(1,2,2)
        plot(SDF_reduced_nl(:,d)+d*2)
        hold on
    end
    subplot(1,2,1)
    hold off
    subplot(1,2,2)
    hold off
end


%{

======= Mean (SD) test performance ======= 
MSE:
 4.82  4.74, (0.16  0.16)
R:
 0.60  0.61, (0.01  0.01)

[~,p,~,stats] = ttest(MSE(:,1),MSE(:,2))
p =
   7.7003e-07
stats = 
    tstat: 11.9999
       df: 9
       sd: 0.0200

[~,p,~,stats] = ttest(R(:,1),R(:,2))
p =
   9.9094e-07
stats = 
    tstat: -11.6493
       df: 9
       sd: 0.0022

%}

% This needs more tweaking and smoothing. A more refine script in plot_3d...m
% color_vec = winter(size(SDF_reduced_nl,1))*.9;
% for j=11:2:size(SDF_reduced_nl,1)
%     plot3(SDF_reduced_nl([j-10 j],1),SDF_reduced_nl([j-10 j],2),SDF_reduced_nl([j-10 j],3),'-','color',color_vec(j,:))
%     hold on
% end
% hold off

