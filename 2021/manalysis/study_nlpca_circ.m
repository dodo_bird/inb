addpath('~/logos/inb/2021/manalysis/nonlinear-pca')

t = (0:.01:10)'.*2*pi;
x = cos(t).*(-t./max(t));
y = cos(t+pi/2).*(-t./max(t)./2);
z = 1+t./max(t);
X = [x,y,z,...
    .1*[x(randperm(numel(x))),y(randperm(numel(y))),z(randperm(numel(z)))],...
    .1*randn(numel(x),3)];

figure(1908344)
plot(t,X)
plot3(x,y,z)
plot3(t,y,z)

[~, scp] = pca(X);
figure(1908345)
plot(t,scp(:,1:3))
plot3(scp(:,1),scp(:,2),scp(:,3))
plot3(t,scp(:,1),scp(:,2))

learning_rate = .001;
num_dims = 3;
[~, npcanet] = nlpca(X', num_dims, 'plotting', 'yes', ...
    'max_iteration', 1e4, 'weight_decay_coefficient', learning_rate, ...
    'circular', 'no'); 
% circular fails miserably as soon as the magnitude of the noise starts to
% approach the magnitude of the signal.
sc = nlpca_get_components(npcanet)';

figure(1908346)
plot(t,sc)
plot3(sc(:,1),sc(:,2),sc(:,3))
plot3(t,sc(:,1),sc(:,2))

