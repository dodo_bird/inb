function data_struct = load_2021_format_data(fpath1)

empty_cell_threshold_sps = 2;

data_temp = load(fullfile(fpath1,'PopStructSTSTlongMarch2021.mat'),'PopLongPe','PopST');

% PopST is spike times in a population of units (neurons) + stim + mov times.
% PopLongPe is spike times in a population of units (neurons) + stim + mov times.
% In Long the animal waits longer to start tapping.

% This is dangerous; if they change syntax you can mistake the conditions.
% fn = fieldnames(data_temp);
% for f=1:numel(fn)
%     data{f} = eval(['data_temp.' fn{f}]);
% end
labels = {'short_wait','long_wait'};
data{1} = data_temp.PopST;
data{2} = data_temp.PopLongPe;
IDs=fields(data_temp);

trial_cumul = 0;
clear data_struct
data_struct.monkey = 'M3';
data_struct.sampling_rate = 24414.0625; % do we ever use this, actually, given that the spikes are already sorted?
data_struct.trial_crop = [.35 .2]; % In this recording the spikes start to arrive 300 ms after the trial start signal.
for d = 1:numel(data)
    
    ncells(d) = size(data{d},1);
    fprintf('%4.0f cells.\n', ncells(d))
    data_struct.num_cells = ncells(d);
    % You better be f**ing consistent or I'll scream!
    if d==2;if ncells(1)~=ncells(2);keyboard;end;end
    nperiods = size(data{d},2);
    for p = 1:nperiods

        % Just checking for consistency.
        Conds.first_tap = [];
        Conds.n_stims = [];
        Conds.n_taps = [];

        trial_n = 0;
        for c = 1:ncells(d)
            for k = 1:size(data{d}(c,p).STime,1)
                
                temp = find(data{d}(c,p).MTime(k,:)>0,2,'first'); % skip the very first tap is to start tapping, it's not aligned with the stimulus.
                Conds.first_tap(k,c) = temp(2);
                Conds.n_stims(k,c) = numel(data{d}(c,p).STime(k,:));
                Conds.n_taps(k,c) = sum(data{d}(c,p).MTime(k,:)>0);
                Conds.intervals(k,c) = mean(diff(data{d}(c,p).STime(k,:)));
                
                fprintf('%8.3f',diff(data{d}(c,p).STime(k,:)))
                fprintf('\n')
                
                % first and second to last stims + constants in trial_crop.
                data_struct.lims(trial_cumul + k,:) = [data_struct.trial_crop(1)+min(data{d}(c,p).STime(k,:)) data_struct.trial_crop(2)+data{d}(c,p).STime(k,end-1)];
                data_struct.first_tap(trial_cumul + k,1) = Conds.first_tap(k,c);
                data_struct.task(trial_cumul + k,1) = d;
                data_struct.task_label{trial_cumul + k,1} = labels{d};
                data_struct.taskID{trial_cumul + k,1} = IDs{d};
                data_struct.interval(trial_cumul + k,1) = round(mean(diff(data{d}(c,p).STime(k,2:end-1))),2);
                data_struct.center(trial_cumul + k,1) = data{d}(c,p).STime(k,Conds.first_tap(k,c));
                data_struct.correct(trial_cumul + k,1) = 1; % these were all selected correct trials.
                data_struct.stims{trial_cumul + k,1} = data{d}(c,p).STime(k,2:end-1);
                data_struct.tap{trial_cumul + k,1} = data{d}(c,p).MTime(k,data_struct.first_tap(trial_cumul + k,1):(end-1));
                data_struct.periodic_index(trial_cumul + k,1) = 1; % these were all w/ periodic stim
                data_struct.id_trials(trial_cumul + k,1) = k;
                data_struct.error(trial_cumul + k,1) = nan;
                
                data_struct.Neurons{trial_cumul + k,c} = data{d}(c,p).Train{k};
                if ~isempty(data{d}(c,p).Train{k})
                    data_struct.first_spike(trial_cumul + k,c) = min(data{d}(c,p).Train{k});
                else
                    data_struct.first_spike(trial_cumul + k,c) = nan;
                end
            end
            trial_n = max([trial_n k]);
        end
        if any(any(mode(mode(Conds.n_taps(:,:))) ~= Conds.n_taps(:,:)))
            fprintf('%s\n','Qué pasó?')
            keyboard
        end
        trial_cumul = trial_cumul + trial_n;
    end
end

data_struct.period_vec = unique(data_struct.interval');

counters = zeros(1,size(data{d},1));
data_struct.fr_ave = zeros(1,size(data{d},1));
data_struct.nonempty_signals = zeros(1,size(data{d},1));
for d = 1:numel(data)
    ncells = size(data{d},1);
    nperiods = size(data{d},2);
    for p = 1:nperiods
        for c = 1:ncells
            for k = 1:size(data{d}(c,p).Train,2)
                counters(c) = counters(c) + 1;
                data_struct.fr_ave(1,c) = data_struct.fr_ave(1,c) + ...
                    numel(data{d}(c,p).Train{k})/(data{d}(c,p).STime(k,end) - data{d}(c,p).STime(k,1));
            end
        end
    end
end
data_struct.fr_ave = data_struct.fr_ave./counters;
data_struct.fr_sd = data_struct.fr_ave*nan;
data_struct.nonempty_signals_index = find(data_struct.fr_ave>empty_cell_threshold_sps);

for tr = 1:numel(data_struct.tap)
    data_struct.stims_to_tap(tr,1) = numel(data_struct.stims{tr}) - sum(~isnan(data_struct.tap{tr})) - 1;
end
