function [sdftime_units,time_warp_factor] = norm_time(events,refs,center)

num_prior_events = sum(refs<center);
num_post_events = sum(refs>center);
time_warp_factor=[];

sdftime_units = events;
index = logical(events<refs(1));
if sum(index)>0
    sdftime_units(index) = (sdftime_units(index)-refs(1)-num_prior_events);
end
time_warp_factor=horzcat(time_warp_factor,1);
for c=1:num_prior_events
    index = logical((events>=refs(c)).*(events<refs(c+1)));
    d = (refs(c+1)-refs(c));
    time_warp_factor=horzcat(time_warp_factor,d);
    if sum(index)>0
        sdftime_units(index) = (sdftime_units(index)-refs(c))./d-num_prior_events+(c-1);
        
    end
end
for c=(num_prior_events+1):(num_prior_events+1+num_post_events-1)
    index = logical((events>=refs(c)).*(events<refs(c+1)));
    d = (refs(c+1)-refs(c));
    time_warp_factor=horzcat(time_warp_factor,d);
    if sum(index)>0
        sdftime_units(index) = (sdftime_units(index)-refs(c))./(refs(c+1)-refs(c))+(c-1-num_prior_events);
    end
end
index = logical(events>=refs(end));
time_warp_factor=horzcat(time_warp_factor,1);
if sum(index)>0
    sdftime_units(index) = (sdftime_units(index)-refs(end))+num_post_events;
end

events_norm = ((-num_prior_events):num_post_events)';

% subplot(2,1,1)
% plot(sdftime,sdftime*0,'-k')
% hold on
% plot(lims,lims*0,'s')
% plot(stims,stims*0,'v')
% plot(center,center*0,'^')
% hold off
%
% subplot(2,1,2)
% plot(sdftime_units,sdftime_units*0,'-k')
% hold on
% plot(lims_norm,lims_norm*0,'s')
% plot(stims_norm,stims_norm*0,'v')
% plot(0,0,'^')
% hold off