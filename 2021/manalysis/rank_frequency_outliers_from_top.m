function [outliers_removed,total_outliers,zfit,sumr2,outliers_table] = rank_frequency_outliers_from_top(r2,plotting)
%
% Decide how many outliers to remove from the top of rank-ordered data.
% Iteratively remove ranks from the top and observe the clusters of data.
% 1) Regress a scaling law. 2) Fit a GMM to the residuals to determine the
% number of clusters. 3) Collect the number of outliers in the cluster with
% the largest (top) center. What number of removed top ranks minimizes the
% number of remaining outliers?
% Dodo Bird, 2021, Hamilton

max_clusters = 5;
max_points_to_remove = 16;
if numel(r2)>2e3
    proportion = .25;
else
    proportion = .5;
end
outliers_table = [];
for outliers_removed=0:max_points_to_remove
    Residuals=[];
    if numel(r2)>=10
        x = ((1+outliers_removed):round(proportion*numel(r2)))';
        y = r2((1+outliers_removed):round(proportion*numel(r2)));
        b = [x.^0 log(x)]\log(y);
        y_flat = log10(y) - log10(exp(b(1)).*x.^b(2));
        Residuals = vertcat(Residuals,[x y_flat]);
    end
    
    E = evalclusters(Residuals(:,2),'kmeans','silhouette','klist',1:max_clusters,'ClusterPriors','equal');
    n_Gaussians_optimal=E.OptimalK;
    switch n_Gaussians_optimal
        case 2
            [idx, centroids] = kmeans(Residuals(:,2), n_Gaussians_optimal, 'Start', [0 .2]');
        case 3
            [idx, centroids] = kmeans(Residuals(:,2), n_Gaussians_optimal, 'Start', [-.2 0 .2]');
        otherwise
            [idx, centroids] = kmeans(Residuals(:,2), n_Gaussians_optimal);
    end
    
    AIC = zeros(1,max_clusters);
    GMModels = cell(1,max_clusters);
    for k = 1:max_clusters
        options = statset('MaxIter',1e3);
        try GMModels{k} = fitgmdist(Residuals(:,2),k,'Options',options);
        catch
            GMModels{k}.AIC = inf;
            GMModels{k}.mu = 0;
        end
        AIC(k)= GMModels{k}.AIC;
    end
    [~,n_Gaussians_AIC] = min(AIC);
    
    if plotting == 1
        figure(175323)
        subplot(2,2,1)
        loglog(r2,'-sk')
        
        subplot(2,2,2)
        plot(Residuals(:,1),Residuals(:,2),'sk')
        
        subplot(2,2,3:4)
        [c,pdfx]=hist(Residuals(:,2),1e2);
        plot(pdfx,c./sum(c))
        hold on
        for k = 1:max_clusters
            if GMModels{k}.AIC~=inf
                pdfy = pdf(GMModels{k},pdfx');
                plot(pdfx,pdfy./sum(pdfy),'--','linewidth',2)
            end
        end
        hold off
        pause
    end
    
    % The 1st rule is: Converge to 1 positive cluster.
    one_cluster_is_positive = sum(centroids>0)<2;
    
    % The 2nd rule is: Less than two of three centers are > .01.
    one_out_of_three_clusters_is_positive = sum(GMModels{3}.mu>.01)<2;
    
    % The 3rd rule is: Less than two of n centers are > .01.
    one_out_of_n_aic_clusters_is_positive = sum(GMModels{n_Gaussians_AIC}.mu>.01)<2;
    
    % Outlier clusters: # positive clusters with mu > 2*SD.
    outlier_clusters = sum(GMModels{n_Gaussians_AIC}.mu'>(2*std(Residuals(:,2))))==0;
    
    % Outliers: # positive values in top cluster.
    if numel(unique(idx)) == 0
        outliers = 0;
    else
        outliers = sum(idx==max(idx));
    end
    
    outliers_table = vertcat(outliers_table, ...
        [outliers_removed ...
        n_Gaussians_optimal ...
        n_Gaussians_AIC ...
        one_cluster_is_positive ...
        one_out_of_three_clusters_is_positive ...
        one_out_of_n_aic_clusters_is_positive ...
        outlier_clusters ...
        outliers ...
        outliers + outliers_removed ...
        sum(r2(1:(outliers + outliers_removed)))]);
end

if plotting == 1
    fprintf('%13s','dims_out','GMMn_opt','GMMn_aic','1>0,opt','rule3','1>0,aic','outcluste','outliers','sum_outliers','cumr2')
    fprintf('\n')
    for r = 1:size(outliers_table,1)
        fprintf('%13.0f',outliers_table(r,:))
        fprintf('\n')
    end
end

% The minimum number of outliers to remove.
% outliers_removed = outliers_table(find(outliers_table(:,3)==1,1,'first'),1);
[~,ind] = min(outliers_table(:,8));
outliers_removed = outliers_table(ind,1);
total_outliers = outliers_table(ind,9);
sumr2 = outliers_table(ind,end);

x = ((1+total_outliers):round(proportion*numel(r2)))';
y = r2((1+total_outliers):round(proportion*numel(r2)));
b = [x.^0 log(x)]\log(y);

zfit.x = x;
zfit.y = y;
zfit.b = b;