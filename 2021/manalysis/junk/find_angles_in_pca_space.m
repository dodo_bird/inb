
t = (0:.01:10)';
x = cos(t*2*pi);
x(:,2) = cos(t*2*pi+pi/2);
x(:,3) = cos(t*2*pi+pi*3/2);
x(:,4) = cos(t*2*pi+pi*2/3);
t1 = t<5;
t2 = t>=5;
x(t1,:) = 2*x(t1,:);
x(t2,:) = x(t2,:).^3;

% x = randn(1e2,10);

[V,xr,L] = pca(x);
% corr(V)

plot(t,x(:,[1 3]),t,xr(:,1:2)*V([1 3],1:2)'+.2,'--')

% [U,S,V] = svd(x);
% corr(V)
% 
% % They have to be orthogonal, дурак, that's the point of SVD!
% angle_between_vecs = @(u,v) atan2(norm(cross(u,v)),dot(u,v));
% angle_between_vecs2 = @(u,v) acos(dot(u,v)/(norm(u)*norm(v)));

[V2,xr2,L2] = pca(xr);
