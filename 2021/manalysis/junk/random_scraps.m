t=(0.1:.01:10)';clear X
X(:,:,1)=sin(t*2*pi*2).*[1 1 1]*1;
X(:,:,1) = X(:,:,1) + randn(size(X(:,:,1)))*.1;
X(:,:,2)=sin(t*2*pi*2).*[1 1 1]*2;
X(:,:,2) = X(:,:,1) + randn(size(X(:,:,2)))*.1;
X(:,:,3)=sin(t*2*pi*2).*[1 1 1]*3;
X(:,:,3) = X(:,:,1) + randn(size(X(:,:,3)))*.1;
speed_stats = ts_cbc_speed(X(:,:,1),t,1,100);
speed_stats = ts_cbc_speed(X(:,:,2),t,1,100);
speed_stats = ts_cbc_speed(X(:,:,3),t,1,100);

