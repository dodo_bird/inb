function l = draw_plane(params,xx,yy)
%DRAW_PLANE(plane_equation_params,x,y)
% A plane is a*x+b*y+c*z+d=0 .
% [a,b,c] is the normal. Thus, we have to calculate d and we're set.
% d = -point*normal'; % dot product for less typing.

normal = params(2:4)';
d = params(1);

% Create x,y.
[xx,yy] = ndgrid(min(xx):((max(xx)-min(xx))/10):max(xx),min(yy):((max(yy)-min(yy))/10):max(yy));
zz = (-normal(1)*xx - normal(2)*yy - d)/normal(3);

% Plot the surface.
l = surf(xx,yy,zz,zz.^0*.2);
alpha(.7)