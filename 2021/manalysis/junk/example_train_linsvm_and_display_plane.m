% Generate some random data with an offset between two clusters.
n = 1e3;
X = [randn(n,3); randn(n,3) + 2];
category = [-1 1];
y = [ones(n,1)*category(1); ones(n,1)*category(2)];

% Prepare to train an SVM with cross-validation.
c = cvpartition(y, 'KFold', 10);
accuracy = zeros(1, c.NumTestSets);
precision = zeros(1, c.NumTestSets);
recall = zeros(1, c.NumTestSets);
f1Scores = zeros(1, c.NumTestSets);

% The k-fold loops.
for i = 1:c.NumTestSets
    % Split the data into training and test sets.
    trainIdx = c.training(i);
    testIdx = c.test(i);

    Xtrain = X(trainIdx,:);
    ytrain = y(trainIdx);

    Xtest = X(testIdx,:);
    ytest = y(testIdx);

    % Train the SVM.
    svmModel = fitcsvm(Xtrain, ytrain, 'Standardize', false, 'KernelFunction', 'Linear');

    % Evaluate the model.
    predictions = predict(svmModel, Xtest);
    accuracy(i) = sum(ytest == predictions) / length(ytest);

    % Calculate confusion matrix.
    [confMat,~] = confusionmat(ytest, predictions);
    
    % Extract TP, FP, FN from confusion matrix.
    TP = confMat(2,2);
    FP = confMat(1,2);
    FN = confMat(2,1);

    % Calculate precision and recall.
    precision(i) = TP / (TP + FP);
    recall(i) = TP / (TP + FN);
    
    % F1-score.
    f1Scores(i) = 2*(precision(i)*recall(i))/(precision(i)+recall(i));
end

% Average scores.
Accuracy = mean(accuracy, 'omitnan');
Precision = mean(precision, 'omitnan');
Recall = mean(recall, 'omitnan');
F1Score = mean(f1Scores, 'omitnan');

% For visualizing the decision boundary, 
% train a full SVM model, instead of using partitioned data.
svmModel = fitcsvm(X, y, 'Standardize', false, 'KernelFunction', 'Linear');
model_equation = [svmModel.Bias svmModel.Beta'];

% Create a grid over the feature space
[x1Grid, x2Grid, x3Grid] = meshgrid(linspace(min(X(:,1)), max(X(:,1)), 50), ...
                                    linspace(min(X(:,2)), max(X(:,2)), 50), ...
                                    linspace(min(X(:,3)), max(X(:,3)), 50));

% Plot the original data
colorvec = hsv(numel(category)+1);
for c = 1:numel(category)
    l(c) = scatter3(X(y==category(c), 1), X(y==category(c), 2), X(y==category(c), 3), 'ok', 'MarkerFaceColor', colorvec(c,:), 'MarkerFaceAlpha', .5);
    hold on
end

% Show the decision boundary (plane)
l(c+1) = draw_plane(model_equation, x1Grid(:), x2Grid(:));
l(c+1).FaceColor = colorvec(end,:);
l(c+1).FaceAlpha = .5;
hold off

grid on
set(gca,'view',[50 10])
set(gcf,'Color','k')
set(gca,'Color',[.2 .2 .2])
set(gca,'GridColor','k')
set(gca,'XTickLabels',[],'YTickLabels',[],'ZTickLabels',[])
leg = legend(l,'-1','1','Decision Boundary');
leg.Location = 'north';
leg.Box = 'on';
leg.Color = [.5 .5 .5];


% Scraps.
% Convenient quick display of the decision plane.
% scatter3(X(:,1), X(:,2), X(:,3), 30, y, 'filled')
%
% CVSVMModel = crossval(SVMModel);
% classLoss = kfoldLoss(CVSVMModel);
% disp(classLoss)
%
% xGrid = [x1Grid(:), x2Grid(:), x3Grid(:)];
% % Predict the scores over the grid
% [yhat, scores] = predict(SVMModel, xGrid);
% % Reshape the scores for 3D visualization
% scoresGrid = reshape(scores(:,2), size(x1Grid));
