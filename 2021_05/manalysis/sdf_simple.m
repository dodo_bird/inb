function [sdf,sdf_time] = sdf_simple(sts,sdf_time,sr,varargin)

if isempty(sts) 
    sdf = nan(size(sdf_time),'single');
    return
end

if isempty(varargin)
    conv_win_len_ms = 50; % msec
else
    conv_win_len_ms = varargin{1};
end
conv_win = round(conv_win_len_ms/1e3*sr);
gaussFilter = gausswin(conv_win);

% Where are the spikes.
[~,index]=min(abs(sdf_time-sts));

% Zeros vector, place spikes in it.
temp = zeros(size(sdf_time),'int8');
temp(index) = 1;

% SDF
sdf = conv(temp,gaussFilter,'same')*(1000/conv_win_len_ms);

return
