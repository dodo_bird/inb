%% Prep paths
clear
[~,pc]=system('uname -a');
if ~isempty(regexp(pc,'pop-os','once'))
    data_folder = '/home/dobri/inb/inb3/';
    addpath('~/logos/inb/2021_05/manalysis')
end
if ~isempty(regexp(pc,'ideapad','once'))
    data_folder = '~/Desktop/inb_data/';
    addpath('~/Desktop/inb/2021_05/manalysis/')
end
if ~isempty(regexp(pc,'PCWIN64','once'))
    data_folder = 'C:\Users\Dobri\Desktop\inb_data';
    addpath('C:\Users\Dobri\Desktop\inb\2021_05\manalysis')
end


%%
m = 3;
f=dir(fullfile(data_folder,['M' num2str(m,'%1.f') '*']));
base_folder = fullfile(f.folder,f.name);
clear f;clear data_folder;clear pc
cd(base_folder)


% PopST is spike times in a population of units (neurons) + stim + mov times.
% PopLongPe is spike times in a population of units (neurons) + stim + mov times.
% In Long the animal waits longer to start tapping.

% Convert to a trial-based data_struct. Each data_struct is one session.
% Here you've got two sessions of 20 trials, so you can concat them together.
data_struct = load_2021_format_data(fullfile(base_folder,'raw'));


%% Compute the sdfs of each unit, each trial.
data_struct = sdf_raw_over_trials(data_struct,[-7 5],0);
% save(fullfile(base_folder,['mid_processing_' datestr(date) '.mat']),'-v7.3');


%% Prepare for PCA.
% X
cd(fullfile(base_folder,'mid_processing'))
% load(fullfile(base_folder,'mid_processing','mid_processing_23-Oct-2021.mat'))
%data_struct.SDF = aggregate_and_resample_sdfs(data_struct);
file_name_sessions = dir('mid_processing_23-Oct-2021.mat');
SDFS = aggredate_sdfs_over_trials_sessions_2021(file_name_sessions,m);


% Assume that missing spikes means no spikes.
data_struct.SDF.sdf(isnan(data_struct.SDF.sdf)) = 0;

% To scale by var (although in this case cells are weirdly uniform and soft.)
var_cells = nanvar(data_struct.SDF.sdf);
max_var_cells = max(var_cells);
max_var_cells = max([max_var_cells;1]); % Floor the var to 1. Each cell by its var, or by one cell?
% bar(range(data_struct.SDF.sdf))

% Also keep the means.
mean_cells = nanmean(data_struct.SDF.sdf);


% PCA
% Normalize: softmax, divide by the max variance from all conditions. or z-score.
[P,~,~,~,explained,mu] = pca((data_struct.SDF.sdf - mean_cells)./max_var_cells);
% loglog(explained,'-ok')

% Project individual trials on the global population.
plotting_ind_pcs = 0;
target_sr_per_cycle = 50;
for tr=1:numel(data_struct.SDF_RAW)
    
    sdf = (data_struct.SDF_RAW{tr}.sdf - mean_cells)./max_var_cells;
    sdf(isnan(sdf)) = 0;
    %sdf = (sdf-mean_cells)./max_var_cells;

    sr1 = round(target_sr_per_cycle*(numel(data_struct.stims{tr})-1)/diff(data_struct.stims{tr}([1 end])));
    sdf = resample(sdf, sr1, data_struct.SDF_RAW{tr}.sr);
    sdftime = linspace(min(data_struct.SDF_RAW{tr}.time),max(data_struct.SDF_RAW{tr}.time),size(sdf,1))';

    lv = (P'*sdf')';
    data_struct.LV.lv{tr,1} = lv(:,1:10);
    data_struct.LV.time{tr,1} = sdftime;
    data_struct.LV.sr1(tr) = sr1;
    
    if plotting_ind_pcs == 1
        fprintf('Trial %3.0f, ISI %6.3f s, condition %s.\n',tr,data_struct.interval(tr),data_struct.task_label{tr})
        for l=1:10
            plot(sdftime,smooth(lv(:,l),3),'linewidth',2)
            hold on
            line([data_struct.stims{tr};data_struct.stims{tr}],[-.1;.1]*data_struct.stims{tr}.^0,'linewidth',2,'color','m')
            line([data_struct.tap{tr};data_struct.tap{tr}],[-.1;.1]*data_struct.tap{tr}.^0,'linewidth',2,'color','k')
            hold off
            pause
        end
    end
end

% Average pcs per condition
counter = 0;
periods_vec = unique(data_struct.interval);
tasks_vec = unique(data_struct.task);
for task = 1:numel(tasks_vec)
    for p = 1:numel(periods_vec)
        counter = counter + 1;
        
        trialsn = sum((data_struct.interval == periods_vec(p)).*(data_struct.task == tasks_vec(task)));
        
        min_t = 0;
        max_t = nan;
        sr = [];
        center = [];
        for tr = 1:numel(data_struct.LV.lv)
            if data_struct.interval(tr) == periods_vec(p) && data_struct.task(tr) == tasks_vec(task)
                max_t = max(max_t,max(data_struct.LV.time{tr}));
                sr = [sr data_struct.LV.sr1(tr)];
                center = [center data_struct.center(tr)];
            end
        end
        max_t = max_t + 1;
        if any(diff(sr)~=0);keyboard;else;sr = mean(sr);end
        center = mean(center);

        lvavetime = linspace(min_t,max_t,(max_t-min_t)*sr)';
        lvpre_ave = nan(numel(lvavetime),size(data_struct.LV.lv{tr},2),trialsn);
        trial_counter = 0;
        stims = [];
        for tr = 1:numel(data_struct.LV.lv)
            if data_struct.interval(tr) == periods_vec(p) && data_struct.task(tr) == tasks_vec(task)
                trial_counter = trial_counter + 1;
                [~,ind1] = min(abs(lvavetime - data_struct.center(tr)));
                [~,ind2] = min(abs(data_struct.LV.time{tr} - data_struct.center(tr)));
                indices = ((ind1 - ind2 + 1):(ind1 - ind2 + numel(data_struct.LV.time{tr})))';
                lvpre_ave(indices,:,trial_counter) = data_struct.LV.lv{tr};
                stims = [stims;data_struct.stims{tr}];
            end
        end
        lvave = nanmean(lvpre_ave,3);
        lvvar = nanvar(lvpre_ave,[],3);

        LVave.time{counter} = lvavetime - center;
        LVave.lvmean{counter} = lvave;
        LVave.lvvar{counter} = lvvar;
        LVave.stims{counter} = mean(stims,1) - center;
        LVave.center{counter} = center - center;
        LVave.task(counter) = tasks_vec(task);
        LVave.interval(counter) = periods_vec(p);
        LVave.sr(counter) = sr;
    end
end

% 1D~time plots
pcvec = [1 2 3 4 5 6 7];
save_plots_flag = 0;
clf
color_vec = cool(4)*.9;
for p = 1:numel(pcvec)
    for c = 1:numel(LVave.time)
        subplot(1,2,(c>4)+1)
        x = LVave.lvmean{c}((10:end-9),pcvec(p));
        ind = ~isnan(x);
        slv = smooth(x,round(LVave.sr(c)/4));
        x(ind) = slv(ind);
        t = LVave.time{c}(10:end-9);
        t = t./LVave.interval(c);
        l(mod(c-1,4)+1) = plot(t,x,'linewidth',2,'color',color_vec(mod(c-1,4)+1,:));
        hold on
        [~,inds] = min(abs(t - LVave.stims{c}./LVave.interval(c)));
        plot(t(inds),x(inds),'ok','MarkerFaceColor',color_vec(mod(c-1,4)+1,:),'linewidth',2,'MarkerSize',7);
        [~,inds] = min(abs(t - LVave.center{c}./LVave.interval(c)));
        plot(t(inds),x(inds),'vk','MarkerFaceColor',color_vec(mod(c-1,4)+1,:),'linewidth',2,'MarkerSize',7);
        line([t(inds) t(inds)],[min(x) max(x)],'color','k')
        if mod(c-1,4)+1==4
            legend(l,[num2str(LVave.interval(1:4)'*1e3) repmat(' ms',4,1)],'location','northwest')
            xlabel('Stimulus time relative to beginning of tapping')
            set(gca,'color',[.4 .4 .4])
            hold off
        end
    end
    if save_plots_flag == 1
        set(gcf,'Position',[1 1 800 300])
        set(gcf,'InvertHardcopy','off')
        f=fullfile(base_folder,['pc' num2str(pcvec(p),'%1.f') '_ave_by_cond_task_' data_struct.monkey '_' datestr(now,'yyyy-mm-dd-HHMMSS') '.jpeg']);
        print('-djpeg','-r200',f)
    end
    if ~save_plots_flag
        %pause
    end
end


% 3D  plots + rotation around
pcvec = [1 3 7; 2 4 5];
save_plots_flag = 0;
save_rotate = 0;
clf
for c = [1 5] % 1:numel(LVave.time)
    for p = 1:2 %2:3:9
        pcs = pcvec(p,:); %pcs = p + [0 1 2];
        plot_3d_cool_color(LVave.time{c}(4:end-3), LVave.lvmean{c}((4:end-3),[pcs(1) pcs(2) pcs(3)]), 1, round(LVave.sr(counter)/4), LVave.stims{c}, LVave.center{c}, pcs)
        set(gca,'view',[-145 28])
        if save_plots_flag == 1
            set(gcf,'Position',[1 1 800 640])
            set(gcf,'InvertHardcopy','off')
            set(gca,'XTickLabel',[]);
            set(gca,'YTickLabel',[]);
            set(gca,'ZTickLabel',[]);
            f=fullfile(base_folder,['traj_pc' num2str(pcs,'%1.f') '_ave_by_cond_task' num2str(LVave.task(c),'%1.f') '_int' num2str(LVave.interval(c)*1e3,'%3.f') '_' data_struct.monkey '_' datestr(now,'yyyy-mm-dd-HHMMSS') '.jpeg']);
            print('-djpeg','-r100',f)
        end
        if save_rotate == 1
            set(gcf,'Position',[1 1 400 320])
            axis square
            for theta = 10:10:360
                set(gca,'view',[theta 44.0000])
                set(gcf,'InvertHardcopy','off')
                set(gca,'XTickLabel',[]);
                set(gca,'YTickLabel',[]);
                set(gca,'ZTickLabel',[]);
                f=fullfile(base_folder,'frames',['traj_pc' num2str(pcs,'%1.f') '_ave_by_cond_task' num2str(LVave.task(c),'%1.f') '_int' num2str(LVave.interval(c)*1e3,'%3.f') '_' data_struct.monkey '_frame' num2str(theta,'%03.f') '_' datestr(now,'yyyy-mm-dd-HHMMSS') '.png']);
                print('-dpng','-r150','-loose',f)
            end
            
            cd(fullfile(base_folder,'frames'))
            f=['traj_pc' num2str(pcs,'%1.f') '_ave_by_cond_task' num2str(LVave.task(c),'%1.f') '_int' num2str(LVave.interval(c)*1e3,'%3.f') '_' data_struct.monkey '.gif'];
            system(['ffmpeg -framerate 5 -pattern_type glob -i "traj*.png" ' f])
            system('rm ~/Desktop/inb/2021_05/spikes_data/frames/traj_*.png')
        end
        if ~save_plots_flag && ~save_rotate
            pause
        end
    end
end


% analyze the PCs cycle-by-cycle.
