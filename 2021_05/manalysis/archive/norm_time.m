function [sdftime_units,stims_norm,time_warp_factor] = norm_time(sdftime,stims,center)

num_prior_stims = sum(stims<center);
num_post_stims = sum(stims>center);
time_warp_factor=[];

sdftime_units = sdftime;
index = logical(sdftime<stims(1));
if sum(index)>0
    sdftime_units(index) = (sdftime_units(index)-stims(1)-num_prior_stims);
end
time_warp_factor=horzcat(time_warp_factor,1);
for c=1:num_prior_stims
    index = logical((sdftime>=stims(c)).*(sdftime<stims(c+1)));
    d = (stims(c+1)-stims(c));
    time_warp_factor=horzcat(time_warp_factor,d);
    if sum(index)>0
        sdftime_units(index) = (sdftime_units(index)-stims(c))./d-num_prior_stims+(c-1);
        
    end
end
for c=(num_prior_stims+1):(num_prior_stims+1+num_post_stims-1)
    index = logical((sdftime>=stims(c)).*(sdftime<stims(c+1)));
    d = (stims(c+1)-stims(c));
    time_warp_factor=horzcat(time_warp_factor,d);
    if sum(index)>0
        sdftime_units(index) = (sdftime_units(index)-stims(c))./(stims(c+1)-stims(c))+(c-1-num_prior_stims);
    end
end
index = logical(sdftime>=stims(end));
time_warp_factor=horzcat(time_warp_factor,1);
if sum(index)>0
    sdftime_units(index) = (sdftime_units(index)-stims(end))+num_post_stims;
end

stims_norm = ((-num_prior_stims):num_post_stims)';

% subplot(2,1,1)
% plot(sdftime,sdftime*0,'-k')
% hold on
% plot(lims,lims*0,'s')
% plot(stims,stims*0,'v')
% plot(center,center*0,'^')
% hold off
%
% subplot(2,1,2)
% plot(sdftime_units,sdftime_units*0,'-k')
% hold on
% plot(lims_norm,lims_norm*0,'s')
% plot(stims_norm,stims_norm*0,'v')
% plot(0,0,'^')
% hold off