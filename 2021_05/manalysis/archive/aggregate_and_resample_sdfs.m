function out = aggregate_and_resample_sdfs(data_struct)

plotting = 0;
target_sr_per_cycle = 50;

SDF = [];
out.time = [];
out.interval = [];
out.trial = [];
out.tapping_var = [];
out.periodic = [];
for tr = 1:size(data_struct.SDF_RAW,1)
    sdftemp = data_struct.SDF_RAW{tr}.sdf;
    sdftime = data_struct.SDF_RAW{tr}.time;
    
    sr1 = round(target_sr_per_cycle*(numel(data_struct.stims{tr})-1)/diff(data_struct.stims{tr}([1 end])));
    sdftemp_ds = resample(sdftemp, sr1, ...
        data_struct.SDF_RAW{tr}.sr);
    sdftime_ds = linspace(min(sdftime),max(sdftime),size(sdftemp_ds,1))';
    
    if plotting == 1
        for c = 1:size(sdftemp,2)
            plot(sdftime, sdftemp(:,c))
            hold on
            plot(sdftime_ds, sdftemp_ds(:,c),'--')
            hold off
            pause
        end
    end
    fprintf('Trial %3.f, sr %4.0f, lenght %4.0f\n',tr,sr1,size(sdftime_ds,1))
    
    SDF = [SDF; sdftemp_ds(10:end-10,:)];
    out.time = [out.time; sdftime_ds(10:end-10,:)];
    out.interval = [out.interval; sdftime_ds(10:end-10,:)*0+data_struct.interval(tr)];
    out.trial = [out.trial; sdftime_ds(10:end-10,:)*0+tr];
    out.tapping_var = [out.tapping_var; sdftime_ds(10:end-10,:)*nan];
    out.periodic = [out.periodic; sdftime_ds(10:end-10,:)*0 + data_struct.periodic_index(tr)];
end
out.sdf = SDF;
out.sr_per_cycle = target_sr_per_cycle;