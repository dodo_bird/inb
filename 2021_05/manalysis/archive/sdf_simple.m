function [sdf_time,sdf] = sdf_simple(sts,lims,varargin)


% For simplicity, don't deal with the sdf outside the limits.
sts = sts(sts>=lims(1));
sts = sts(sts<=lims(2));

if isempty(sts)
    sdf_time = [];
    sdf = [];
    return
end

if size(varargin)<1
    sr = 24414.0625;
else
    sr = varargin{1};
end
if size(varargin)<2
    ds = 1e1; % Output sdfs downsampled to 1000 Hz.
else
    ds = varargin{2};
end


conv_win_len_ms = 50; % msec
%gaussFilter = gausswin(round(conv_win_len_ms/1e3*sr))*2*1e3/conv_win_len_ms;
gaussFilter = gausswin(round(conv_win_len_ms/1e3*sr));


sdf_time = linspace(lims(1),lims(2),round(diff(lims)*sr))';
[~,index]=min(abs(sdf_time-sts));


% Zeros vector, place spikes in it.
temp = int16(sdf_time*0);
temp(index) = 1;


sdf = conv(temp,gaussFilter,'same'); % Check the sps from conv formula.
sdf = sdf(1:ds:end);
sdf_time = sdf_time(1:ds:end);
