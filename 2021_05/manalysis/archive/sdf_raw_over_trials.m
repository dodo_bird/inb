function [SDF_RAW,fr_ave_sdf,fr_sd_sdf] = sdf_raw_over_trials(data_struct)

debugging_plot = 1;

% Raw single neuron/trial sdfs
sdfsr = 2e4;
ds = 2e1;
SDF_RAW = cell(numel(data_struct.interval),1);
for tr = 1:size(data_struct.Neurons,1)
    fprintf('SDFS, Trial #%3.0d.\n',tr)
    sdf = [];
    ids_temp = [];

    % Make the time vector
    sdftime = linspace(data_struct.lims(tr,1),data_struct.lims(tr,2),round(diff(data_struct.lims(tr,:))*sdfsr))';
    sdftime = sdftime(1:ds:end);
    
    for l=1:size(data_struct.Neurons,2)
        if ~isempty(data_struct.Neurons{tr,l})
            [~,sdftemp] = sdf_simple_old(data_struct.Neurons{tr,l},data_struct.lims(tr,:),sdfsr,ds);
        else
            sdftemp = nan(size(sdftime));
        end
        
        if ~isempty(sdf)
            if size(sdf,1)>numel(sdftemp)
                sdftemp = [sdftemp; zeros(size(sdf,1)-numel(sdftemp),1)];
            end
            if size(sdf,1)<numel(sdftemp)
                sdftemp = sdftemp(1:size(sdf,1));
            end
        end
        sdf = horzcat(sdf,sdftemp);
        ids_temp = horzcat(ids_temp,l);
    end
    
    % Norm the time, and the behav evants
    [sdftime_units,stims_norm,time_warp_factor] = norm_time_old(sdftime,data_struct.stims{tr},data_struct.center(tr));
    [taps_norm,~,~] = norm_time_old(data_struct.tap{tr},data_struct.stims{tr},data_struct.center(tr));
    
    % Store everything
    SDF_RAW{tr}.sdf = sdf;
    SDF_RAW{tr}.cell_ids = ids_temp;
    SDF_RAW{tr}.time = sdftime; %- data_struct.center(tr);
    SDF_RAW{tr}.sr = sdfsr/ds;
    SDF_RAW{tr}.time_units = sdftime_units;
    SDF_RAW{tr}.stims_norm = stims_norm;
    SDF_RAW{tr}.time_warp_factor = time_warp_factor;
    SDF_RAW{tr}.taps_norm = taps_norm;
    SDF_RAW{tr}.lims_norm = stims_norm([1 end]);
    SDF_RAW{tr}.sps_ave = nanmean(sdf);
    SDF_RAW{tr}.sps_sd = nanstd(sdf);
    
    % That's for debugging.
    if debugging_plot == 1
        subplot(2,1,1)
        plot(SDF_RAW{tr}.time,SDF_RAW{tr}.sdf);
        hold on
        plot(data_struct.stims{tr},data_struct.stims{tr}*0,'^','MarkerSize',10,'LineWidth',2);
        plot(data_struct.tap{tr},data_struct.tap{tr}*0,'v','MarkerSize',10,'LineWidth',2);
        plot(SDF_RAW{tr}.time(1:10:end),mean(SDF_RAW{tr}.sdf(1:10:end,:),2),'LineWidth',3)
        hold off
        
        subplot(2,1,2)
        plot(SDF_RAW{tr}.time_units,SDF_RAW{tr}.sdf);
        hold on
        plot(SDF_RAW{tr}.stims_norm,SDF_RAW{tr}.stims_norm*0,'^','MarkerSize',10,'LineWidth',2);
        plot(SDF_RAW{tr}.taps_norm,SDF_RAW{tr}.taps_norm*0,'v','MarkerSize',10,'LineWidth',2);
        plot(SDF_RAW{tr}.time_units(1:10:end),mean(SDF_RAW{tr}.sdf(1:10:end,:),2),'LineWidth',3)
        hold off
        pause
    end
end


fr_ave_sdf = nan(size(data_struct.Neurons));
fr_sd_sdf = nan(size(fr_ave_sdf));

for tr=1:size(data_struct.Neurons,1)
    for l=1:size(data_struct.Neurons,2)
        ind = find(SDF_RAW{tr}.cell_ids == l);
        if ~isempty(ind)
            fr_ave_sdf(tr,l) = SDF_RAW{tr}.sps_ave(ind);
            fr_sd_sdf(tr,l) = SDF_RAW{tr}.sps_sd(ind);
        end
    end
end

fr_ave_sdf = nanmean(fr_ave_sdf);
fr_sd_sdf = nanmean(fr_sd_sdf);

% fprintf('%s\n','A small check for cell_id alignment after some of the pre-processing steps.')
% fprintf('%5.0f',data_struct.nonempty_signals_index'-find(~isnan(fr_ave_sdf)))
% fprintf('\n')
