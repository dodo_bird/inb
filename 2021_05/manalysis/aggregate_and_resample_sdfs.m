function out = aggregate_and_resample_sdfs(data_struct)

plotting = 0;
target_sr_per_cycle = 50;

SDF = [];
out.time = [];
out.interval = [];
out.trial = [];
out.tapping_var = [];
out.periodic = [];
for tr = 1:size(data_struct.SDF_RAW,1)
    % sr1 = round(target_sr_per_cycle*(numel(data_struct.stims{tr})-1)/diff(data_struct.stims{tr}([1 end])));
    % sdftemp_ds = resample(data_struct.SDF_RAW{tr}.sdf, sr1, data_struct.SDF_RAW{tr}.sr);
    sdftemp = data_struct.SDF_RAW{tr}.sdf;
    sdftime = linspace(min(data_struct.SDF_RAW{tr}.time),max(data_struct.SDF_RAW{tr}.time),size(sdftemp,1))';
    
    if plotting == 1
        for c = 1:size(sdftemp,2)
            plot(data_struct.SDF_RAW{tr}.time, data_struct.SDF_RAW{tr}.sdf(:,c))
            hold on
            plot(sdftime, sdftemp(:,c),'--')
            hold off
            pause
        end
    end
    % fprintf('Trial %3.f, sr %4.0f, length %4.0f\n',tr,sr1,size(sdftime,1))
    fprintf('Trial %3.f, length %4.0f\n',tr,size(sdftime,1))
    
    SDF = [SDF; sdftemp(10:end-10,:)];
    out.time = [out.time; sdftime(10:end-10,:)];
    out.interval = [out.interval; sdftime(10:end-10,:)*0+data_struct.interval(tr)];
    out.trial = [out.trial; sdftime(10:end-10,:)*0+tr];
    out.tapping_var = [out.tapping_var; sdftime(10:end-10,:)*nan];
    out.periodic = [out.periodic; sdftime(10:end-10,:)*0 + data_struct.periodic_index(tr)];
end
out.sdf = SDF;
out.sr_per_cycle = target_sr_per_cycle;